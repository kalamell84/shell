const express = require('express');
const app = express();


app.get('/', function (req, res) {
    res.send('CMS Coming soon');
})


//Setting up server
var server = app.listen(process.env.PORT || 3002, function () {
    var port = server.address().port;
    console.log("App now running on port", port);
});