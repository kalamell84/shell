-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: mariadb_node
-- Generation Time: Mar 28, 2022 at 01:19 AM
-- Server version: 10.4.22-MariaDB-1:10.4.22+maria~focal
-- PHP Version: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shell_bcdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_role`
--

CREATE TABLE `admin_role` (
  `id` int(11) NOT NULL,
  `role` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `is_active` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_role`
--

INSERT INTO `admin_role` (`id`, `role`, `create_date`, `update_date`, `is_active`) VALUES
(1, 'Super Admin', '2022-03-24 23:00:44', '2022-03-24 23:00:44', 1),
(2, 'Admin', '2022-03-24 23:00:44', '2022-03-24 23:00:44', 1);

-- --------------------------------------------------------

--
-- Table structure for table `admin_user`
--

CREATE TABLE `admin_user` (
  `user_id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `is_active` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `campaign_info`
--

CREATE TABLE `campaign_info` (
  `campaign_id` int(11) NOT NULL,
  `campaign_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_image`
--

CREATE TABLE `cms_image` (
  `id` int(11) NOT NULL,
  `image_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_folder` int(11) DEFAULT NULL,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_tag` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_image_folder`
--

CREATE TABLE `cms_image_folder` (
  `id` int(11) NOT NULL,
  `folder_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `line_user_info`
--

CREATE TABLE `line_user_info` (
  `line_user_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `line_user_firstname` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `line_user_lastname` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `line_user_tel` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `line_user_email` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_blocked` int(1) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `fullname` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amphur` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `district` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zipcode` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_condition` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `privacy` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `register` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `line_user_info`
--

INSERT INTO `line_user_info` (`line_user_id`, `line_user_firstname`, `line_user_lastname`, `line_user_tel`, `line_user_email`, `is_blocked`, `create_date`, `update_date`, `fullname`, `gender`, `birthdate`, `address`, `province`, `amphur`, `district`, `zipcode`, `is_condition`, `privacy`, `register`, `phone`) VALUES
('Uc63926328d5d8571107d727bdea9d3dd', NULL, NULL, NULL, NULL, 0, NULL, '2022-03-25 04:37:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `request_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_message` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pillar_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `tag` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `segment_id` int(11) DEFAULT NULL,
  `broadcast_time` datetime DEFAULT NULL,
  `broadcast_type` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `json` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `status` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` int(1) DEFAULT NULL,
  `update_batch_date` datetime DEFAULT NULL,
  `pillar2_id` int(11) DEFAULT NULL,
  `campaign_id` int(11) DEFAULT NULL,
  `branch` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `message_report`
--

CREATE TABLE `message_report` (
  `id` int(11) NOT NULL,
  `message_id` int(11) DEFAULT NULL,
  `request_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pillar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tag` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `broadcast_time` datetime DEFAULT NULL,
  `image_message` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `json` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `delivered` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uniqueimpression` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uniqueclick` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `shell_personalize_link`
--

CREATE TABLE `shell_personalize_link` (
  `id` int(11) NOT NULL,
  `line_user_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message_id` int(11) DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `push_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shell_personalize_link`
--

INSERT INTO `shell_personalize_link` (`id`, `line_user_id`, `message_id`, `link`, `create_date`, `push_date`) VALUES
(1, '1', 1, 'https://www.google.co.th', NULL, NULL),
(2, '1', 1, 'https://www.google.co.th', NULL, NULL),
(3, '1', 1, 'https://www.google.co.th', NULL, NULL),
(4, '1', 1, 'https://www.google.co.th', '2022-03-28 01:15:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `shell_personalize_response`
--

CREATE TABLE `shell_personalize_response` (
  `id` int(11) NOT NULL,
  `line_user_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `response_source` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `response_keyword` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `push_date` datetime DEFAULT NULL,
  `td` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shell_personalize_response`
--

INSERT INTO `shell_personalize_response` (`id`, `line_user_id`, `response_source`, `response_keyword`, `create_date`, `push_date`, `td`) VALUES
(1, 'Uc63926328d5d8571107d727bdea9d3dd', 'text', 'fdfdf', '2022-03-27 03:01:53', NULL, 1),
(2, 'Uc63926328d5d8571107d727bdea9d3dd', 'text', 'erjkfjkejkfjkfdk', '2022-03-27 03:02:15', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `shell_response_log`
--

CREATE TABLE `shell_response_log` (
  `id` int(11) NOT NULL,
  `line_user_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `response_source` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `response_json` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shell_response_log`
--

INSERT INTO `shell_response_log` (`id`, `line_user_id`, `response_source`, `response_json`, `create_date`) VALUES
(1, 'Uc63926328d5d8571107d727bdea9d3dd', 'text', '{\"type\":\"message\",\"message\":{\"type\":\"text\",\"id\":\"15813714923203\",\"text\":\"fdfdf\"},\"timestamp\":1648350112345,\"source\":{\"type\":\"user\",\"userId\":\"Uc63926328d5d8571107d727bdea9d3dd\"},\"replyToken\":\"dd25390cb3ca42519033ebf890626928\",\"mode\":\"active\"}', '2022-03-27 03:01:53'),
(2, 'Uc63926328d5d8571107d727bdea9d3dd', 'text', '{\"type\":\"message\",\"message\":{\"type\":\"text\",\"id\":\"15813716575297\",\"text\":\"erjkfjkejkfjkfdk\"},\"timestamp\":1648350134459,\"source\":{\"type\":\"user\",\"userId\":\"Uc63926328d5d8571107d727bdea9d3dd\"},\"replyToken\":\"e747abf790a341e3aa99f6712b1e478d\",\"mode\":\"active\"}', '2022-03-27 03:02:15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_role`
--
ALTER TABLE `admin_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_user`
--
ALTER TABLE `admin_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `campaign_info`
--
ALTER TABLE `campaign_info`
  ADD PRIMARY KEY (`campaign_id`);

--
-- Indexes for table `cms_image`
--
ALTER TABLE `cms_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_image_folder`
--
ALTER TABLE `cms_image_folder`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `line_user_info`
--
ALTER TABLE `line_user_info`
  ADD PRIMARY KEY (`line_user_id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message_report`
--
ALTER TABLE `message_report`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shell_personalize_link`
--
ALTER TABLE `shell_personalize_link`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shell_personalize_response`
--
ALTER TABLE `shell_personalize_response`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shell_response_log`
--
ALTER TABLE `shell_response_log`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_role`
--
ALTER TABLE `admin_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `admin_user`
--
ALTER TABLE `admin_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `campaign_info`
--
ALTER TABLE `campaign_info`
  MODIFY `campaign_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_image`
--
ALTER TABLE `cms_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_image_folder`
--
ALTER TABLE `cms_image_folder`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `message_report`
--
ALTER TABLE `message_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `shell_personalize_link`
--
ALTER TABLE `shell_personalize_link`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `shell_personalize_response`
--
ALTER TABLE `shell_personalize_response`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `shell_response_log`
--
ALTER TABLE `shell_response_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
