module.exports = {
    name: "UserInfo",
    columns: {
        Id:{
            primary: true,
            type: "int"
        },
        LineId: {
            type: "varchar"
        },
        CarModel: {
            type: "varchar"
        }
    }
  };
  