'use strict';
const line = require('@line/bot-sdk');
const express = require('express');;
const config = require('./config.json');
const service = require('./service');
const request = require('request');
const client = new line.Client(config);
const { default: axios } = require('axios');



async function handleEvent(req,event) {
  sendEvent(event);

  console.log("almond_text",req.headers);
  delete req.headers['host'];

  var options = {
    "rejectUnauthorized": false,
      url: 'https://shelllineservices.shellcampaigns.com/api/webhook',
      method: 'POST',
      headers: req.headers,
      body: JSON.stringify(req.body),
  }; 

  /*
  
  request(options, function (error, response) {
    if (error) {
      console.log('error allmond', error);
    }
    console.log('<<<<<<<<< : ');
  });
  */




  switch (event.type) {
    case 'message':
      const message = event.message;
      switch (message.type) {
        case 'text':
          return handleText(req,message, event);
        case 'image':
          return handleImage(message, event);
        case 'video':
          return handleVideo(message, event);
        case 'audio':
          return handleAudio(message, event);
        case 'location':
          return handleLocation(message, event);
        case 'sticker':
          return handleSticker(message, event);
        default:
          throw new Error(`Unknown message: ${JSON.stringify(message)}`);
      }

    case 'follow':
        
        var user_id = event.source.userId;
        var insrt_result = await service.insertLINEUser(user_id);
        var source = "follow";
        var json = JSON.stringify(event);
        var resultinsert = await service.insertShellResponseLog(user_id,source,json);
        insrt_result = (insrt_result != 'undefined' && insrt_result != null ? insrt_result : 0)+resultinsert;
        return console.log("insert user: "+insrt_result);
    case 'unfollow':
      var user_id = event.source.userId;
      var update_result = await service.updateBlockedLINEUser(user_id);
      var source = "unfollow";
      var json = JSON.stringify(event);
      var resultinsert = await service.insertShellResponseLog(user_id,source,json);
      return console.log(`Unfollowed this bot: ${JSON.stringify(event)}`);

    case 'join':
      // const joinText = `Joined ${event.source.type}`;
      // return client.replyMessage(event.replyToken, { type: 'text', text: joinText });

    case 'leave':
      return console.log(`Left: ${JSON.stringify(event)}`);

    case 'postback':
      /* Almond */
      let data = event.postback.data;
      const postbackText = `${data}`;
      console.log("postback",req);
      console.log("postbackText",postbackText)
      var event_postback = event;
      // var almond_postbackText = ["ยืนยัน","ยกเลิก","CHANGE_QUEUE","เลือกบริการ"];
      // if(almond_postbackText.includes(postbackText)){
          console.log("event_postback",event_postback);
          delete req.headers['host'];
          var options = {
              url: 'https://shelllineservices.shellcampaigns.com/api/webhook',
              method: 'POST',
              headers: req.headers,
              body: JSON.stringify(req.body),
          }; 
          return request(options, function (error, response) {
            if (error) throw new Error(error);
            console.log(response.body);
          });
      // }
      /* end */
    case 'beacon':
      // const beaconText = `${event.beacon.type} beacon hwid : ${event.beacon.hwid} with device message = ${dm}`;
      // return client.replyMessage(event.replyToken, { type: 'text', text: beaconText });
    default:
      throw new Error(`Unknown event: ${JSON.stringify(event)}`);
  }
}

async function sendEvent(event) {
  console.log(event);
}

async function updateTD(table, data) {
  var config = {
      method: 'post',
      url: 'https://in.treasuredata.com/postback/v3/event/dtl_shell/' + table,
      headers: { 
        'Content-Type': 'application/json', 
        'X-TD-Write-Key': '9629/f02dd56fd8f4c7badb3afad0641c10228d2ee22a'
      },
      data : data
    };
    
    const res = await axios(config);

    return res.status;

}

async function handleText(req,message, event) {
  var keyword = message.text;
  var userId = event.source.userId;
  var source = "text";
  var json = JSON.stringify(event);
  var resultinsert = await service.insertShellResponseLog(userId,source,json);
  var resultinsert2 = await service.insertPersonalizeResponseTD(userId,source,keyword);
  
  console.log('value : ', resultinsert2);
  
  

 console.log('resultinsert2', resultinsert2.insertId);
  var ondate = new Date().toISOString().replace('T', ' ').substring(0, 19);
  var value = {
    'id': resultinsert2.insertId,
    'line_user_id': userId,
    'response_source': source,
    'response_keyword': keyword,
    'create_date': new Date()
  }
  console.log(value);
  /*
  let res = await updateTD('personal_keyword_json', value);
  if (res == 200) {
    console.log(value);
      console.log('send data to td');
  }
  */
  

  if(message.text == "ค้นหาปั๊มเชลล์"){
    var richmenu = {
      "type": "imagemap",
      "baseUrl": "https://shellconnect.shellcampaigns.com/uploads/richmenu/findstation_imgmap.jpg?_ignored=",
      "altText": "ค้นหาปั๊มเชลล์",
      "baseSize": {
        "width": 1040,
        "height": 1040
      },
      "actions": [
        {
          "type": "uri",
          "area": {
            "x": 47,
            "y": 45,
            "width": 948,
            "height": 458
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Fwww.shell.co.th%252Fth_th%252Fmotorists%252Fshell-station-locator.html%2523iframe%253DLz9sb2NhbGU9dGhfVEgjL0AxMy4wMzc0NSw5Ni4yMjc3Niw2eg%253Futm%253Dstationsearch&messageID=-1"
        },
        {
          "type": "uri",
          "area": {
            "x": 47,
            "y": 546,
            "width": 457,
            "height": 452
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Fwww.google.com%252Fmaps%252Fd%252Fviewer%253Fmid%253D1KafHXaOU0BcBMMRWWtrhrtBYbCE%2526shorturl%253D1%2526utm%253Dstationsearch&messageId=-1"
        },
        {
          "type": "uri",
          "area": {
            "x": 539,
            "y": 544,
            "width": 458,
            "height": 454
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Fshellretaillocator.geoapp.me%252F%253Flocale%253Dth_TH%2526prefilters%253Damenities.selectshop%2526utm%253Dstationsearch&messageID=-1"
        }
      ]
    }
    return client.replyMessage(event.replyToken, richmenu);
  }

  if(message.text == "โปรแรงประจำเดือน"){
    var richmenu = {
      "type": "imagemap",
      "baseUrl": "https://shellconnect.shellcampaigns.com/uploads/042021/richmenu-19.png?_ignored=",
      "altText": "โปรแรงเกินคุ้ม (test)",
      "baseSize": {
        "width": 1040,
        "height": 2080
      },
      "actions": [
        {
          "type": "uri",
          "area": {
            "x": 66,
            "y": 296,
            "width": 300,
            "height": 136
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL?t=18032021&utm=promo&messageId=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1160594020209063375"
        },
        {
          "type": "uri",
          "area": {
            "x": 380,
            "y": 297,
            "width": 284,
            "height": 136
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL?t=18032021&utm=promo&messageId=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1160533540209064311"
        },
        {
          "type": "uri",
          "area": {
            "x": 685,
            "y": 294,
            "width": 284,
            "height": 142
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL?t=18032021&utm=promo&messageId=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1160549740309063546"
        },
        {
          "type": "uri",
          "area": {
            "x": 73,
            "y": 456,
            "width": 291,
            "height": 135
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL?t=18032021&utm=promo&messageId=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1159011430209063170"
        },
        {
          "type": "uri",
          "area": {
            "x": 379,
            "y": 454,
            "width": 282,
            "height": 137
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL?t=18032021&utm=promo&messageId=-1&url=https://www.shellcampaigns.com/shellroadsideassistance"
        },
        {
          "type": "uri",
          "area": {
            "x": 680,
            "y": 456,
            "width": 291,
            "height": 136
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL?t=18032021&utm=promo&messageId=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1159410700309066240"
        },
        {
          "type": "uri",
          "area": {
            "x": 70,
            "y": 615,
            "width": 292,
            "height": 138
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL?t=18032021&utm=promo&messageId=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1161732608909067356"
        },
        {
          "type": "uri",
          "area": {
            "x": 380,
            "y": 613,
            "width": 286,
            "height": 139
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL?t=18032021&utm=promo&messageId=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1160983000409061045"
        },
        {
          "type": "uri",
          "area": {
            "x": 681,
            "y": 611,
            "width": 290,
            "height": 140
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL?t=18032021&utm=promo&messageId=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1161335770209063422"
        },
        {
          "type": "uri",
          "area": {
            "x": 72,
            "y": 779,
            "width": 286,
            "height": 135
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL?t=18032021&utm=promo&messageId=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1161413560709063305"
        },
        {
          "type": "uri",
          "area": {
            "x": 685,
            "y": 776,
            "width": 285,
            "height": 135
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL?t=18032021&utm=promo&messageId=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1161689509309063460"
        },
        {
          "type": "uri",
          "area": {
            "x": 532,
            "y": 1100,
            "width": 288,
            "height": 138
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL?t=18032021&utm=promo&messageId=-1&url=https://www.shell.co.th/travel-together"
        },
        {
          "type": "uri",
          "area": {
            "x": 222,
            "y": 1449,
            "width": 292,
            "height": 135
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL?t=18032021&utm=promo&messageId=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1161456580209066418"
        },
        {
          "type": "uri",
          "area": {
            "x": 533,
            "y": 1450,
            "width": 289,
            "height": 136
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL?t=18032021&utm=promo&messageId=-1&url=https://www.shell.co.th/travel-together"
        },
        {
          "type": "uri",
          "area": {
            "x": 71,
            "y": 1768,
            "width": 288,
            "height": 134
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL?t=18032021&utm=promo&messageId=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1159479100209062919"
        },
        {
          "type": "uri",
          "area": {
            "x": 378,
            "y": 1766,
            "width": 283,
            "height": 133
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL?t=18032021&utm=promo&messageId=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1161517060309069553"
        },
        {
          "type": "uri",
          "area": {
            "x": 682,
            "y": 1766,
            "width": 291,
            "height": 134
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL?t=18032021&utm=promo&messageId=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1161594820409064065"
        },
        {
          "type": "uri",
          "area": {
            "x": 227,
            "y": 1101,
            "width": 284,
            "height": 134
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL?t=18032021&utm=promo&messageId=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1161577600109068316"
        },
        {
          "type": "uri",
          "area": {
            "x": 376,
            "y": 778,
            "width": 288,
            "height": 135
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL?t=18032021&utm=promo&messageId=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1161655300209065578"
        },
        {
          "type": "uri",
          "area": {
            "x": 225,
            "y": 1924,
            "width": 283,
            "height": 136
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL?t=18032021&utm=promo&messageId=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1161767641609068206"
        },
        {
          "type": "uri",
          "area": {
            "x": 534,
            "y": 1926,
            "width": 282,
            "height": 137
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL?t=18032021&utm=promo&messageId=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1161853302309069751"
        }
      ]
    }
    return client.replyMessage(event.replyToken, richmenu);
  }

  if(message.text == "อิ่มอร่อยกับเชลล์ชวนชิม"){
    var richmenu = {
        "type": "imagemap",
        "baseUrl": "https://shellconnect.shellcampaigns.com/uploads/richmenu/sss_20201030.jpg?_ignored=",
        "altText": "อิ่มอร่อยกับเชลล์ชวนชิม",
        "baseSize": {
            "width": 1040,
            "height": 1500
        },
        "actions": [{
            "type": "uri",
            "area": {
                "x": 82,
                "y": 82,
                "width": 417,
                "height": 419
            },
            "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Fshellshuanshim.com%252F%253Futm%253Dsss&messageId=-1"
        }, {
            "type": "uri",
            "area": {
                "x": 544,
                "y": 81,
                "width": 418,
                "height": 420
            },
            "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Fshellshuanshim.com%252Fpromotion%252Flist%253Ftype%253Dclubsmart%2526utm%253Dsss&messageId=-1"
        }, {
            "type": "uri",
            "area": {
                "x": 78,
                "y": 539,
                "width": 419,
                "height": 419
            },
            "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Fshellshuanshim.com%252Fsearch%252Fsearch-result%253Fkeyword%253Dnull%2526cate%253Dall%2526type%253Dall%2526location%253D13.721528573443024%252C100.55992780193273%2526minPrice%253D0%2526maxPrice%253D3000%2526nearby%253D13.721528573443024%252C100.55992780193273%2526page%253D1%2526utm%253Dsss&messageId=-1"
        }, {
            "type": "uri",
            "area": {
                "x": 542,
                "y": 541,
                "width": 418,
                "height": 416
            },
            "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Fshellshuanshim.com%252Froute%253Futm%253Dsss&messageId=-1"
        }, {
            "type": "message",
            "area": {
                "x": 81,
                "y": 996,
                "width": 878,
                "height": 387
            },
            "text": "เกี่ยวกับเชลล์ชวนชิม"
        }]
    }
    return client.replyMessage(event.replyToken, richmenu);
  }

  if(message.text == 'getid') {
    var richtext = {
      "type": "text",
      "text": userId
    }
    return client.replyMessage(event.replyToken, richtext);

  }
  if(message.text == "เกี่ยวกับเชลล์ชวนชิม"){
    var richmenu = {
        "type": "imagemap",
        "baseUrl": "https://shellconnect.shellcampaigns.com/uploads/richmenu/about_sss_imgmap.jpg?_ignored=",
        "altText": "เกี่ยวกับเชลล์ชวนชิม",
        "baseSize": {
          "width": 1040,
          "height": 1040
        },
        "actions": [
          {
            "type": "uri",
            "area": {
              "x": 54,
              "y": 252,
              "width": 447,
              "height": 317
            },
            "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Fshellshuanshim.com%252Fabout%253Futm%253Dsss&messageId=-1"
          },
          {
            "type": "uri",
            "area": {
              "x": 539,
              "y": 252,
              "width": 449,
              "height": 315
            },
            "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Fshellshuanshim.com%252Fabout%252Frestaurant-standard%253Futm%253Dsss&messageId=-1"
          },
          {
            "type": "uri",
            "area": {
              "x": 55,
              "y": 596,
              "width": 446,
              "height": 314
            },
            "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Fshellshuanshim.com%252Fsss-team%253Futm%253Dsss&messageId=-1"
          },
          {
            "type": "uri",
            "area": {
              "x": 541,
              "y": 598,
              "width": 447,
              "height": 311
            },
            "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Fshellshuanshim.com%252Fshuan-shell-shim%253Futm%253Dsss&messageId=-1"
          }
        ]
    }
    return client.replyMessage(event.replyToken, richmenu);
  }

  if(message.text == "testpro"){
    var richmenu = {
      "type": "imagemap",
      "baseUrl": "https://shellconnect.shellcampaigns.com/uploads/072021/hotpro-15.png?_ignore=",
      "altText": "(TEST) โปรแรงเกินคุ้ม",
      "baseSize": {
        "width": 1040,
        "height": 2080
      },
      "actions": [
        {
          "type": "uri",
          "area": {
            "x": 71,
            "y": 297,
            "width": 290,
            "height": 136
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1160594020209063375&utm=hotprogoplus"
        },
        {
          "type": "uri",
          "area": {
            "x": 380,
            "y": 297,
            "width": 284,
            "height": 136
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1160549740309063546&utm=hotprogoplus"
        },
        {
          "type": "uri",
          "area": {
            "x": 685,
            "y": 294,
            "width": 284,
            "height": 142
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1159011430209063170&utm=hotprogoplus"
        },
        {
          "type": "uri",
          "area": {
            "x": 73,
            "y": 456,
            "width": 291,
            "height": 135
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://www.shellcampaigns.com/shellroadsideassistance&utm=hotprogoplus"
        },
        {
          "type": "uri",
          "area": {
            "x": 379,
            "y": 454,
            "width": 282,
            "height": 137
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1160681140509065408&utm=hotprogoplus"
        },
        {
          "type": "uri",
          "area": {
            "x": 680,
            "y": 456,
            "width": 291,
            "height": 136
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1161335770209063422&utm=hotprogoplus"
        },
        {
          "type": "uri",
          "area": {
            "x": 70,
            "y": 615,
            "width": 292,
            "height": 138
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1161655300209065578&utm=hotprogoplus"
        },
        {
          "type": "uri",
          "area": {
            "x": 380,
            "y": 613,
            "width": 286,
            "height": 139
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1162060843109069382&utm=hotprogoplus"
        },
        {
          "type": "uri",
          "area": {
            "x": 681,
            "y": 611,
            "width": 290,
            "height": 140
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1162372248909062815&utm=hotprogoplus"
        },
        {
          "type": "uri",
          "area": {
            "x": 374,
            "y": 1263,
            "width": 288,
            "height": 147
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://www.shell.co.th/travel-together&utm=hotprodeliselect"
        },
        {
          "type": "uri",
          "area": {
            "x": 678,
            "y": 1264,
            "width": 292,
            "height": 144
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1162408695209067111&utm=hotprodeliselect"
        },
        {
          "type": "uri",
          "area": {
            "x": 222,
            "y": 1432,
            "width": 289,
            "height": 140
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1162623023409069432&utm=hotprodeliselect"
        },
        {
          "type": "uri",
          "area": {
            "x": 528,
            "y": 1439,
            "width": 288,
            "height": 141
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1162691562609062737&utm=hotprodeliselect"
        },
        {
          "type": "uri",
          "area": {
            "x": 71,
            "y": 1267,
            "width": 284,
            "height": 142
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1161456580209066418&utm=hotprodeliselect"
        },
        {
          "type": "uri",
          "area": {
            "x": 375,
            "y": 927,
            "width": 288,
            "height": 135
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://www.shell.co.th/travel-together&utm=hotprofuellube"
        },
        {
          "type": "uri",
          "area": {
            "x": 73,
            "y": 1761,
            "width": 286,
            "height": 140
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1159479100209062919&utm=hotproback"
        },
        {
          "type": "uri",
          "area": {
            "x": 374,
            "y": 1765,
            "width": 286,
            "height": 135
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1161767641609068206&utm=hotproback"
        },
        {
          "type": "uri",
          "area": {
            "x": 683,
            "y": 1761,
            "width": 286,
            "height": 144
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1162337500409064272&utm=hotproback"
        },
        {
          "type": "uri",
          "area": {
            "x": 222,
            "y": 1924,
            "width": 288,
            "height": 135
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1162389888009063428&utm=hotproback"
        },
        {
          "type": "uri",
          "area": {
            "x": 527,
            "y": 1924,
            "width": 289,
            "height": 137
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1162556483909068603&utm=hotproback"
        }
      ]
    }
    return client.replyMessage(event.replyToken, richmenu);
  }

  if(message.text == "test_sss"){
    var richmenu = {
        "type": "imagemap",
        "baseUrl": "https://shellconnect.shellcampaigns.com/uploads/richmenu/sss_20201030.jpg?_ignored=",
        "altText": "อิ่มอร่อยกับเชลล์ชวนชิม",
        "baseSize": {
            "width": 1040,
            "height": 1500
        },
        "actions": [{
            "type": "uri",
            "area": {
                "x": 82,
                "y": 82,
                "width": 417,
                "height": 419
            },
            "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Fshellshuanshim.com%252F%253Futm%253Dsss&messageId=-1"
        }, {
            "type": "uri",
            "area": {
                "x": 544,
                "y": 81,
                "width": 418,
                "height": 420
            },
            "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Fshellshuanshim.com%252Fpromotion%252Flist%253Ftype%253Dclubsmart%2526utm%253Dsss&messageId=-1"
        }, {
            "type": "uri",
            "area": {
                "x": 78,
                "y": 539,
                "width": 419,
                "height": 419
            },
            "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Fshellshuanshim.com%252Fsearch%252Fsearch-result%253Fkeyword%253Dnull%2526cate%253Dall%2526type%253Dall%2526location%253D13.721528573443024%252C100.55992780193273%2526minPrice%253D0%2526maxPrice%253D3000%2526nearby%253D13.721528573443024%252C100.55992780193273%2526page%253D1%2526utm%253Dsss&messageId=-1"
        }, {
            "type": "uri",
            "area": {
                "x": 542,
                "y": 541,
                "width": 418,
                "height": 416
            },
            "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Fshellshuanshim.com%252Froute%253Futm%253Dsss&messageId=-1"
        }, {
            "type": "message",
            "area": {
                "x": 81,
                "y": 996,
                "width": 878,
                "height": 387
            },
            "text": "เกี่ยวกับเชลล์ชวนชิม"
        }]
    }
    return client.replyMessage(event.replyToken, richmenu);
  }

  if(message.text == "ชอบดื่มชา"){
    var richtext = {
      "type": "imagemap",
      "baseUrl": "https://shellconnect.shellcampaigns.com/uploads/delicafe/horoscope_tea.jpg?_ignored=",
      "altText": "ชอบดื่มชา",
      "baseSize": {
        "width": 1040,
        "height": 1040
      },
      "actions": []
    }
    return client.replyMessage(event.replyToken, richtext);
  }

  if(message.text == "ชอบดื่มนม"){
    var richtext = {
      "type": "imagemap",
      "baseUrl": "https://shellconnect.shellcampaigns.com/uploads/delicafe/horoscope_milk.jpg?_ignored=",
      "altText": "ชอบดื่มนม",
      "baseSize": {
        "width": 1040,
        "height": 1040
      },
      "actions": []
    }
    return client.replyMessage(event.replyToken, richtext);
  }

  if(message.text == "ชอบดื่มกาแฟ"){
    var richtext = {
      "type": "imagemap",
      "baseUrl": "https://shellconnect.shellcampaigns.com/uploads/delicafe/horoscope_coffee.jpg?_ignored=",
      "altText": "ชอบดื่มกาแฟ",
      "baseSize": {
        "width": 1040,
        "height": 1040
      },
      "actions": []
    }
    return client.replyMessage(event.replyToken, richtext);
  }

  if(message.text == "r1"){
    var richtext = {
      "type": "text",
      "text": "Richmenu v1 "
    }
    client.unlinkRichMenuFromUser(userId);
    return client.replyMessage(event.replyToken, richtext);
  }

  if(message.text == "r2"){
    var richtext = {
      "type": "text",
      "text": "Switch to new richmenu"
    }
    //8615807e4946579fa898436078f8c93c
    //
    client.linkRichMenuToUser(userId, 'richmenu-582cdb633b496b5abeb5614c21a0bc59');
    return client.replyMessage(event.replyToken, richtext);
  }

  if(message.text == 'vdoimage') {
    var richtext = {"type":"imagemap","baseUrl":"https://shellconnect.shellcampaigns.com/uploads/032021/25.jpg?_ignored=","altText":"พร้อมไปต่อทุกเส้นทาง ด้วย เชลล์ เฮลิกส์ HX7 ดีเซล คู่แกร่งสายลุย","baseSize":{"width":1040,"height":1040},"video":{"originalContentUrl":"https://player.vimeo.com/external/527712777.hd.mp4?s=c70993e6f36513d10bd5bb897d51b47eb1080cd0&profile_id=174","previewImageUrl":"https://shellconnect.shellcampaigns.com/uploads/032021/25-preview.png","area":{"x":0,"y":190,"width":1040,"height":586}},"actions":[{"type":"uri","area":{"x":0,"y":586,"width":1040,"height":714},"linkUri":"https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Ftimeline.line.me%252Fpost%252F1161649089810016703&messageId=162"}]}
    return client.replyMessage(event.replyToken, richtext);
  }

  if (message.text == 'พิกัดร้านเด็ด เชลล์ชวนชิม') {
    var richmenu = {
      "type": "imagemap",
      "baseUrl": "https://shellconnect.shellcampaigns.com/uploads/richmenu/shell-eat.png?_ignored=",
      "altText": "เชลล์ชวนชิม",
      "baseSize": {
        "width": 1040,
        "height": 1338
      },
      "actions": [
        {
          "type": "uri",
          "area": {
            "x": 0,
            "y": 0,
            "width": 518,
            "height": 511
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%3A%2F%2Fshellshuanshim.com%2F%3Futm%3Dsss&messageID=-1"
        },
        {
          "type": "uri",
          "area": {
            "x": 518,
            "y": 0,
            "width": 519,
            "height": 513
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%3A%2F%2Fshellshuanshim.com%2Fpromotion%2Flist%3Ftype%3Dclubsmart%26utm%3Dsss&messageID=-1"
        },
        {
          "type": "uri",
          "area": {
            "x": 2,
            "y": 513,
            "width": 515,
            "height": 485
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%3A%2F%2Fshellshuanshim.com%2Fsearch%2Fsearch-result%3Fkeyword%3Dnull%26cate%3Dall%26type%3Dall%26location%3D13.721528573443024%2C100.55992780193273%26minPrice%3D0%26maxPrice%3D3000%26nearby%3D13.721528573443024%2C100.55992780193273%26page%3D1&messageID=-1"
        },
        {
          "type": "uri",
          "area": {
            "x": 520,
            "y": 513,
            "width": 515,
            "height": 484
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%3A%2F%2Fshellshuanshim.com%2Froute%3Futm%3Dsss&messageID=-1"
        },
        {
          "type": "uri",
          "area": {
            "x": 2,
            "y": 1004,
            "width": 1038,
            "height": 334
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%3A%2F%2Fshellshuanshim.com%2Fabout&messageID=-1"
        }
      ]
    }
    return client.replyMessage(event.replyToken, richmenu);
  }

  if (message.text == '#พิกัดเด็ด เชลล์ชวนชิม') {
    var richmenu = {
      "type": "imagemap",
      "baseUrl": "https://shellconnect.shellcampaigns.com/uploads/richmenu/shell-eat.png?_ignored=",
      "altText": "เชลล์ชวนชิม",
      "baseSize": {
        "width": 1040,
        "height": 1338
      },
      "actions": [
        {
          "type": "uri",
          "area": {
            "x": 0,
            "y": 0,
            "width": 518,
            "height": 511
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%3A%2F%2Fshellshuanshim.com%2F%3Futm%3Dsss&messageID=-1&utm=sss&utm_content=sss_newrestaurant"
        },
        {
          "type": "uri",
          "area": {
            "x": 518,
            "y": 0,
            "width": 519,
            "height": 513
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%3A%2F%2Fshellshuanshim.com%2Fpromotion%2Flist%3Ftype%3Dclubsmart%26utm%3Dsss&messageID=-1&utm=sss&utm_content=sss_pro_member"
        },
        {
          "type": "uri",
          "area": {
            "x": 2,
            "y": 513,
            "width": 515,
            "height": 485
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%3A%2F%2Fshellshuanshim.com%2Fsearch%2Fsearch-result%3Fkeyword%3Dnull%26cate%3Dall%26type%3Dall%26location%3D13.721528573443024%2C100.55992780193273%26minPrice%3D0%26maxPrice%3D3000%26nearby%3D13.721528573443024%2C100.55992780193273%26page%3D1&messageID=-1&utm=sss&utm_content=sss_find_location"
        },
        {
          "type": "uri",
          "area": {
            "x": 520,
            "y": 513,
            "width": 515,
            "height": 484
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%3A%2F%2Fshellshuanshim.com%2Froute%3Futm%3Dsss&messageID=-1&utm=sss&utm_content=sss_route"
        },
        {
          "type": "uri",
          "area": {
            "x": 2,
            "y": 1004,
            "width": 1038,
            "height": 334
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%3A%2F%2Fshellshuanshim.com%2Fabout&messageID=-1&utm=sss&utm_content=sss_aboutsss"
        }
      ]
    }
    return client.replyMessage(event.replyToken, richmenu);
  }

  if (message.text == 'โปรแรงสุดคุ้มโดนใจ') {
    var richmenu = {
      "type": "imagemap",
      "baseUrl": "https://shellconnect.shellcampaigns.com/uploads/richmenu-v2/hot-pro.png?_ignore=",
      "altText": "โปรแรงสุดคุ้มโดนใจ",
      "baseSize": {
        "width": 1040,
        "height": 1027
      },
      "actions": [
        {
          "type": "message",
          "area": {
            "x": 0,
            "y": 0,
            "width": 513,
            "height": 504
          },
          "text": "ดีลโดนใจ"
        },
        {
          "type": "message",
          "area": {
            "x": 518,
            "y": 0,
            "width": 519,
            "height": 506
          },
          "text": "โปรน้ำมันเชลล์และน้ำมันเครื่องสุดคุ้ม"
        },
        {
          "type": "message",
          "area": {
            "x": 0,
            "y": 513,
            "width": 515,
            "height": 514
          },
          "text": "โปรสดชื่นเต็มอิ่ม"
        },
        {
          "type": "message",
          "area": {
            "x": 520,
            "y": 515,
            "width": 520,
            "height": 512
          },
          "text": "โปรบัตรเครดิต"
        }
      ]
    };
    return client.replyMessage(event.replyToken, richmenu);
  }

  if (message.text == '#โปรแรงสุดคุ้มโดนใจ') {
    var richmenu = {
      "type": "imagemap",
      "baseUrl": "https://shellconnect.shellcampaigns.com/uploads/richmenu-v2/hot-pro.png?_ignore=",
      "altText": "โปรแรงสุดคุ้มโดนใจ",
      "baseSize": {
        "width": 1040,
        "height": 1027
      },
      "actions": [
        {
          "type": "message",
          "area": {
            "x": 0,
            "y": 0,
            "width": 513,
            "height": 504
          },
          "text": "#ดีลโดนใจ"
        },
        {
          "type": "message",
          "area": {
            "x": 518,
            "y": 0,
            "width": 519,
            "height": 506
          },
          "text": "#โปรน้ำมันเชลล์และน้ำมันเครื่องสุดคุ้ม"
        },
        {
          "type": "message",
          "area": {
            "x": 0,
            "y": 513,
            "width": 515,
            "height": 514
          },
          "text": "#โปรสดชื่นเต็มอิ่ม"
        },
        {
          "type": "message",
          "area": {
            "x": 520,
            "y": 515,
            "width": 520,
            "height": 512
          },
          "text": "#โปรบัตรเครดิต"
        }
      ]
    };
    return client.replyMessage(event.replyToken, richmenu);
  }

  if (message.text == 'โปรน้ำมันเชลล์และน้ำมันเครื่องสุดคุ้มx') {
    var richmenu = {
      "type": "imagemap",
      "baseUrl": "https://shellconnect.shellcampaigns.com/uploads/102021/fuel.png?_ignore=",
      "altText": "โปรน้ำมันเชลล์และน้ำมันเครื่องสุดคุ้ม",
      "baseSize": {
        "width": 1040,
        "height": 788
      },
      "actions": [
        {
          "type": "uri",
          "area": {
            "x": 14,
            "y": 230,
            "width": 512,
            "height": 544
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://www.shell.co.th/travel-together/?utm=hotprofuellube&utm_content=bigday21_vpowerx2"
        },
        {
          "type": "uri",
          "area": {
            "x": 527,
            "y": 219,
            "width": 513,
            "height": 549
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://www.shell.co.th/th_th/motorists/offers-and-competitions/mobility-promotion.html?utm=hotprofuellube&utm_content=mobility21_umbrella"
        }
      ]
    }
    return client.replyMessage(event.replyToken, richmenu);
  }

  if (message.text == 'โปรน้ำมันเชลล์และน้ำมันเครื่องสุดคุ้ม') {
    var richmenu = {
      "type": "imagemap",
      "baseUrl": "https://shellconnect.shellcampaigns.com/uploads/112021/fuel.png?_ignore=",
      "altText": "โปรน้ำมันเชลล์และน้ำมันเครื่องสุดคุ้ม",
      "baseSize": {
        "width": 1040,
        "height": 788
      },
      "actions": [
        {
          "type": "uri",
          "area": {
            "x": 14,
            "y": 230,
            "width": 512,
            "height": 544
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageId=-1&url=https://www.shell.co.th/travel-together/?utm=hotprofuellube&utm_content=bigday21_vpowerx2"
        },
        {
          "type": "uri",
          "area": {
            "x": 527,
            "y": 219,
            "width": 513,
            "height": 549
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageId=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1163420547809065171?utm=hotprofuellube&utm_content=bigday21_shocplus"
        }
      ]
    }
    
    
    return client.replyMessage(event.replyToken, richmenu);
  }

  if (message.text == '#โปรน้ำมันเชลล์และน้ำมันเครื่องสุดคุ้ม') {
    var richmenu = {
      "type": "imagemap",
      "baseUrl": "https://shellconnect.shellcampaigns.com/uploads/032022/fuel.png?_ignore=",
      "altText": "โปรน้ำมันเชลล์และน้ำมันเครื่องสุดคุ้ม",
      "baseSize": {
        "width": 1040,
        "height": 766
      },
      "actions": [
        {
          "type": "uri",
          "area": {
            "x": 14,
            "y": 230,
            "width": 335,
            "height": 544
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Flinevoom.line.me%252Fpost%252F1164607141509060695&messageID=-1"
        },
        {
          "type": "uri",
          "area": {
            "x": 354,
            "y": 216,
            "width": 338,
            "height": 549
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%3A%2F%2Flinevoom.line.me%2Fpost%2F1164514828709064846%3Futm%3Dhotprofuellube%26utm_content%3Dfuellube_helixpro&messageID=-1"
        },
        {
          "type": "uri",
          "area": {
            "x": 692,
            "y": 218,
            "width": 342,
            "height": 548
          },
          "linkUri": "https://linevoom.line.me/post/1164508360209068577?utm=hotprofuellube&utm_content=fuellube_shocplus_summerpro"
        }
      ]
    }
    
    
    return client.replyMessage(event.replyToken, richmenu);
  }

  if (message.text == 'โปรสดชื่นเต็มอิ่ม1') {
    var richmenu = {
      "type": "imagemap",
      "baseUrl": "https://shellconnect.shellcampaigns.com/uploads/102021/delicafev2.png?_ignore=",
      "altText": "โปรสดชื่นเต็มอิ่ม",
      "baseSize": {
        "width": 1040,
        "height": 768
      },
      "actions": [
        {
          "type": "uri",
          "area": {
            "x": 15,
            "y": 233,
            "width": 501,
            "height": 520
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://www.shell.co.th/travel-together/?utm=hotprodeliselect&utm_content=bigday21_holiday50discount"
        },
        {
          "type": "uri",
          "area": {
            "x": 522,
            "y": 232,
            "width": 515,
            "height": 520
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=shttps://www.shell.co.th/th_th/motorists/offers-and-competitions/mobility-promotion.html?utm=hotprodeliselect&utm_content=mobility21_umbrella"
        }
      ]
    }
    return client.replyMessage(event.replyToken, richmenu);
  }

  if (message.text == 'โปรสดชื่นเต็มอิ่ม') {
    var richmenu = 
    {
        "type": "imagemap",
        "baseUrl": "https://shellconnect.shellcampaigns.com/uploads/112021/delicafe.png?_ignore=",
        "altText": "โปรสดชื่นเต็มอิ่ม",
        "baseSize": {
          "width": 1040,
          "height": 577
        },
        "actions": [
          {
            "type": "uri",
            "area": {
              "x": 15,
              "y": 233,
              "width": 1009,
              "height": 324
            },
            "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageId=-1&url=https://www.shell.co.th/travel-together/?utm=hotprodeliselect&utm_content=bigday21_holiday50discount"
          }
        ]
      }
    return client.replyMessage(event.replyToken, richmenu);
  }

  if (message.text == '#โปรสดชื่นเต็มอิ่ม') {
    var richmenu = {
      "type": "imagemap",
      "baseUrl": "https://shellconnect.shellcampaigns.com/uploads/032022/creditcard.png?_ignore=",
      "altText": "โปรสดชื่นเต็มอิ่ม",
      "baseSize": {
        "width": 1040,
        "height": 577
      },
      "actions": [
        {
          "type": "uri",
          "area": {
            "x": 15,
            "y": 233,
            "width": 1023,
            "height": 327
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%3A%2F%2Flinevoom.line.me%2Fpost%2F1164326161309066840%3Futm%3Dhotprodeliselect%26utm_content%3Ddeliselect_croissant&messageID=-1"
        }
      ]
    }
    
    return client.replyMessage(event.replyToken, richmenu);
  }


  if (message.text == 'โปรบัตรเครดิตx') {
    var richmenu = {
      "type": "imagemap",
      "baseUrl": "https://shellconnect.shellcampaigns.com/uploads/102021/creditcard.png?_ignore=",
      "altText": "โปรบัตรเครดิต",
      "baseSize": {
        "width": 1040,
        "height": 1301
      },
      "actions": [
        {
          "type": "uri",
          "area": {
            "x": 15,
            "y": 237,
            "width": 327,
            "height": 516
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1159479100209062919?utm=hotprobank&utm_content=creditcard_scbm3payback"
        },
        {
          "type": "uri",
          "area": {
            "x": 357,
            "y": 232,
            "width": 329,
            "height": 522
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1161767641609068206?utm=hotprobank&utm_content=creditcard_citi_payback205"
        },
        {
          "type": "uri",
          "area": {
            "x": 693,
            "y": 231,
            "width": 329,
            "height": 525
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1162855333409066657?utm=hotprobank&utm_content=creditcard_citi_payback10"
        },
        {
          "type": "uri",
          "area": {
            "x": 18,
            "y": 766,
            "width": 329,
            "height": 518
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1163166343509062689?utm=hotprobank&utm_content=creditcard_ktc_ecoupon20thb"
        },
        {
          "type": "uri",
          "area": {
            "x": 357,
            "y": 767,
            "width": 326,
            "height": 524
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1163097381609068228?utm=hotprobank&utm_content=creditcard_t1_ecoupon100"
        },
        {
          "type": "uri",
          "area": {
            "x": 703,
            "y": 764,
            "width": 337,
            "height": 537
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1163062947209066883?utm=hotprobank&utm_content=creditcard_kcc_payback10"
        }
      ]
    }
    return client.replyMessage(event.replyToken, richmenu);

  }


  if (message.text == 'โปรบัตรเครดิต') {
    var richmenu =
    {
      "type": "imagemap",
      "baseUrl": "https://shellconnect.shellcampaigns.com/uploads/112021/creditcard.png?_ignore=",
      "altText": "โปรบัตรเครดิต",
      "baseSize": {
        "width": 1040,
        "height": 1300
      },
      "actions": [
        {
          "type": "uri",
          "area": {
            "x": 15,
            "y": 237,
            "width": 327,
            "height": 516
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageId=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1159479100209062919?utm=hotprobank&utm_content=creditcard_scbm3payback"
        },
        {
          "type": "uri",
          "area": {
            "x": 357,
            "y": 232,
            "width": 329,
            "height": 522
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageId=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1161767641609068206?utm=hotprobank&utm_content=creditcard_citi_payback205"
        },
        {
          "type": "uri",
          "area": {
            "x": 693,
            "y": 231,
            "width": 329,
            "height": 525
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageId=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1163166343509062689?utm=hotprobank&utm_content=creditcard_ktc_ecoupon20thb"
        },
        {
          "type": "uri",
          "area": {
            "x": 18,
            "y": 766,
            "width": 329,
            "height": 518
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageId=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1163097381609068228?utm=hotprobank&utm_content=creditcard_t1_ecoupon100"
        },
        {
          "type": "uri",
          "area": {
            "x": 358,
            "y": 764,
            "width": 326,
            "height": 524
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageId=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1163402943809066365? utm=hotprobank&utm_content=creditcard_kcc_cashback4all"
        },
        {
          "type": "uri",
          "area": {
            "x": 694,
            "y": 759,
            "width": 337,
            "height": 526
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageId=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1163460743709066271? utm=hotprobank&utm_content=creditcard_scb_mastercardcashback10"
        }
      ]
    }
    return client.replyMessage(event.replyToken, richmenu);
  }


  if (message.text == '#โปรบัตรเครดิต') {
    var richmenu = {
      "type": "imagemap",
      "baseUrl": "https://shellconnect.shellcampaigns.com/uploads/032022/delicafe.png?_ignore=",
      "altText": "โปรบัตรเครดิต",
      "baseSize": {
        "width": 1040,
        "height": 769
      },
      "actions": [
        {
          "type": "uri",
          "area": {
            "x": 0,
            "y": 227,
            "width": 349,
            "height": 542
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%3A%2F%2Flinevoom.line.me%2Fpost%2F1161767641609068206&messageID=-1&utm=hotprobank&utm_content=creditcard_citi_payback205"
        },
        {
          "type": "uri",
          "area": {
            "x": 350,
            "y": 227,
            "width": 336,
            "height": 542
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%3A%2F%2Flinevoom.line.me%2Fpost%2F1163166343509062689&messageID=-1&utm=hotprobank&utm_content=creditcard_ktc_ecoupon20thb"
        },
        {
          "type": "uri",
          "area": {
            "x": 693,
            "y": 223,
            "width": 347,
            "height": 546
          },
          "linkUri": "https://linevoom.line.me/post/1164307521509069462?utm=hotprobank&utm_content=creditcard_creditpoint_drinkdelicafe"
        }
      ]
    }

    
    return client.replyMessage(event.replyToken, richmenu);
  }

  if (message.text == 'ดีลโดนใจ') {
    var richmenu = {
      "type": "imagemap",
      "baseUrl": "https://shellconnect.shellcampaigns.com/uploads/102021/go-plus.png?_ignore=",
      "altText": "ดีลโดนใจ",
      "baseSize": {
        "width": 1040,
        "height": 1834
      },
      "actions": [
        {
          "type": "uri",
          "area": {
            "x": 17,
            "y": 231,
            "width": 328,
            "height": 521
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://www.shellcampaigns.com/shellroadsideassistance?utm=hotprogoplus&utm_content=goplusprivilege_rsa24h"
        },
        {
          "type": "uri",
          "area": {
            "x": 357,
            "y": 231,
            "width": 329,
            "height": 521
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1161655300209065578?utm=hotprogoplus&utm_content=goplusredeem_ferraricollection"
        },
        {
          "type": "uri",
          "area": {
            "x": 697,
            "y": 232,
            "width": 329,
            "height": 522
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1159011430209063170?utm=hotprogoplus&utm_content=goplusredeem_stationdiscount"
        },
        {
          "type": "uri",
          "area": {
            "x": 17,
            "y": 766,
            "width": 328,
            "height": 522
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1161335770209063422?utm=hotprogoplus&utm_content=goplusprivilege_citi3000fueldiscount"
        },
        {
          "type": "uri",
          "area": {
            "x": 361,
            "y": 764,
            "width": 328,
            "height": 522
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1162060843109069382?utm=hotprogoplus&utm_content=goplusredeem_shoppingcode"
        },
        {
          "type": "uri",
          "area": {
            "x": 699,
            "y": 764,
            "width": 329,
            "height": 522
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1162760726209061067?utm=hotprogoplus&utm_content=goplusredeem_tescoevoucher"
        },
        {
          "type": "uri",
          "area": {
            "x": 17,
            "y": 1303,
            "width": 326,
            "height": 517
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://www.shell.co.th/travel-together/?utm=hotprogoplus&utm_content=bigday21_vpowerx2"
        },
        {
          "type": "uri",
          "area": {
            "x": 355,
            "y": 1302,
            "width": 331,
            "height": 520
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1163040403009065068?utm=hotprogoplus&utm_content=goplusredeem_jooxgrabcode"
        }
      ]
    }
    return client.replyMessage(event.replyToken, richmenu);
  }


  if (message.text == '#ดีลโดนใจ' || message.text == '#ดีลดีประจำเดือนสำหรับสมาชิกเชลล์ โกพลัส') {
    var richmenu = {
      "type": "imagemap",
      "baseUrl": "https://shellconnect.shellcampaigns.com/uploads/032022/goplus.png?_ignore=",
      "altText": "ดีลโดนใจ",
      "baseSize": {
        "width": 1040,
        "height": 1302
      },
      "actions": [
        {
          "type": "uri",
          "area": {
            "x": 17,
            "y": 231,
            "width": 328,
            "height": 521
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%3A%2F%2Flinevoom.line.me%2Fpost%2F1161655300209065578%3Futm%3Dhotprogoplus%26utm_content%3Dgoplusredeem_ferraricollection&messageID=-1"
        },
        {
          "type": "uri",
          "area": {
            "x": 357,
            "y": 231,
            "width": 329,
            "height": 521
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%3A%2F%2Fwww.citibank.co.th%2Fth%2Fshell%2Fapply%2Flead-capture.htm%3Fecid%3DSMSHELLTHI0239%C2%A0%3Futm%3Dhotprogoplus%26utm_content%3Dgoplusprivilege_citi3000fueldiscount&messageID=-1"
        },
        {
          "type": "uri",
          "area": {
            "x": 697,
            "y": 232,
            "width": 329,
            "height": 522
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%3A%2F%2Flinevoom.line.me%2Fpost%2F1162760726209061067%3Futm%3Dhotprogoplus%26utm_content%3Dgoplusredeem_tescoevoucher&messageID=-1"
        },
        {
          "type": "uri",
          "area": {
            "x": 17,
            "y": 766,
            "width": 328,
            "height": 522
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%3A%2F%2Flinevoom.line.me%2Fpost%2F1163997132409066582%3Futm%3Dhotprogoplus%26utm_content%3Dgoplusredeem_jooxgrabcode&messageID=-1"
        },
        {
          "type": "uri",
          "area": {
            "x": 361,
            "y": 764,
            "width": 328,
            "height": 522
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%3A%2F%2Flinevoom.line.me%2Fpost%2F1163850623309066548%3Futm%3Dhotprogoplus%26utm_content%3Dgoplus_gourmet&messageID=-1"
        },
        {
          "type": "uri",
          "area": {
            "x": 699,
            "y": 764,
            "width": 329,
            "height": 522
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%3A%2F%2Flinevoom.line.me%2Fpost%2F1164427406709063223%3Futm%3Dhotprogoplus%26utm_content%3Dgoplus_topvalueonline&messageID=-1"
        }
      ]
    }
    
    return client.replyMessage(event.replyToken, richmenu);
  }

  if (message.text == 'ดีลโดนใจ1') {
    var richmenu = {
      "type": "imagemap",
      "baseUrl": "https://shellconnect.shellcampaigns.com/uploads/richmenu-v2/goplus-092021.png?_ignore=",
      "altText": "ดีลโดนใจ",
      "baseSize": {
        "width": 1040,
        "height": 1834
      },
      "actions": [
        {
          "type": "uri",
          "area": {
            "x": 17,
            "y": 231,
            "width": 328,
            "height": 521
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://www.shellcampaigns.com/shellroadsideassistance?utm=hotprogoplus"
        },
        {
          "type": "uri",
          "area": {
            "x": 357,
            "y": 231,
            "width": 329,
            "height": 521
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1161655300209065578?utm=hotprogoplus"
        },
        {
          "type": "uri",
          "area": {
            "x": 697,
            "y": 232,
            "width": 329,
            "height": 522
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1159011430209063170?utm=hotprogoplus"
        },
        {
          "type": "uri",
          "area": {
            "x": 17,
            "y": 766,
            "width": 328,
            "height": 522
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1160594020209063375?utm=hotprogoplus"
        },
        {
          "type": "uri",
          "area": {
            "x": 357,
            "y": 764,
            "width": 328,
            "height": 522
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1160549740309063546?utm=hotprogoplus"
        },
        {
          "type": "uri",
          "area": {
            "x": 699,
            "y": 764,
            "width": 329,
            "height": 522
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1161335770209063422?utm=hotprogoplus"
        },
        {
          "type": "uri",
          "area": {
            "x": 17,
            "y": 1303,
            "width": 326,
            "height": 517
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1162060843109069382?utm=hotprogoplus"
        },
        {
          "type": "uri",
          "area": {
            "x": 355,
            "y": 1302,
            "width": 331,
            "height": 520
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1162760726209061067?utm=hotprogoplus"
        },
        {
          "type": "uri",
          "area": {
            "x": 697,
            "y": 1302,
            "width": 329,
            "height": 521
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://www.shell.co.th/travel-together/?utm=hotprogoplus"
        }
      ]
    }
    return client.replyMessage(event.replyToken, richmenu);
  }

  if (message.text == 'เชลล์ โกพลัส') {
    var richmenu =  {
      "type": "imagemap",
      "baseUrl": "https://shellconnect.shellcampaigns.com/uploads/richmenu-v2/go-plus.png?_ignore=",
      "altText": "เชลล์ โกพลัส",
      "baseSize": {
        "width": 1040,
        "height": 1390
      },
      "actions": [
        {
          "type": "uri",
          "area": {
            "x": 0,
            "y": 2,
            "width": 511,
            "height": 504
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%3A%2F%2Fwww.shellgoplus.com%2Fth-th%2Fsso%2Flogin%2Fstart%3Fmode%3DREGISTER%26utm%3Dgoplus&messageID=-1"
        },
        {
          "type": "uri",
          "area": {
            "x": 518,
            "y": 2,
            "width": 522,
            "height": 508
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%3A%2F%2Fwww.shellgoplus.com%2Fth-th%2Fsso%2Flogin%2Fstart%3Fmode%3DMIGRATE%26utm%3Dgoplus&messageID=-1"
        },
        {
          "type": "uri",
          "area": {
            "x": 0,
            "y": 510,
            "width": 509,
            "height": 485
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=http%3A%2F%2Fonelink.to%2Fyhfwxy%3Futm%3Dgoplus&messageID=-1"
        },
        {
          "type": "uri",
          "area": {
            "x": 515,
            "y": 513,
            "width": 525,
            "height": 484
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Ftimeline.line.me%252Fpost%252F_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk%252F1161214841209068446%253Futm%253Dgoplus&messageID=-1"
        },
        {
          "type": "message",
          "area": {
            "x": 0,
            "y": 1007,
            "width": 1038,
            "height": 383
          },
          "text": "ดีลโดนใจ"
        }
      ]
    };
    return client.replyMessage(event.replyToken, richmenu);
  }

  if (message.text == '#เชลล์ โกพลัส') {
    var richmenu =  {
      "type": "imagemap",
      "baseUrl": "https://shellconnect.shellcampaigns.com/uploads/richmenu-v2/go-plus.png?_ignore=",
      "altText": "เชลล์ โกพลัส",
      "baseSize": {
        "width": 1040,
        "height": 1390
      },
      "actions": [
        {
          "type": "uri",
          "area": {
            "x": 0,
            "y": 2,
            "width": 511,
            "height": 504
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%3A%2F%2Fwww.shellgoplus.com%2Fth-th%2Fsso%2Flogin%2Fstart%3Fmode%3DREGISTER%26utm%3Dgoplus&messageID=-1"
        },
        {
          "type": "uri",
          "area": {
            "x": 518,
            "y": 2,
            "width": 522,
            "height": 508
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%3A%2F%2Fwww.shellgoplus.com%2Fth-th%2Fsso%2Flogin%2Fstart%3Fmode%3DMIGRATE%26utm%3Dgoplus&messageID=-1"
        },
        {
          "type": "uri",
          "area": {
            "x": 0,
            "y": 510,
            "width": 509,
            "height": 485
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=http%3A%2F%2Fonelink.to%2Fyhfwxy%3Futm%3Dgoplus&messageID=-1"
        },
        {
          "type": "uri",
          "area": {
            "x": 515,
            "y": 513,
            "width": 525,
            "height": 484
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Ftimeline.line.me%252Fpost%252F_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk%252F1161214841209068446%253Futm%253Dgoplus&messageID=-1"
        },
        {
          "type": "message",
          "area": {
            "x": 0,
            "y": 1007,
            "width": 1038,
            "height": 383
          },
          "text": "#ดีลดีประจำเดือนสำหรับสมาชิกเชลล์ โกพลัส"
        }
      ]
    };
    return client.replyMessage(event.replyToken, richmenu);
  }

  if (message.text == 'ขั้นตอนการสมัครสมาชิก เชลล์ โกพลัส') {
    var richmenu = {"type":"flex","altText":"ขั้นตอนการสมัครสมาชิกใหม่ เชลล์ โกพลัส","contents":{"type":"carousel","contents":[{"type":"bubble","hero":{"type":"image","url":"https://shellconnect.shellcampaigns.com/uploads/102021/newmember0601.png","align":"center","size":"full","aspectRatio":"1:1","aspectMode":"cover","action":{"type":"uri","uri":"https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Ftimeline.line.me%252Fpost%252F1163343264310011715&messageId=278"}}},{"type":"bubble","hero":{"type":"image","url":"https://shellconnect.shellcampaigns.com/uploads/102021/newmember0602.png","align":"center","size":"full","aspectRatio":"1:1","aspectMode":"cover","action":{"type":"uri","uri":"https://liff.line.me/1493169972-DozVrxpL/trck?url=http%253A%252F%252Fonelink.to%252Fyhfwxy&messageId=278"}}},{"type":"bubble","hero":{"type":"image","url":"https://shellconnect.shellcampaigns.com/uploads/102021/newmember0603.png","align":"center","size":"full","aspectRatio":"1:1","aspectMode":"cover","action":{"type":"uri","uri":"https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Ftimeline.line.me%252Fpost%252F1163343264310011715&messageId=278"}}},{"type":"bubble","hero":{"type":"image","url":"https://shellconnect.shellcampaigns.com/uploads/102021/newmember0604.png","align":"center","size":"full","aspectRatio":"1:1","aspectMode":"cover","action":{"type":"uri","uri":"https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Ftimeline.line.me%252Fpost%252F1163343264310011715&messageId=278"}}},{"type":"bubble","hero":{"type":"image","url":"https://shellconnect.shellcampaigns.com/uploads/102021/newmember0605.png","align":"center","size":"full","aspectRatio":"1:1","aspectMode":"cover","action":{"type":"uri","uri":"https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Ftimeline.line.me%252Fpost%252F1163343264310011715&messageId=278"}}},{"type":"bubble","hero":{"type":"image","url":"https://shellconnect.shellcampaigns.com/uploads/102021/newmember0606.png","align":"center","size":"full","aspectRatio":"1:1","aspectMode":"cover","action":{"type":"uri","uri":"https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Ftimeline.line.me%252Fpost%252F1163343264310011715&messageId=278"}}},{"type":"bubble","hero":{"type":"image","url":"https://shellconnect.shellcampaigns.com/uploads/102021/newmember0607.png","align":"center","size":"full","aspectRatio":"1:1","aspectMode":"cover","action":{"type":"uri","uri":"https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Ftimeline.line.me%252Fpost%252F1163227278209068770&messageId=265&utm_source=line&utm_medium=chat&utm_campaign=goplus2021&utm_content=250points&utm_land=linetimeline&utm_target=all"}}}]}}
    return client.replyMessage(event.replyToken, richmenu);
  }

  if (message.text == 'ขั้นตอนการย้ายบัญชีจากเชลล์ คลับสมาร์ท') {
    var richmenu = {"type":"flex","altText":"ขั้นตอนการย้ายบัญชีจากเชลล์ คลับสมาร์ท","contents":{"type":"carousel","contents":[{"type":"bubble","hero":{"type":"image","url":"https://shellconnect.shellcampaigns.com/uploads/102021/clubsmart0601.png","align":"center","size":"full","aspectRatio":"1:1","aspectMode":"cover","action":{"type":"uri","uri":"https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Ftimeline.line.me%252Fpost%252F1163343264310011715&messageId=279"}}},{"type":"bubble","hero":{"type":"image","url":"https://shellconnect.shellcampaigns.com/uploads/102021/clubsmart0602.png","align":"center","size":"full","aspectRatio":"1:1","aspectMode":"cover","action":{"type":"uri","uri":"https://liff.line.me/1493169972-DozVrxpL/trck?url=http%253A%252F%252Fonelink.to%252Fyhfwxy&messageId=279"}}},{"type":"bubble","hero":{"type":"image","url":"https://shellconnect.shellcampaigns.com/uploads/102021/clubsmart0603.png","align":"center","size":"full","aspectRatio":"1:1","aspectMode":"cover","action":{"type":"uri","uri":"https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Ftimeline.line.me%252Fpost%252F1163343264310011715&messageId=279"}}},{"type":"bubble","hero":{"type":"image","url":"https://shellconnect.shellcampaigns.com/uploads/102021/clubsmart0604.png","align":"center","size":"full","aspectRatio":"1:1","aspectMode":"cover","action":{"type":"uri","uri":"https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Ftimeline.line.me%252Fpost%252F1163343264310011715&messageId=279"}}},{"type":"bubble","hero":{"type":"image","url":"https://shellconnect.shellcampaigns.com/uploads/102021/clubsmart0605.png","align":"center","size":"full","aspectRatio":"1:1","aspectMode":"cover","action":{"type":"uri","uri":"https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Ftimeline.line.me%252Fpost%252F1163343264310011715&messageId=279"}}},{"type":"bubble","hero":{"type":"image","url":"https://shellconnect.shellcampaigns.com/uploads/102021/clubsmart0606.png","align":"center","size":"full","aspectRatio":"1:1","aspectMode":"cover","action":{"type":"uri","uri":"https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Ftimeline.line.me%252Fpost%252F1163343264310011715&messageId=279"}}},{"type":"bubble","hero":{"type":"image","url":"https://shellconnect.shellcampaigns.com/uploads/102021/clubsmart0607.png","align":"center","size":"full","aspectRatio":"1:1","aspectMode":"cover","action":{"type":"uri","uri":"https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Ftimeline.line.me%252Fpost%252F1163227278209068770&messageId=265&utm_source=line&utm_medium=chat&utm_campaign=goplus2021&utm_content=250points&utm_land=linetimeline&utm_target=all"}}}]}}
    return client.replyMessage(event.replyToken, richmenu);
  }

  if (message.text == 'ขั้นตอนการสมัครสมาชิก เชลล์ โกพลัสcc') {
    var richmenu = {"type":"flex","altText":"ขั้นตอนการสมัครสมาชิกใหม่ เชลล์ โกพลัส","contents":{"type":"carousel","contents":[{"type":"bubble","hero":{"type":"image","url":"https://shellconnect.shellcampaigns.com/uploads/092021/new01.png","align":"center","size":"full","aspectRatio":"1:1","aspectMode":"cover","action":{"type":"uri","uri":"https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Fbit.ly%252F39p6O4s&messageId=265"}}},{"type":"bubble","hero":{"type":"image","url":"https://shellconnect.shellcampaigns.com/uploads/092021/new02.png","align":"center","size":"full","aspectRatio":"1:1","aspectMode":"cover","action":{"type":"uri","uri":"https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Fbit.ly%252F39p6O4s&messageId=265"}}},{"type":"bubble","hero":{"type":"image","url":"https://shellconnect.shellcampaigns.com/uploads/092021/new03.png","align":"center","size":"full","aspectRatio":"1:1","aspectMode":"cover","action":{"type":"uri","uri":"https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Fbit.ly%252F39p6O4s&messageId=265"}}},{"type":"bubble","hero":{"type":"image","url":"https://shellconnect.shellcampaigns.com/uploads/092021/new04.png","align":"center","size":"full","aspectRatio":"1:1","aspectMode":"cover","action":{"type":"uri","uri":"https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Fbit.ly%252F39p6O4s&messageId=265"}}},{"type":"bubble","hero":{"type":"image","url":"https://shellconnect.shellcampaigns.com/uploads/092021/new05.png","align":"center","size":"full","aspectRatio":"1:1","aspectMode":"cover","action":{"type":"uri","uri":"https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Fbit.ly%252F39p6O4s&messageId=265"}}},{"type":"bubble","hero":{"type":"image","url":"https://shellconnect.shellcampaigns.com/uploads/092021/new06.png","align":"center","size":"full","aspectRatio":"1:1","aspectMode":"cover","action":{"type":"uri","uri":"https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Ftimeline.line.me%252Fpost%252F1163227278209068770&messageId=265&utm_source=line&utm_medium=chat&utm_campaign=goplus2021&utm_content=250points&utm_land=linetimeline&utm_target=all"}}},{"type":"bubble","hero":{"type":"image","url":"https://shellconnect.shellcampaigns.com/uploads/092021/new07.png","align":"center","size":"full","aspectRatio":"1:1","aspectMode":"cover","action":{"type":"uri","uri":"https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Ftimeline.line.me%252Fpost%252F1163227278209068770&messageId=265&utm_source=line&utm_medium=chat&utm_campaign=goplus2021&utm_content=250points&utm_land=linetimeline&utm_target=all"}}}]}}
    return client.replyMessage(event.replyToken, richmenu);

  }

  if (message.text == 'ขั้นตอนการย้ายบัญชีจากเชลล์ คลับสมาร์ทcc') {
   var richmenu = {"type":"flex","altText":"ขั้นตอนการย้ายบัญชีจากเชลล์ คลับสมาร์ท","contents":{"type":"carousel","contents":[{"type":"bubble","hero":{"type":"image","url":"https://shellconnect.shellcampaigns.com/uploads/092021/club01.png","align":"center","size":"full","aspectRatio":"1:1","aspectMode":"cover","action":{"type":"uri","uri":"https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Fbit.ly%252F3EFvmEz&messageId=266"}}},{"type":"bubble","hero":{"type":"image","url":"https://shellconnect.shellcampaigns.com/uploads/092021/club02.png","align":"center","size":"full","aspectRatio":"1:1","aspectMode":"cover","action":{"type":"uri","uri":"https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Fbit.ly%252F3EFvmEz&messageId=266"}}},{"type":"bubble","hero":{"type":"image","url":"https://shellconnect.shellcampaigns.com/uploads/092021/club03.png","align":"center","size":"full","aspectRatio":"1:1","aspectMode":"cover","action":{"type":"uri","uri":"https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Fbit.ly%252F3EFvmEz&messageId=266"}}},{"type":"bubble","hero":{"type":"image","url":"https://shellconnect.shellcampaigns.com/uploads/092021/club04.png","align":"center","size":"full","aspectRatio":"1:1","aspectMode":"cover","action":{"type":"uri","uri":"https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Fbit.ly%252F3EFvmEz&messageId=266"}}},{"type":"bubble","hero":{"type":"image","url":"https://shellconnect.shellcampaigns.com/uploads/092021/club05.png","align":"center","size":"full","aspectRatio":"1:1","aspectMode":"cover","action":{"type":"uri","uri":"https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Fbit.ly%252F3EFvmEz&messageId=266"}}},{"type":"bubble","hero":{"type":"image","url":"https://shellconnect.shellcampaigns.com/uploads/092021/club06.png","align":"center","size":"full","aspectRatio":"1:1","aspectMode":"cover","action":{"type":"uri","uri":"https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Ftimeline.line.me%252Fpost%252F1163227282109068798&messageId=266&utm_source=line&utm_medium=chat&utm_campaign=goplus2021&utm_content=250points&utm_land=linetimeline&utm_target=all"}}},{"type":"bubble","hero":{"type":"image","url":"https://shellconnect.shellcampaigns.com/uploads/092021/club07.png","align":"center","size":"full","aspectRatio":"1:1","aspectMode":"cover","action":{"type":"uri","uri":"https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Ftimeline.line.me%252Fpost%252F1163227282109068798&messageId=266&utm_source=line&utm_medium=chat&utm_campaign=goplus2021&utm_content=250points&utm_land=linetimeline&utm_target=all"}}}]}}
    return client.replyMessage(event.replyToken, richmenu);

  }

  if (message.text == 'เชลล์ชวนชิม') {
    var richmenu = {
      "type": "imagemap",
      "baseUrl": "https://shellconnect.shellcampaigns.com/uploads/richmenu-v2/sss.png?_ignore=",
      "altText": "พิกัดร้านเด็ด เชลล์ชวนชิม",
      "baseSize": {
        "width": 1040,
        "height": 1338
      },
      "actions": [
        {
          "type": "uri",
          "area": {
            "x": 2,
            "y": 2,
            "width": 511,
            "height": 506
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%3A%2F%2Fshellshuanshim.com%3Futm%3Dsss&messageID=-1"
        },
        {
          "type": "uri",
          "area": {
            "x": 522,
            "y": 0,
            "width": 518,
            "height": 511
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%3A%2F%2Fshellshuanshim.com%2Fpromotion%2Flist%3Ftype%3Dclubsmart%26utm%3Dsss&messageID=-1"
        },
        {
          "type": "uri",
          "area": {
            "x": 2,
            "y": 520,
            "width": 516,
            "height": 478
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%3A%2F%2Fshellshuanshim.com%2Fsearch%2Fsearch-result%3Fkeyword%3Dnull%26cate%3Dall%26type%3Dall%26location%3D13.721528573443024%2C100.55992780193273%26minPrice%3D0%26maxPrice%3D3000%26nearby%3D13.721528573443024%2C100.55992780193273%26page%3D1%26utm%3Dsss&messageID=-1"
        },
        {
          "type": "uri",
          "area": {
            "x": 525,
            "y": 522,
            "width": 515,
            "height": 475
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%3A%2F%2Fshellshuanshim.com%2Froute%3Futm%3Dsss&messageID=-1"
        },
        {
          "type": "uri",
          "area": {
            "x": 0,
            "y": 1002,
            "width": 1040,
            "height": 336
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%3A%2F%2Fshellshuanshim.com%2Fabout%3Futm%3Dsss&messageID=-1"
        }
      ]
    };
    return client.replyMessage(event.replyToken, richmenu);
  }

  if (message.text == 'พลังแรง') {
    var richmenu = {
      "type": "imagemap",
      "baseUrl": "https://shellconnect.shellcampaigns.com/uploads/072021/1007-2v2.png?_ignored=",
      "altText": "พลังแรง",
      "baseSize": { "width": 1040, "height": 1300 },
      "video": {
          "originalContentUrl": "https://player.vimeo.com/external/557926801.sd.mp4?s=cabb1861c7c131ba552ad70d4259af75fa01103a&profile_id=165",
          "previewImageUrl": "https://shellconnect.shellcampaigns.com/uploads/072021/1007-preview.png?_ignored=",
          "area": { "x": 0, "y": 0, "width": 1040, "height": 586 }
      },
      "actions": [
          { "type": "uri", "area": { "x": 0, "y": 588, "width": 1040, "height": 586 }, "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Ftimeline.line.me%252Fpost%252F1162545193909062643&messageId=205&utm_source=line&utm_medium=chat&utm_campaign=vpd&utm_content=info&utm_land=linetimeline&utm_target=all" }
      ]
  }
  
  
    return client.replyMessage(event.replyToken, richmenu);
  }

  if (message.text == 'พลังลุย') {
    var richmenu = {
      "type": "imagemap",
      "baseUrl": "https://shellconnect.shellcampaigns.com/uploads/072021/1007-3v2.png?_ignored=",
      "altText": "พลังลุย",
      "baseSize": { "width": 1040, "height": 1300 },
      "video": {
          "originalContentUrl": "https://player.vimeo.com/external/557926801.sd.mp4?s=cabb1861c7c131ba552ad70d4259af75fa01103a&profile_id=165",
          "previewImageUrl": "https://shellconnect.shellcampaigns.com/uploads/072021/1007-preview.png?_ignored=",
          "area": { "x": 0, "y": 0, "width": 1040, "height": 586 }
      },
      "actions": [
          { "type": "uri", "area": { "x": 0, "y": 588, "width": 1040, "height": 586 }, "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Ftimeline.line.me%252Fpost%252F1162545193909062643&messageId=205&utm_source=line&utm_medium=chat&utm_campaign=vpd&utm_content=info&utm_land=linetimeline&utm_target=all" }
      ]
  }
  
    return client.replyMessage(event.replyToken, richmenu);
  }

  if (message.text == 'โปรแรงเกินคุ้ม' || message.text == 'โปรโมชั่น' || message.text == 'โปรแรง' || message.text == 'โปร') {
    var richmenu = {
      "type": "imagemap",
      "baseUrl": "https://shellconnect.shellcampaigns.com/uploads/072021/hotpro-15.png?_ignore=",
      "altText": "โปรแรงเกินคุ้ม",
      "baseSize": {
        "width": 1040,
        "height": 2080
      },
      "actions": [
        {
          "type": "uri",
          "area": {
            "x": 71,
            "y": 297,
            "width": 290,
            "height": 136
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1160594020209063375&utm=hotprogoplus"
        },
        {
          "type": "uri",
          "area": {
            "x": 380,
            "y": 297,
            "width": 284,
            "height": 136
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1160549740309063546&utm=hotprogoplus"
        },
        {
          "type": "uri",
          "area": {
            "x": 685,
            "y": 294,
            "width": 284,
            "height": 142
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1159011430209063170&utm=hotprogoplus"
        },
        {
          "type": "uri",
          "area": {
            "x": 73,
            "y": 456,
            "width": 291,
            "height": 135
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://www.shellcampaigns.com/shellroadsideassistance&utm=hotprogoplus"
        },
        {
          "type": "uri",
          "area": {
            "x": 379,
            "y": 454,
            "width": 282,
            "height": 137
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1160681140509065408&utm=hotprogoplus"
        },
        {
          "type": "uri",
          "area": {
            "x": 680,
            "y": 456,
            "width": 291,
            "height": 136
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1161335770209063422&utm=hotprogoplus"
        },
        {
          "type": "uri",
          "area": {
            "x": 70,
            "y": 615,
            "width": 292,
            "height": 138
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1161655300209065578&utm=hotprogoplus"
        },
        {
          "type": "uri",
          "area": {
            "x": 380,
            "y": 613,
            "width": 286,
            "height": 139
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1162060843109069382&utm=hotprogoplus"
        },
        {
          "type": "uri",
          "area": {
            "x": 681,
            "y": 611,
            "width": 290,
            "height": 140
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1162372248909062815&utm=hotprogoplus"
        },
        {
          "type": "uri",
          "area": {
            "x": 374,
            "y": 1263,
            "width": 288,
            "height": 147
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://www.shell.co.th/travel-together&utm=hotprodeliselect"
        },
        {
          "type": "uri",
          "area": {
            "x": 678,
            "y": 1264,
            "width": 292,
            "height": 144
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1162408695209067111&utm=hotprodeliselect"
        },
        {
          "type": "uri",
          "area": {
            "x": 222,
            "y": 1432,
            "width": 289,
            "height": 140
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1162623023409069432&utm=hotprodeliselect"
        },
        {
          "type": "uri",
          "area": {
            "x": 528,
            "y": 1439,
            "width": 288,
            "height": 141
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1162691562609062737&utm=hotprodeliselect"
        },
        {
          "type": "uri",
          "area": {
            "x": 71,
            "y": 1267,
            "width": 284,
            "height": 142
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1161456580209066418&utm=hotprodeliselect"
        },
        {
          "type": "uri",
          "area": {
            "x": 375,
            "y": 927,
            "width": 288,
            "height": 135
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://www.shell.co.th/travel-together&utm=hotprofuellube"
        },
        {
          "type": "uri",
          "area": {
            "x": 73,
            "y": 1761,
            "width": 286,
            "height": 140
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1159479100209062919&utm=hotproback"
        },
        {
          "type": "uri",
          "area": {
            "x": 374,
            "y": 1765,
            "width": 286,
            "height": 135
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1161767641609068206&utm=hotproback"
        },
        {
          "type": "uri",
          "area": {
            "x": 683,
            "y": 1761,
            "width": 286,
            "height": 144
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1162337500409064272&utm=hotproback"
        },
        {
          "type": "uri",
          "area": {
            "x": 222,
            "y": 1924,
            "width": 288,
            "height": 135
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1162389888009063428&utm=hotproback"
        },
        {
          "type": "uri",
          "area": {
            "x": 527,
            "y": 1924,
            "width": 289,
            "height": 137
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?messageID=-1&url=https://timeline.line.me/post/_dVcGUqO0Sq8E_RSnbR3iU5OgDx5iWVLW_3p05Lk/1162556483909068603&utm=hotproback"
        }
      ]
    }
    return client.replyMessage(event.replyToken, richmenu);
  }

  if (message.text == 'ราคาและปั๊มน้ำมัน') {
    var richmenu = {
      "type": "imagemap",
      "baseUrl": "https://shellconnect.shellcampaigns.com/uploads/052021/shell_station.png?_ignored=",
      "altText": "ราคาและปั๊มน้ำมัน",
      "baseSize": {
        "width": 1040,
        "height": 1040
      },
      "actions": [
        {
          "type": "uri",
          "area": {
            "x": 0,
            "y": 0,
            "width": 522,
            "height": 1040
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https://www.shell.co.th/th_th/motorists/shell-fuels/fuel-price/app-fuel-prices.html?utm=oilprice&messageID=-1"
        },
        {
          "type": "uri",
          "area": {
            "x": 522,
            "y": 0,
            "width": 516,
            "height": 1037
          },
          "linkUri": "https://liff.line.me/1493169972-DozVrxpL/trck?url=https%253A%252F%252Fwww.shell.co.th%252Fth_th%252Fmotorists%252Fshell-station-locator.html%2523iframe%253DLz9sb2NhbGU9dGhfVEgjL0AxMy4wMzc0NSw5Ni4yMjc3Niw2eg%253Futm%253Dstationsearch&messageID=-1"
        }
      ]
    }
    return client.replyMessage(event.replyToken, richmenu);
  }

  if(message.text == "ชอบดื่มน้ำผลไม้"){
    var richtext = {
      "type": "imagemap",
      "baseUrl": "https://shellconnect.shellcampaigns.com/uploads/delicafe/horoscope_juice.jpg?_ignored=",
      "altText": "ชอบดื่มน้ำผลไม้",
      "baseSize": {
        "width": 1040,
        "height": 1040
      },
      "actions": []
    }
    return client.replyMessage(event.replyToken, richtext);
  }

  if(message.text == 'kalamell') {

    
    var result = await service.scheduleRequest();
    var richtext = {
      "type": "text",
      "text": result.toString(),
    }

    return client.replyMessage(event.replyToken, richtext)
  }

  console.log("almond: "+message.text);
  /* Almond */
  // var almond_keyword = ["almond test","SETSTAFF","RESET","ขอรหัส","แสดง QR code"]
  // if(almond_keyword.includes(message.text)){
  //   console.log("almond_text",req.headers);
  //   delete req.headers['host'];
  //   var options = {
  //       url: 'https://shelllineservices.shellcampaigns.com/api/webhook',
  //       method: 'POST',
  //       headers: req.headers,
  //       body: JSON.stringify(req.body),
  //   }; 
  //   return request(options, function (error, response) {
  //     if (error) throw new Error(error);
  //     console.log(response.body);
  //   });
  // }
  // var almond_keyword = ["almond test","SETSTAFF","RESET","ขอรหัส","แสดง QR code"]
  // if(almond_keyword.includes(message.text)){

  /*
    console.log("almond_text",req.headers);
    delete req.headers['host'];
    console.log("body",req.body);
    var options = {
        url: 'https://shelllineservices.shellcampaigns.com/api/webhook',
        method: 'POST',
        headers: req.headers,
        body: JSON.stringify(req.body),
    }; 
    request(options, function (error, response) {
      if (error) throw new Error(error);
      console.log('return data ');
      console.log('<<<<<<<<< : ', response.body);
    });
    */

    
  //}
  /* End */
}

function handleImage(message, event) {
  //return client.replyMessage(event.replyToken, { type: 'text', text: 'Got Image' });
}

function handleVideo(message, event) {
  //return client.replyMessage(event.replyToken, { type: 'text', text: 'Got Video' });
}

function handleAudio(message, event) {
  //return client.replyMessage(event.replyToken, { type: 'text', text: 'Got Audio' });
}

function handleLocation(message, event) {
  var text = "location: "+message.latitude+",\n"+message.longitude
  console.log('latitude: '+message.latitude);
  console.log('longitude: '+message.longitude);
  //return client.replyMessage(event.replyToken, { type: 'text', text: text });
}

function handleSticker(message, event) {
  //return client.replyMessage(event.replyToken, { type: 'text', text: 'Got Sticker' });
}

const app = express();
const port = Number(config.port) || 3004;
var bodyParser = require('body-parser');
const { urlencoded } = require('express');

app.get('/webhook', (req, res) => { 
  res.send('dfdfdfdfd');
});
var myconfig = {
  channelAccessToken: process.env.channelAccessToken,
  channelSecret: process.env.channelSecret
}
app.post('/webhook', line.middleware(myconfig), (req, res) => {
  console.log(config.dev);
  if (!Array.isArray(req.body.events)) {
    return res.status(500).end();
  }
  Promise.all(req.body.events.map(event => {
    // check verify webhook event
    if (event.source.userId === 'Udeadbeefdeadbeefdeadbeefdeadbeef') {
      return;
    }
    return handleEvent(req,event);
  }))
    .then(() => res.end())
    .catch((err) => {
      console.error(err);
      res.status(500).end();
    });
});

app.get('/schedule', async (req, res) => {
  var result = await service.scheduleRequest();
  //res.json(result.recordsets);
  result.recordsets.forEach(function(c) {
    console.log('data : ', c.ID);

  });

  res.send('ok');
});

app.get('/api/xx', (req, res) => {
  res.send('xxx');
})

app.get('/api/line_user_id/:id', async (req, res) => { 
  res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true); // If needed
  var save   = await service.insertLINEUser(req.params.id);
  var result = await service.getLineUser(req.params.id);
  // res.json(recordset.recordset)
  if (result.recordset.length == 0) {
    
    res.json({
      result: false
    })
  } else { 
    let data = result.recordset;
    res.json({
      result: true,
      data: data
    })

  }
  
 
})


var urlencodedParser = bodyParser.urlencoded({ extended: true })

app.post('/api/register', urlencodedParser,  async (req, res) => { 
  res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'POST'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true); // If needed

    var result = await service.updatedLINERegister(req.body);
    res.json({
      result: true, 
      data: req.body
    })
})


app.post('/api/checksurvey', urlencodedParser,  async (req, res) => { 
  res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'POST'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true); // If needed

    var result = await service.checkSurvey(req.body.LINE_USER_ID, req.body.SURVEY_ID);
    if (result.recordset.length == 0) {
      res.json({
        result: true
      });
    } else {
      res.json({
        result: false
      })
    }
})

app.post('/api/survey', urlencodedParser,  async (req, res) => { 
  res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'POST'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true); // If needed

    let ar = [];
    for (var key in req.body) {
      if (req.body.hasOwnProperty(key)) {
        ar.push(key);
        
        if (key != 'LINE_USER_ID' && key != 'SURVEY_ID') {
          for (var j = 0; j < req.body[key].length; j++) {
            let save = await service.insertSurvey(req.body.LINE_USER_ID, req.body.SURVEY_ID, key, req.body[key][j]);

          }
          
          ar.push(req.body[key].length);
        }
        


      }
    }

    res.json(ar);
})


app.listen(port, () => {
  console.log(`App is listening on port ${port}`);
});