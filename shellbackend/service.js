const config = require('./config.json');
var sql = require("mssql");

const pooldb = require('./db.js');

async function checkExistingLINEUser(userId){
    let conn;
    try{
        const res = await pooldb.query("SELECT * FROM line_user_info WHERE line_user_id = ?", [userId]);
        return res;
        
    }catch(err){
        console.log("checkExistingLINEUser",err);
    }
}


async function insertLINEUser(userId){
    
    try {
        console.log("insertLINEUser",userId);
        var checkLineUser = await checkExistingLINEUser(userId);
        if (checkLineUser.length  == 0) {
            var res  =pooldb.query('INSERT INTO line_user_info (line_user_id) VALUES(?) ', [userId]);
            //await conn.close();
            return res.rowsAffected;

        } else {

            
            var res = pooldb.query('UPDATE line_user_info SET is_blocked = 0 ,update_date = NOW() WHERE line_user_id = ?', [userId]);
            //await conn.close();
            return res.rowsAffected;

        }

    } catch(err) {
        console.log("insertLINEUser",err);
    }
    /*
    try{
        console.log("insertLINEUser",userId);
        var checkLineUser = await checkExistingLINEUser(userId);
        console.log("checkLineUser",checkLineUser);
        if(checkLineUser.recordset.length <= 0){
            var pool = await sql.connect(config.mssql);
            var recordset = await pool.request()
                .input('userlineid', sql.VarChar, userId)
                .query('INSERT INTO line_user_info (LINE_USER_ID) VALUES (@userlineid)');
                console.log("rowsAffected",recordset.rowsAffected)
            await sql.close();
            return recordset.rowsAffected;
        }else{
            console.log("updateLINEUser");
            var pool = await sql.connect(config.mssql);
            var recordset = await pool.request()
                .input('userlineid', sql.VarChar, userId)
                .query('UPDATE line_user_info SET IS_BLOCKED = 0 ,UPDATE_DATE = GETDATE() WHERE LINE_USER_ID = @userlineid');
            await sql.close();
            return recordset.rowsAffected;
        }
    }catch(err){
        console.log("insertLINEUser",err);
    }
    */
}



async function updateBlockedLINEUser(userId){
    let conn;
    try{
        console.log("updateBlockLINEUser");
        var checkLineUser = await checkExistingLINEUser(userId);
        console.log("checkLineUser",checkLineUser.length);
        if(checkLineUser.length > 0){
            conn = await pooldb.getConnection();
            const res = await conn.query("UPDATE line_user_info SET is_blocked = 1, update_date = NOW() WHERE line_user_id = ?", [userId]);
            //await conn.close();

            return res.rowsAffected;

            /*
            var pool = await sql.connect(config.mssql);
            var recordset = await pool.request()
                .input('userlineid', sql.VarChar, userId)
                .query('UPDATE line_user_info SET IS_BLOCKED = 1 ,UPDATE_DATE = GETDATE() WHERE LINE_USER_ID = @userlineid');
            await sql.close();
            return recordset.rowsAffected;
            */

        }else{
            return 0;
        }
    }catch(err){
        console.log("updateBlockLINEUser",err);
    }
}

async function insertShellResponseLog(userId,responseSource,responseJson){
    let conn; 

    try{
        console.log("insertShellResponseLog >>>>> ",userId);
        conn = await pooldb.getConnection();
        let sql = 'INSERT INTO shell_response_log (line_user_id, response_source, response_json, create_date) VALUES (?, ?, ?, NOW())';
        let res = await conn.query(sql, [userId, responseSource, responseJson]);
        //await conn.close();
        return res.rowsAffected;

    }catch(err){
        console.log("insert response log > ",err.code);
    }
}

async function insertPersonalizeResponse(userId,responseSource,responseKeyword){
    let conn;
    try{
        console.log("insertShellResponseLog",userId);
        conn = await pooldb.getConnection();
        let sql = 'INSERT INTO shell_personalize_response (line_user_id, response_source, response_keyword) VALUES (?, ?, ?)';
        let res = await conn.query(sql, [userId, responseSource, responseKeyword]);
        //await conn.close();
        return res.rowsAffected;
    }catch(err){
        console.log("insertPersonalizeResponse",err);
    }
}

async function insertPersonalizeResponseTD(userId,responseSource,responseKeyword){
    let conn;
    try{
        console.log("insertShellResponseLog",userId);
        conn = await pooldb.getConnection();
        let sql = 'INSERT INTO shell_personalize_response (line_user_id, response_source, response_keyword, td, create_date) VALUES (?, ?, ?, 1, NOW())';
        let res = await conn.query(sql, [userId, responseSource, responseKeyword]);
        //await conn.close();
        return res;
    }catch(err){
        console.log("insertPersonalizeResponse",err);
    }
}

async function insertPersonalizeResponseTDxxx(userId,responseSource,responseKeyword){
    try{
        console.log("insertShellResponseLog",userId);
        var pool = await sql.connect(config.mssql);
        var recordset = await pool.request()
            .output("id", sql.Int)
            .input('userlineid', sql.VarChar, userId)
            .input('responseSource', sql.VarChar, responseSource)
            .input('responseKeyword', sql.NVarChar, responseKeyword)
            .query('INSERT INTO shell_personalize_response (LINE_USER_ID,RESPONSE_SOURCE,RESPONSE_KEYWORD, td) VALUES (@userlineid,@responseSource,@responseKeyword, 1); SELECT SCOPE_IDENTITY() AS id;');
            console.log("rowsAffected2",recordset.rowsAffected)
        await sql.close();
        return recordset;
    }catch(err){
        console.log("insertLINEUser",err);
    }
}

async function scheduleRequest(){
    try{
        
        console.log("schedule request");
        var pool = await sql.connect(config.mssql);
        var recordset = await pool.request()
            .query("SELECT TOP 1 * FROM message WHERE STATUS = 'success' AND IS_ACTIVE = 1 AND (UPDATE_BATCH_DATE IS NULL) ORDER BY BROADCAST_TIME ASC");
        await sql.close();
        //console.log("recordset",recordset)
        return recordset;
        
      // return '';
    }catch(err){
        console.log("schedule request",err);
    }
}

// async function insertShellResponseLog(userId){
//     try{
//         console.log("insertLINEUser",userId);
//         var checkLineUser = await checkExistingLINEUser(userId);
//         console.log("checkLineUser",checkLineUser);
//         if(checkLineUser.recordset.length <= 0){
//             var pool = await sql.connect(config.mssql);
//             var recordset = await pool.request()
//                 .input('userlineid', sql.VarChar, userId)
//                 .input('responseSource', sql.VarChar, responseSource)
//                 .input('responseJson', sql.VarChar, responseJson)
//                 .query('INSERT INTO shell_response_log (LINE_USER_ID,RESPONSE_SOURCE,RESPONSE_JSON) VALUES (@userlineid,@responseSource,@responseJson)');
//                 console.log("rowsAffected",recordset.rowsAffected)
//             await sql.close();
//             return recordset.rowsAffected;
//         }
//     }catch(err){
//         console.log("insertLINEUser",err);
//     }
// }


async function getLineUser(userId){
    try{
        console.log("getLineUser");
        var pool = await sql.connect(config.mssql);
        var recordset = await pool.request()
            .input('userlineid', sql.VarChar, userId)
            .query('SELECT LINE_USER_ID, LINE_USER_FIRSTNAME AS FULLNAME, GENDER, BIRTHDATE, ADDRESS, PROVINCE, AMPHUR, DISTRICT, ZIPCODE, CONDITION, PRIVACY, REGISTER FROM line_user_info WHERE LINE_USER_ID = @userlineid');
        await sql.close();
        console.log("recordset",recordset)
        return recordset;
    }catch(err){
        console.log("checkExistingLINEUser",err);
    }
}


async function updatedLINERegister(data){
    try{
        console.log("updateBlockLINEUser");

        if (data.LINE_USER_ID != '') {
        
            var checkLineUser = await checkExistingLINEUser(data.LINE_USER_ID);
            console.log("checkLineUser",checkLineUser);
            if(checkLineUser.recordset.length > 0){
                var birthdate = data.year + '-' + data.month + '-' + data.date;
                var pool = await sql.connect(config.mssql);
                var recordset = await pool.request()
                    .input('userlineid', sql.VarChar, data.LINE_USER_ID)
                    .input('FIRSTNAME', sql.NVarChar, data.FIRSTNAME)
                    .input('LASTNAME', sql.NVarChar, data.LASTNAME)
                    .input('LINE_USER_EMAIL', sql.NVarChar, data.LINE_USER_EMAIL)
                    .input('GENDER', sql.NVarChar, data.GENDER)
                    .input('BIRTHDATE', sql.Date, birthdate)
                    .input('ADDRESS', sql.NText, data.ADDRESS)
                    .input('PROVINCE', sql.NVarChar, data.PROVINCE)
                    .input('AMPHUR', sql.NVarChar, data.AMPHUR)
                    .input('DISTRICT', sql.NVarChar, data.DISTRICT)
                    .input('ZIPCODE', sql.NVarChar, data.ZIPCODE)
                    .input('CONDITION', sql.NVarChar, data.CONDITION)
                    .input('PRIVACY', sql.NVarChar, data.PRIVACY)
                    .input('PHONE', sql.NVarChar, data.PHONE)
                    .query('UPDATE line_user_info SET LINE_USER_EMAIL=@LINE_USER_EMAIL, LINE_USER_FIRSTNAME = @FIRSTNAME, LINE_USER_LASTNAME = @LASTNAME, GENDER = @GENDER, BIRTHDATE = @BIRTHDATE, ADDRESS = @ADDRESS, PROVINCE = @PROVINCE, AMPHUR = @AMPHUR, DISTRICT = @DISTRICT, ZIPCODE = @ZIPCODE, CONDITION = @CONDITION, PRIVACY = @PRIVACY, PHONE = @PHONE, LINE_USER_TEL = @PHONE, REGISTER=1  WHERE LINE_USER_ID = @userlineid');
                await sql.close();
                return recordset.rowsAffected;
            }else{

                return 0;
            }
        } else { 
            return 0;
        }
    }catch(err){
        console.log("updateBlockLINEUser",err);
    }
}



async function insertSurvey(userId, id, k, v){
    try{
        console.log("insertShellResponseLog",userId);
        var pool = await sql.connect(config.mssql);
        var recordset = await pool.request()
            .input('userlineid', sql.VarChar, userId)
            .input('servey_id', sql.VarChar, id)
            .input('survey_key', sql.NVarChar, k)
            .input('survey_value', sql.NVarChar, v)
            .query('INSERT INTO survey_info (LINE_USER_ID,SURVEY_ID,SURVEY_KEY, SURVEY_VALUE) VALUES (@userlineid,@servey_id,@survey_key, @survey_value)');
            console.log("rowsAffected2",recordset.rowsAffected)
        await sql.close();
        return recordset.rowsAffected;
    }catch(err){
        console.log("insertLINEUser",err);
    }
}


async function checkSurvey(userId ,surveyid){
    try{
        console.log("checkExistingLINEUser");
        var pool = await sql.connect(config.mssql);
        var recordset = await pool.request()
            .input('userId', sql.VarChar, userId)
            .input('servey_id', sql.VarChar, surveyid)
            .query('SELECT * FROM survey_info WHERE LINE_USER_ID = @userId AND SURVEY_ID =@servey_id');
        await sql.close();
        console.log("recordset",recordset)
        return recordset;
    }catch(err){
        console.log("checkExistingLINEUser",err);
    }
}

module.exports = {
    insertLINEUser,
    checkExistingLINEUser,
    updateBlockedLINEUser,
    insertShellResponseLog,
    insertPersonalizeResponse,
    insertPersonalizeResponseTD,
    scheduleRequest,
    getLineUser,
    updatedLINERegister,
    insertSurvey,
    checkSurvey
}