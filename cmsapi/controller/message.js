const sql = require('mssql');
const config = require('../config').dbconfig;
const channelAccessToken = require('../config').channelAccessToken;
const utils = require('../utility/utility');
const request = require('request');
const liffTrackUrl = require('../config').liffTrackUrl;
const mariadb = require('mariadb');

const pooldb = mariadb.createPool(config);
const pooldata = require('../db');
async function createMessage(data,res){
    try{
        if(data.title != ""){
            var message_type = "";
            var message_json = "";
            
            for(var i = 0;i < data.message.length; i++){
                if(i != 0){
                    message_type+= ",";
                    message_json+= ",";
                }else{
                    message_json += "{\"messages\": [";
                }
                message_type += await data.message[i].messageType;
                var messageJson = await JSON.parse(data.message[i].json);
                var messageTypeReal = await messageJson.type;
                if(messageTypeReal == "imagemap"){
                    var messageImageReal = await messageJson.baseUrl;
                    if(messageImageReal.indexOf("?_ignored=") < 0){
                        messageJson.baseUrl = await messageImageReal+"?_ignored=";
                    }
                }
                var nextMessageId = await getNextMessageId();
                messageJson = await findAndReplaceLink(messageJson,nextMessageId);
                messageJson = await JSON.stringify(messageJson);
                message_json += await messageJson;
                
                if(i == parseInt(data.message.length)-1){
                    message_json += "]}";
                }
            }
            
            message_json = JSON.stringify(message_json);
            if(data.broadcastType == "sendnow" && data.segmentId == 0){
                broadcastMessage(insertMessage,data,res,message_type,message_json);
            }else if(data.broadcastType == "sendnow" && data.segmentId > 0){
                narrowcastMessage(insertMessage,data,res,message_type,message_json);
            }else{
                var requestId = "";
                insertMessage(200,requestId,data,res,message_type,message_json);
            }
        }else{
            await utils.showResult(res,false,"no data");
        }
    }catch(err){
        console.log("createMessage",err);
        await utils.showResult(res,false,"Something went wrong.",err);
    }
}


async function testMessage(data,res){
    try{
        var pool = await sql.connect(config);
        var recordset = await pool.request()
            .query('SELECT * FROM tester_user WHERE IS_ACTIVATE = 1');
            await sql.close();
        var arr_userTest = [];
        if(recordset.recordset.length > 0){
            arr_userTest = data.testUser;
            if(data.title != ""){
                var message_type = "";
                var message_json = "";
                var json_arr_userTest = JSON.stringify(arr_userTest);
                for(var i = 0;i < data.message.length; i++){
                    if(i != 0){
                        message_type+= ",";
                        message_json+= ",";
                    }else{
                        message_json += "{\"to\":"+json_arr_userTest+","
                        message_json += "\"messages\": [";
                    }
                    message_type += await data.message[i].messageType;
                    var messageJson = await JSON.parse(data.message[i].json);
                    var messageTypeReal = await messageJson.type;
                    if(messageTypeReal == "imagemap"){
                        var messageImageReal = await messageJson.baseUrl;
                        if(messageImageReal.indexOf("?_ignored=") < 0){
                            messageJson.baseUrl = await messageImageReal+"?_ignored=";
                        }
                    }
                   messageJson = await findAndReplaceLink(messageJson, 0);
                    
                    
                    messageJson = await JSON.stringify(messageJson);
                    message_json += await messageJson;

                    if(i == parseInt(data.message.length)-1){
                        message_json += "]}";
                    }
                }
                multicastMessage(data,res,message_type,message_json);
            }else{
                await utils.showResult(res,false,"no data");
            }
        }else{
            await utils.showResult(res,false,"no test user");
        }
    }catch(err){
        console.log(err);
        await utils.showResult(res,false,"Something went wrong.",err);
    }
}

async function insertMessage(statusCode,requestId,data,res,message_type,message_json){
    if(statusCode == 200){
        var status = "";
        if(data.broadcastType == "sendnow"){
            status = "success";
        }else if(data.broadcastType == "draft"){
            status = "draft";
        }else{
            status = "pending";
        }
        var pool = await sql.connect(config);
        var recordset = await pool.request()
            .input('title', sql.NVarChar, data.title)
            .input('requestId', sql.VarChar, requestId)
            .input('image_message', sql.NVarChar, data.imageMessage)
            .input('pillar_id', sql.NVarChar, data.pillarId)
            .input('product_id', sql.NVarChar, data.productId)
            .input('tag', sql.NVarChar, data.tag)
            .input('segment_id', sql.Int, data.segmentId)
            .input('broadcast_time', sql.DateTimeOffset, new Date(data.broadcastTime))
            .input('broadcast_type', sql.VarChar, data.broadcastType)
            .input('message_type', sql.VarChar, message_type)
            .input('json', sql.NText, message_json)
            .input('status', sql.VarChar, status)
            .input('pillar2_id', sql.NVarChar, data.pillarId2)
            .input('campaign_id', sql.NVarChar, data.campaignId)
            .query('INSERT INTO message (TITLE,IMAGE_MESSAGE,REQUEST_ID,PILLAR_ID,SEGMENT_ID,BROADCAST_TIME,BROADCAST_TYPE,MESSAGE_TYPE,JSON, STATUS, PRODUCT_ID,TAG, PILLAR2_ID, CAMPAIGN_ID) VALUES (@title,@image_message,@requestId,@pillar_id,@segment_id,@broadcast_time,@broadcast_type,@message_type,@json,@status,@product_id,@tag, @pillar2_id, @campaign_id)');
            await sql.close();
        if(recordset.rowsAffected > 0){
            var result = [{
                "title": data.title,
                "imageMessage": data.imageMessage,
                "pillarId": data.pillarId,
                "productId": data.productId,
                "tag": data.tag,
                "segmentId": data.segmentId,
                "broadcastTime": data.broadcastTime,
                "broadcastType": data.broadcastType,
                "message_type": message_type,
                "json": JSON.parse(message_json),
                "status": status,
                "pillarId2": data.pillarId2,
                "campaign": data.campaign
            }]
            await utils.showResult(res,true,"success",result);
        }else{
            await utils.showResult(res,false,"insert unsuccess");
        }
    }else{
        await utils.showResult(res,false,"send unsuccess",result);  
    }
}

async function getAllSegments(res){
    try{
        var pool = await sql.connect(config);
        var recordset = await pool.request()
            .query('SELECT * FROM segment WHERE IS_ACTIVE = 1');
        if(recordset.recordset.length > 0){
            console.log(recordset.recordset);
            await utils.showResult(res,true,"success",recordset.recordset);
        }else{
            await utils.showResult(res,true,"no segment",[]);
        }
    }catch(err){
        console.log(err);
        await utils.showResult(res,false,"Something went wrong.");
    }
}

async function getSegment(data,res){
    try{
        var pool = await sql.connect(config);
        var recordset = await pool.request()
                .input('segmentId', sql.Int, data.segmentId)
                .query('SELECT * FROM segment WHERE ID = @segmentId AND IS_ACTIVE = 1');
        await sql.close();
        if(recordset.recordset.length > 0){
            await utils.showResult(res,true,"success",recordset.recordset);
        }else{
            await utils.showResult(res,false,"This segment is not found. or not active.");
        }
    }catch(err){
        console.log(err);
        await utils.showResult(res,false,"Something went wrong.");
    }
}

async function getAllPillars(res){
    try{
        var pool = await sql.connect(config);
        var recordset = await pool.request()
            .query('SELECT * FROM pillar WHERE IS_ACTIVE = 1');
        if(recordset.recordset.length > 0){
            await utils.showResult(res,true,"success",recordset.recordset);
        }else{
            await utils.showResult(res,false,"no pillar");
        }
    }catch(err){
        console.log("allPillars",err);
        await utils.showResult(res,false,"Something went wrong.");
    }
}

async function getAllPillars2(res){
    try{
        var pool = await sql.connect(config);
        var recordset = await pool.request()
            .query('SELECT * FROM pillar2 WHERE IS_ACTIVE = 1');
        if(recordset.recordset.length > 0){
            await utils.showResult(res,true,"success",recordset.recordset);
        }else{
            await utils.showResult(res,false,"no pillar");
        }
    }catch(err){
        console.log("allPillars",err);
        await utils.showResult(res,false,"Something went wrong.");
    }
}

async function getAllCampaigns(res){
    try{
        var pool = await sql.connect(config);
        var recordset = await pool.request()
            .query('SELECT * FROM campaign_info');
        if(recordset.recordset.length > 0){
            await utils.showResult(res,true,"success",recordset.recordset);
        }else{
            await utils.showResult(res,false,"no campaign");
        }
    }catch(err){
        console.log("allcampaign info",err);
        await utils.showResult(res,false,"Something went wrong.");
    }
}


async function getPillar(data,res){
    try{
        var pool = await sql.connect(config);
        var recordset = await pool.request()
                .input('pillarId', sql.Int, data.pillarId)
                .query('SELECT * FROM pillar WHERE ID = @pillarId AND IS_ACTIVE = 1');
        await sql.close();
        if(recordset.recordset.length > 0){
            await utils.showResult(res,true,"success",recordset.recordset);
        }else{
            await utils.showResult(res,false,"This pillar is not found. or not active.");
        }
    }catch(err){
        console.log(err);
        await utils.showResult(res,false,"Something went wrong.");
    }
}

async function getAllTypes(res){
    try{
        var pool = await sql.connect(config);
        var recordset = await pool.request()
            .query('SELECT * FROM type WHERE IS_ACTIVE = 1');
        if(recordset.recordset.length > 0){
            await utils.showResult(res,true,"success",recordset.recordset);
        }else{
            await utils.showResult(res,false,"no type");
        }
    }catch(err){
        console.log("allTypes",err);
        await utils.showResult(res,false,"Something went wrong.");
    }
}

async function getType(data,res){
    try{
        var pool = await sql.connect(config);
        var recordset = await pool.request()
                .input('typeId', sql.Int, data.typeId)
                .query('SELECT * FROM type WHERE ID = @typeId AND IS_ACTIVE = 1');
        await sql.close();
        if(recordset.recordset.length > 0){
            await utils.showResult(res,true,"success",recordset.recordset);
        }else{
            await utils.showResult(res,false,"This type is not found. or not active.");
        }
    }catch(err){
        console.log(err);
        await utils.showResult(res,false,"Something went wrong.");
    }
}

async function getAllProducts(res){
    try{
        var pool = await sql.connect(config);
        var recordset = await pool.request()
            .query('SELECT * FROM product WHERE IS_ACTIVE = 1');
        if(recordset.recordset.length > 0){
            await utils.showResult(res,true,"success",recordset.recordset);
        }else{
            await utils.showResult(res,false,"no product");
        }
    }catch(err){
        console.log("allProducts",err);
        await utils.showResult(res,false,"Something went wrong.");
    }
}

async function getProduct(data,res){
    try{
        var pool = await sql.connect(config);
        var recordset = await pool.request()
                .input('productId', sql.Int, data.productId)
                .query('SELECT * FROM product WHERE ID = @productId AND IS_ACTIVE = 1');
        await sql.close();
        if(recordset.recordset.length > 0){
            await utils.showResult(res,true,"success",recordset.recordset);
        }else{
            await utils.showResult(res,false,"This product is not found. or not active.");
        }
    }catch(err){
        console.log(err);
        await utils.showResult(res,false,"Something went wrong.");
    }
}

async function broadcastMessage(callback,data,res,message_type,message_json){
    try{
        var item = await JSON.parse(message_json);
        var options = {
            uri: 'https://api.line.me/v2/bot/message/broadcast',
            method: 'POST',
            headers: {
                Authorization: 'Bearer '+channelAccessToken
            },
            json: JSON.parse(item)
        }; 
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                return callback(response.statusCode,response.headers['x-line-request-id'],data,res,message_type,message_json);
            }else{
                console.log("LINE api",error);
                return utils.showResult(res,false,"Something went wrong with LINE api");
            }
        });
    }catch(err){
        console.log(err);
        return utils.showResult(res,false,"Something went wrong");
    }
}

async function narrowcastMessage(callback,data,res,message_type,message_json){
    console.log("narrowcastMessage");
    var requestId = "";
    return callback(200,requestId,data,res,message_type,message_json);
    // var item = await JSON.parse(message_json);
    // var options = {
    //     uri: 'https://api.line.me/v2/bot/message/broadcast',
    //     method: 'POST',
    //     headers: {
    //         Authorization: 'Bearer '+channelAccessToken
    //     },
    //     json: JSON.parse(item)
    // }; 

    // request(options, function (error, response, body) {
    //     if (!error && response.statusCode == 200) {
    //         return callback(response.statusCode,data,res,message_type,message_json);
    //     }
    // });
}

async function multicastMessage(data,res,message_type,message_json){
    try{
        var item = message_json;
        var options = {
            uri: 'https://api.line.me/v2/bot/message/multicast',
            method: 'POST',
            headers: {
                Authorization: 'Bearer '+channelAccessToken
            },
            json: JSON.parse(item)
        }; 
        request(options, function (error, response, body) {
            if (response.statusCode == 200) {
                return utils.showResult(res,true,"success");
            }else{
                return utils.showResult(res,false,"Something went wrong with LINE api");
            }
        });
    }catch(err){
        console.log(err);
        return utils.showResult(res,false,"Something went wrong");
    }
}

async function getAllMessages(res){
    try{
        var pool = await sql.connect(config);
        var recordset = await pool.request()
            .query('SELECT * FROM message WHERE IS_ACTIVE = 1 ORDER BY CREATE_DATE DESC');
        await sql.close();
        if(recordset.recordset.length > 0){
            await utils.showResult(res,true,"success",recordset.recordset);
        }else{
            await utils.showResult(res,false,"no message");
        }
    }catch(err){
        console.log(err);
        await utils.showResult(res,false,"Something went wrong.");
    }
}

async function getMessage(data,res){
    try{
        var pool = await sql.connect(config);
        var recordset = await pool.request()
                .input('messageId', sql.Int, data.messageId)
                .query('SELECT * FROM message WHERE ID = @messageId AND IS_ACTIVE = 1');
        if(recordset.recordset.length > 0){
            await utils.showResult(res,true,"success",recordset.recordset);
        }else{
            await utils.showResult(res,false,"This message is not found. or not active.");
        }
    }catch(err){
        console.log("getMessage",err);
        await utils.showResult(res,false,"Something went wrong.");
    }
}

async function checkExistingMessage(data){
    try{
        var pool = await sql.connect(config);
        var recordset = await pool.request()
            .input('messageId', sql.Int, data.messageId)
            .query('SELECT * FROM message WHERE ID = @messageId AND IS_ACTIVE = 1');
        await sql.close();
        if(recordset.recordset.length > 0){
            return true;
        }else{
            return false;
        }
    }catch(err){
        console.log("err",err);
        return false;
    }
}

async function deleteMessage(data,res){
    try{
        var has_message = await checkExistingMessage(data);
        if(has_message){
            var pool = await sql.connect(config);
            var recordset = await pool.request()
                .input('messageId', sql.Int, data.messageId)
                .query('UPDATE message SET IS_ACTIVE = 0 WHERE ID = @messageId');
            await sql.close();
            if(recordset.rowsAffected > 0){
                await utils.showResult(res,true,"delete success");
            }else{
                await utils.showResult(res,false,"delete unsuccess");
            }
        }else{
            await utils.showResult(res,false,"This message is not existing.");
        }
    }catch(err){
        await utils.showResult(res,false,"Something went wrong.");
    }
}

async function editMessage(data,res){
    try{
        if(data.title != ""){
            var has_message = await checkExistingMessage(data);
            if(has_message){
                var message_type = "";
                var message_json = "";
                for(var i = 0;i < data.message.length; i++){
                    if(i != 0){
                        message_type+= ",";
                        message_json+= ",";
                    }else{
                        message_json += "{\"messages\": [";
                    }
                    message_type += await data.message[i].messageType;
                    var messageJson = await JSON.parse(data.message[i].json);
                    var messageTypeReal = await messageJson.type;
                    if(messageTypeReal == "imagemap"){
                        var messageImageReal = await messageJson.baseUrl;
                        if(messageImageReal.indexOf("?_ignored=") < 0){
                            messageJson.baseUrl = await messageImageReal+"?_ignored=";
                        }
                    }
                    messageJson = await findAndReplaceLink(messageJson,data.messageId);

                    messageJson = await JSON.stringify(messageJson);
                    message_json += await messageJson;
                    if(i == parseInt(data.message.length)-1){
                        message_json += "]}";
                    }
                }
                
                message_json = JSON.stringify(message_json);
                if(data.broadcastType == "sendnow" && data.segmentId == 0){
                    broadcastMessage(updateMessage,data,res,message_type,message_json);
                }else if(data.broadcastType == "sendnow" && data.segmentId > 0){
                    narrowcastMessage(updateMessage,data,res,message_type,message_json);
                }else{
                    var requestId = "";
                    updateMessage(200,requestId,data,res,message_type,message_json);
                }
            }else{
                await utils.showResult(res,false,"This message is not existing.");
            }
        }else{
            await utils.showResult(res,false,"no sent data");
        }
    }catch(err){
        console.log(err);
        await utils.showResult(res,false,"Something went wrong.",err);
    }
}

async function updateMessage(statusCode,requestId,data,res,message_type,message_json){
    try{
        if(statusCode == 200){
            var status = "";
            if(data.broadcastType == "sendnow"){
                status = "success";
            }else if(data.broadcastType == "draft"){
                status = "draft";
            }else{
                status = "pending";
            }
            var pool = await sql.connect(config);
            var recordset;
            if(data.broadcastType == "sendnow"){
                recordset = await pool.request()
                .input('messageId', sql.Int, data.messageId)
                .input('image_message', sql.NVarChar, data.imageMessage)
                .input('requestId', sql.VarChar, requestId)
                .input('title', sql.NVarChar, data.title)
                .input('pillar_id', sql.NVarChar, data.pillarId)
                .input('product_id', sql.NVarChar, data.productId)
                .input('tag', sql.NVarChar, data.tag)
                .input('segment_id', sql.Int, data.segmentId)
                .input('broadcast_time', sql.DateTimeOffset, new Date(data.broadcastTime))
                .input('broadcast_type', sql.VarChar, data.broadcastType)
                .input('message_type', sql.VarChar, message_type)
                .input('json', sql.NText, message_json)
                .input('status', sql.VarChar, status)
                .input('pillar2_id', sql.NVarChar, data.pillarId2)
                .input('campaign_id', sql.NVarChar, data.campaignId)
                .query('UPDATE message SET TITLE = @title, IMAGE_MESSAGE = @image_message, REQUEST_ID = @requestId, PILLAR_ID = @pillar_id, PILLAR2_ID = @pillar2_id, CAMPAIGN_ID = @campaign_id, SEGMENT_ID = @segment_id, BROADCAST_TIME = @broadcast_time, BROADCAST_TYPE = @broadcast_type, MESSAGE_TYPE = @message_type, JSON = @json, STATUS = @status, PRODUCT_ID = @product_id, TAG = @tag WHERE ID = @messageId');
            }else{
                recordset = await pool.request()
                .input('messageId', sql.Int, data.messageId)
                .input('title', sql.NVarChar, data.title)
                .input('image_message', sql.NVarChar, data.imageMessage)
                .input('pillar_id', sql.NVarChar, data.pillarId)
                .input('product_id', sql.NVarChar, data.productId)
                .input('tag', sql.NVarChar, data.tag)
                .input('segment_id', sql.Int, data.segmentId)
                .input('broadcast_time', sql.DateTimeOffset, new Date(data.broadcastTime))
                .input('broadcast_type', sql.VarChar, data.broadcastType)
                .input('message_type', sql.VarChar, message_type)
                .input('json', sql.NText, message_json)
                .input('status', sql.VarChar, status)
                .input('pillar2_id', sql.NVarChar, data.pillarId2)
                .input('campaign_id', sql.NVarChar, data.campaignId)
                .query('UPDATE message SET TITLE = @title, IMAGE_MESSAGE = @image_message, PILLAR_ID = @pillar_id, PILLAR2_ID = @pillar2_id, CAMPAIGN_ID = @campaign_id, SEGMENT_ID = @segment_id, BROADCAST_TIME = @broadcast_time, BROADCAST_TYPE = @broadcast_type, MESSAGE_TYPE = @message_type, JSON = @json, STATUS = @status, PRODUCT_ID = @product_id, TAG = @tag WHERE ID = @messageId');
            }
            await sql.close();
            if(recordset.rowsAffected > 0){
                var result = [{
                    "title": data.title,
                    "imageMessage": data.imageMessage,
                    "pillarId": data.pillarId,
                    "productId": data.productId,
                    "tag": data.tag,
                    "segmentId": data.segmentId,
                    "broadcastTime": data.broadcastTime,
                    "broadcastType": data.broadcastType,
                    "message_type": message_type,
                    "json": JSON.parse(message_json),
                    "status": status,
                    "pillarId2": data.pillarId,
                    "campaign": data.campaign,
                }]
                await utils.showResult(res,true,"success",result);
            }else{
                await utils.showResult(res,false,"update unsuccess");
            }
        }else{
            await utils.showResult(res,false,"send unsuccess",result);  
        }
    }catch(err){
        console.log(err);
        await utils.showResult(res,false,"Something went wrong.",err);
    }
}

async function insertPersonalizeLink(data,res){

    try{
        if(data.messageId > 0 || data.messageId == -1){
       

            let sql = 'INSERT INTO shell_personalize_link (LINE_USER_ID,MESSAGE_ID,LINK, create_date) VALUES (?, ?, ?, NOW())';
            const rows = await pooldata.query(sql, [data.lineUserId, data.messageId, data.link]);
            if (rows.rowsAffected > 0) {
                await utils.showResult(res,true,"insert log success");
            } else {
                await utils.showResult(res,false,"log unsuccess");
            }
            
            /*
            var pool = await sql.connect(config);
            var recordset = await pool.request()
                .input('userlineid', sql.VarChar, data.lineUserId)
                .input('messageId', sql.VarChar, data.messageId)
                .input('link', sql.NVarChar, data.link)
                .query('INSERT INTO shell_personalize_link (LINE_USER_ID,MESSAGE_ID,LINK) VALUES (@userlineid,@messageId,@link)'); 
            if(recordset.rowsAffected > 0){
                await utils.showResult(res,true,"insert log success");
            }else{
                await utils.showResult(res,false,"insert log unsuccess");
            }
            */
        }else{
            await utils.showResult(res,false,"just test");
        }
    }catch(err){
        console.log("insertPersonalizeLink",err);
        await utils.showResult(res,false,"Something went wrong.",err);
    }
}

async function getNextMessageId(){
    try{
        var pool = await sql.connect(config);
        var recordset = await pool.request()
            .query('SELECT IDENT_CURRENT(\'message\')+1 AS MESSAGE_ID');
        if(recordset.recordset.length > 0){
            return recordset.recordset[0]['MESSAGE_ID'];
        }else{
            return 0;
        }
    }catch(err){
        console.log(err);
        return 0;
    }
}

async function findAndReplaceLink(object,nextMessageId) {
    for (var x in object) {
        if (object.hasOwnProperty(x)) {
            if (typeof object[x] == 'object') {
                await findAndReplaceLink(object[x],nextMessageId);
            }

            if (x == "uri" || x == "linkUri") { 
                if(x == "uri"){
                    if(object["uri"].indexOf("trck?") < 0){
                        object["uri"] = liffTrackUrl+"?url="+encodeURIComponent(encodeURIComponent(object["uri"]))+"&messageId="+nextMessageId;
                    }
                }else{
                    if(object["linkUri"].indexOf("trck?") < 0){
                        object["linkUri"] = liffTrackUrl+"?url="+encodeURIComponent(encodeURIComponent(object["linkUri"]))+"&messageId="+nextMessageId;
                    }
                }
            }
            
        }
    }
    return object;
}


async function getAllPersonaKeywords(res){
    try{
        var pool = await sql.connect(config);
        var recordset = await pool.request()
            .query('SELECT [ID],[LINE_USER_ID],[RESPONSE_SOURCE],[RESPONSE_KEYWORD],[CREATE_DATE] FROM [shell_bcdb].[dbo].[shell_personalize_response] WHERE ID > 1107224 ORDER BY ID');
        if(recordset.recordset.length > 0){
            await utils.showResult(res,true,"success",recordset.recordset);
        }else{
            await utils.showResult(res,false,"no product");
        }
    }catch(err){
        //console.log("allProducts",err);
        await utils.showResult(res,false,"Something went wrong.");
    }
}



async function getAllPersonaKeywordsv2(res, id, date){
    /*try{
        //var pool = await sql.connect(config);
        var pool = await mariadb.createPool(config);
        var recordset = await pool.request()
            .input('date', sql.DateTimeOffset, date)
            .input('id', sql.VarChar, id)
            .query("SELECT TOP 15000 [ID] as id,[LINE_USER_ID] as line_user_id,[RESPONSE_SOURCE] as response_source,[RESPONSE_KEYWORD] as response_keyword,[CREATE_DATE] as create_date FROM [shell_bcdb].[dbo].[shell_personalize_response] WHERE ID >= @id AND CREATE_DATE < '" + date + "' AND PUSH_DATE IS NULL ORDER BY ID");
        console.log(recordset.recordset.length, ' ', date);
            if(recordset.recordset.length > 0){
            //await utils.showResult(res,true,"success",recordset.recordset);
            return recordset.recordset;
        }else{
            await utils.showResult(res,false,"no data keywords");
        }
    }catch(err){
        //console.log("allProducts",err);
        await utils.showResult(res,false,"Something went wrong.");
    }
    */
   let conn;
   try {
       conn = await pooldb.getConnection();
       const rows = await conn.query("select * from shell_personalize_response");
       
       return rows;


   } catch(err) {
        await utils.showResult(res,false,"Something went wrong " + err);

   }
}

async function getAllPersonalink(res, id) {
    /*
    var recordset = await pool.request()
                .input('userlineid', sql.VarChar, data.lineUserId)
                .input('messageId', sql.VarChar, data.messageId)
                .input('link', sql.NVarChar, data.link)
                .query('INSERT INTO shell_personalize_link (LINE_USER_ID,MESSAGE_ID,LINK) VALUES (@userlineid,@messageId,@link)'); 
            
                */
    try{
        var pool = await sql.connect(config);
        var recordset = await pool.request()
            .input('id', sql.VarChar, id)
            .query('SELECT TOP 15000 [ID] as id,[LINE_USER_ID] as line_user_id,[MESSAGE_ID] as message_id,[LINK] as link,[CREATE_DATE] as create_date, current_timestamp as time FROM [shell_bcdb].[dbo].[shell_personalize_link] WHERE ID >= @id  ORDER BY ID');
        if(recordset.recordset.length > 0){
            //await utils.showResult(res,true,"success",recordset.recordset);
            return recordset.recordset;
        }else{
            await utils.showResult(res,false,"no product");
        }
    }catch(err){
        //console.log("allProducts",err);
        await utils.showResult(res,false,"Something went wrong.");
    }
}

async function getPushMessage(res, id) {
   
    try{
        var pool = await sql.connect(config);
        var recordset = await pool.request()
            .input('id', sql.VarChar, id)
            .query("SELECT [ID] as id ,[REQUEST_ID] as request_id,[TITLE] as title,[IMAGE_MESSAGE] as image_message,[PILLAR_ID] as pillar_id,[PRODUCT_ID] as product_id,[TAG] as tag,[SEGMENT_ID] as segment,[BROADCAST_TIME] as broadcast_time,[BROADCAST_TYPE] as broadcast_type,[MESSAGE_TYPE] as message_type,[JSON] as json,[CREATE_DATE] as create_date,[UPDATE_DATE] as update_date,[STATUS] as status,[IS_ACTIVE] as is_active,[UPDATE_BATCH_DATE] as update_bath_date,[PILLAR2_ID] as pillar2_id,[CAMPAIGN_ID] as campaign_id,[Branch] as branch, current_timestamp as time FROM [shell_bcdb].[dbo].[message] WHERE ID >= @id AND IS_ACTIVE = 1");
        if(recordset.recordset.length > 0){
            //await utils.showResult(res,true,"success",recordset.recordset);
            return recordset.recordset;
        }else{
            await utils.showResult(res,false, 'no personal data ' + id);
        }
    }catch(err){
        //console.log("allProducts",err);
        await utils.showResult(res,false,"Something went wrong.");
    }
}


async function updateSendpush(id, date) {
    var pool = await sql.connect(config);
    var recordset = await pool.request()
        .input('ID', sql.Int, id)
        .input('DATE', sql.DateTimeOffset, date)
        .query('UPDATE shell_personalize_link SET PUSH_DATE = @DATE WHERE ID = @ID');
    //await sql.close();
    if(recordset.rowsAffected > 0){
        //await utils.showResult(res,true,"delete success");

        return true;

    }else{
        //await utils.showResult(res,false,"delete unsuccess");
        return false;
    }

}


async function updateKeywordSendpush(id, date) {
    var pool = await sql.connect(config);
    var recordset = await pool.request()
        .input('ID', sql.Int, id)
        .input('DATE', sql.DateTimeOffset, date)
        .query('UPDATE shell_personalize_response SET PUSH_DATE = @DATE WHERE ID = @ID');
    //await sql.close();
    if(recordset.rowsAffected > 0){
        //await utils.showResult(res,true,"delete success");

        return true;

    }else{
        //await utils.showResult(res,false,"delete unsuccess");
        return false;
    }

}

module.exports = {
    createMessage,
    getAllSegments,
    getSegment,
    getAllPillars,
    getAllPillars2,
    getPillar,
    getAllMessages,
    getMessage,
    deleteMessage,
    editMessage,
    testMessage,
    insertPersonalizeLink,
    getAllTypes,
    getType,
    getProduct,
    getAllProducts,
    getAllCampaigns,
    getAllPersonalink,
    getAllPersonaKeywords,
    getAllPersonaKeywordsv2,
    getPushMessage,
    updateKeywordSendpush,
    updateSendpush
}
