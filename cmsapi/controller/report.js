const sql = require('mssql');
const config = require('../config').dbconfg;
const utils = require('../utility/utility');

async function getBroadcastReport(data,res){
    try{
        var prev_deliverycount = 0;
        var arr_insight = [];
        var pool = await sql.connect(config);
        var recordset = await pool.request()
                .input('dateStart', sql.VarChar, data.dateStart+' 00:00')
                .input('dateEnd', sql.VarChar, data.dateEnd+' 23:59')
                .query('SELECT MESSAGE_ID, JSON, DATENAME(dw,BROADCAST_TIME) AS BROADCAST_DN, FORMAT(BROADCAST_TIME, \'HH:mm\') AS BROADCAST_HM, CONVERT(varchar,BROADCAST_TIME,6) AS BROADCAST_D,IMAGE_MESSAGE,REQUEST_ID,TITLE,PILLAR FROM message_report WHERE BROADCAST_TIME BETWEEN @dateStart AND @dateEnd ORDER BY BROADCAST_TIME,CREATE_DATE ASC');        
        if(recordset.recordset.length > 0){
            arr_insight = [];
            for(var i = 0; i < recordset.recordset.length; i++){
                var body = JSON.parse(recordset.recordset[i]['JSON']);
                var delivercount = parseInt((body.overview.delivered != null? body.overview.delivered : 0));
                var interest = parseInt((body.overview.uniqueImpression != null? body.overview.uniqueImpression : 0));
                var uuclick = parseInt((body.overview.uniqueClick != null? body.overview.uniqueClick : 0));
                var fanLost = prev_deliverycount-delivercount;
                prev_deliverycount = delivercount;
                var impression = 0;
                if(body.messages.length > 0){
                    for(var key2 in body.messages){
                        impression += parseInt((body.messages[key2].impression != null? body.messages[key2].impression : 0));
                    }
                }
                var click = 0;
                if(body.clicks.length > 0){
                    for(var key3 in body.clicks){
                        click += parseInt((body.clicks[key3].click != null? body.clicks[key3].click : 0));
                    }
                }
                var interest_percent = 0;
                var fanLost_percent = 0;
                if(delivercount > 0){
                    interest_percent = (interest/delivercount)*100;
                    fanLost_percent = (fanLost/delivercount)*100
                }
                
                var ctr = 0;
                var worthiness = 0;
                if(interest > 0){
                    ctr = (uuclick/interest)*100;
                    worthiness = (uuclick-fanLost)/interest;
                }
                var result = {
                    messageId: recordset.recordset[i]['MESSAGE_ID'],
                    title: recordset.recordset[i]['TITLE'],
                    pillar: recordset.recordset[i]['PILLAR'],
                    requestId: recordset.recordset[i]['REQUEST_ID'],
                    broadcastDate: recordset.recordset[i]['BROADCAST_D'],
                    broadcastDayname: recordset.recordset[i]['BROADCAST_DN'],
                    broadcastHourmin: recordset.recordset[i]['BROADCAST_HM'],
                    messageImage: recordset.recordset[i]['IMAGE_MESSAGE'],
                    delivercount: delivercount,
                    fanLost: fanLost,
                    impression: impression,
                    interest: interest,
                    uuclick: uuclick,
                    click: click,
                    interestPercent: interest_percent,
                    ctr: ctr,
                    worthiness: worthiness,
                    fanLostPercent: fanLost_percent
                };
                arr_insight.push(result);
            }
            await sql.close();
            if(arr_insight.length > 0){
                await utils.showResult(res,true,"success",arr_insight);
            }else{
                await utils.showResult(res,false,"No report",[]);
            }
        }else{
            await sql.close();
            await utils.showResult(res,false,"No report");
        }
    }catch(err){
        console.log("getdata error: ",err)
        await utils.showResult(res,false,"Something went wrong.");
    }
}

async function getBoardcastLinkReport(data,res){
    try{
        var arr_insight = [];
        var arr_linkDetail = [];
        var pool = await sql.connect(config);
        var recordset = await pool.request()
                .input('messageId', sql.Int, data.messageId)
                .query('SELECT MESSAGE_ID, JSON FROM message_report WHERE MESSAGE_ID = @messageId');        
        if(recordset.recordset.length > 0){
            arr_insight = [];
            arr_linkDetail = [];
            var body = JSON.parse(recordset.recordset[0]['JSON']);
            var arr_clicks = body.clicks;
            console.log(arr_clicks);
            if(arr_clicks.length > 0){
                for(var j = 0; j< arr_clicks.length; j++){
                    var orderSent = arr_clicks[j].seq;
                    var url = arr_clicks[j].url;
                    var impressions = body.messages[j].impression;
                    var clicks = arr_clicks[j].click;
                    var clickedUserCount = arr_clicks[j].uniqueClick;
                    var clickTroughRate = (clicks*100)/impressions;
                    var result_link = {
                        url: url,
                        orderSent: orderSent,
                        impressions: impressions,
                        clicks: clicks,
                        clickTroughRate: parseFloat(clickTroughRate.toFixed(2)),
                        clickedUserCount: clickedUserCount
                    };
                    arr_linkDetail.push(result_link);
                }
            }
            var result = {
                messageId: recordset.recordset[0]['MESSAGE_ID'],
                links: arr_linkDetail
            };
            arr_insight.push(result);
            await sql.close();
            if(arr_linkDetail.length > 0){
                await utils.showResult(res,true,"success",arr_insight);
            }else{
                await utils.showResult(res,false,"No link detail report",arr_insight);
            }
        }else{
            await sql.close();
            await utils.showResult(res,false,"No report");
        }
    }catch(err){
        console.log("error: ",err)
        await utils.showResult(res,false,"Something went wrong.");
    }
}

async function getYearReport(data,res){
    try{
        var arr_month = ['Jan','Feb','March','April','May','June','July','Aug','Sep','Oct','Nov','Dec'];
        var arr_insight_year = [];
        var arr_avgCTR = [];
        var arr_avgWorthiness = [];
        var pool = await sql.connect(config);
        for(var i = 0; i < arr_month.length; i++){
            var delivercountTotal = 0;
            var uuclickTotal = 0;
            var clickTotal = 0;
            var avgCTR = 0;
            var noOfContent = 0;
            var interestTotal = 0;
            var impressionTotal = 0;
            var fanLostTotal = 0;
            var avgView = 0;
            var avgClick = 0;
            var avgFanLost = 0;
            var avgWorthinessPercent = 0;
            var prev_deliverycountTotal = 0;
            var month = i+1;
            var recordset = await pool.request()
                .input('month', sql.Int, month)
                .input('year', sql.Int, data.year)
                .query('SELECT * FROM message_report WHERE MONTH(BROADCAST_TIME) = @month AND YEAR(BROADCAST_TIME) = @year ORDER BY BROADCAST_TIME,CREATE_DATE ASC');
            if(recordset.recordset.length > 0){
                for(var j = 0; j < recordset.recordset.length; j++){
                    noOfContent += 1;
                    var body = await JSON.parse(recordset.recordset[j]['JSON']);
                    delivercountTotal += parseInt((body.overview.delivered != null? body.overview.delivered : 0));
                    interestTotal += parseInt((body.overview.uniqueImpression != null? body.overview.uniqueImpression : 0));
                    uuclickTotal += parseInt((body.overview.uniqueClick != null? body.overview.uniqueClick : 0));
                    fanLostTotal = prev_deliverycountTotal-delivercountTotal;
                    prev_deliverycountTotal = delivercountTotal;
                    if(body.messages.length > 0){
                        for(var key2 in body.messages){
                            impressionTotal += parseInt((body.messages[key2].impression != null? body.messages[key2].impression : 0));
                        }
                    }
                    if(body.clicks.length > 0){
                        for(var key3 in body.clicks){
                            clickTotal += parseInt((body.clicks[key3].click != null? body.clicks[key3].click : 0));
                        }
                    }
                }
                if(delivercountTotal > 0){
                    avgCTR = (clickTotal/delivercountTotal)*100;
                }
                if(noOfContent > 0){
                    avgView = delivercountTotal/noOfContent;
                    avgClick = uuclickTotal/noOfContent;
                    avgFanLost = fanLostTotal/noOfContent;
                }
                if(interestTotal > 0){
                    avgWorthinessPercent = (avgClick-avgFanLost)/interestTotal;
                }
                var result = {
                    month: i,
                    delivercountTotal: delivercountTotal,
                    clickTotal: uuclickTotal,
                    avgCTR: avgCTR.toFixed(2),
                    noOfContent: noOfContent,
                    interestTotal: interestTotal,
                    avgView: avgView.toFixed(2),
                    avgClick: avgClick.toFixed(2),
                    avgFanLost: avgFanLost.toFixed(2),
                    avgWorthinessPercent: avgWorthinessPercent.toFixed(2)
                };
                arr_insight_year.push(result);
                arr_avgCTR.push(avgCTR.toFixed(2));
                arr_avgWorthiness.push(avgWorthinessPercent.toFixed(2));
            }else{
                var result = {
                    month: i,
                    delivercountTotal: null,
                    clickTotal: null,
                    avgCTR: null,
                    noOfContent: null,
                    interestTotal: null,
                    avgView: null,
                    avgClick: null,
                    avgFanLost: null,
                    avgWorthinessPercent: null
                };
                arr_insight_year.push(result);
                arr_avgCTR.push(null);
                arr_avgWorthiness.push(null);
            }
        }
        var json = [{
            "status": true,
            "message": "success",
            "avgCTRAll": arr_avgCTR,
            "avgWorthinessAll": arr_avgWorthiness,
            "result": arr_insight_year
        }];
        await sql.close();
        res.json(json);
    }catch(err){
        console.log("error: ",err)
        await utils.showResult(res,false,"Something went wrong.");
    }
}


async function getMonthReport(data,res){
    try{
        var prev_deliverycount = 0;
        var arr_insight_month = [];
        var arr_delivercount = [];
        var arr_interest = [];
        var arr_uuclick = [];
        var arr_fanlost = [];
        var arr_date = [];
        var pool = await sql.connect(config);
        var recordset = await pool.request()
            .input('month', sql.Int, data.month)
            .input('year', sql.Int, data.year)
            .query('SELECT *, (FORMAT(BROADCAST_TIME, \'d MMM\') +\' \'+ FORMAT(BROADCAST_TIME, \'HH:mm\')) AS BROADCAST_D FROM message_report WHERE MONTH(BROADCAST_TIME) = @month AND YEAR(BROADCAST_TIME) = @year ORDER BY BROADCAST_TIME,CREATE_DATE ASC');  
        if(recordset.recordset.length > 0){
            for(var j = 0; j < recordset.recordset.length; j++){
                var body = await JSON.parse(recordset.recordset[j]['JSON']);
                var delivercount = parseInt((body.overview.delivered != null? body.overview.delivered : 0));
                var interest = parseInt((body.overview.uniqueImpression != null? body.overview.uniqueImpression : 0));
                var uuclick = parseInt((body.overview.uniqueClick != null? body.overview.uniqueClick : 0));
                var fanLost = prev_deliverycount-delivercount;
                prev_deliverycount = delivercount;
                var arr_month = ['Jan','Feb','March','April','May','June','July','Aug','Sep','Oct','Nov','Dec'];
                var result = {
                    messageId: recordset.recordset[j]['MESSAGE_ID'],
                    date: recordset.recordset[j]['BROADCAST_D'],
                    monthId: data.month,
                    month: arr_month[data.month-1],
                    year: data.year,
                    delivercount: delivercount,
                    interest: interest,
                    uuclick: uuclick,
                    fanLost: fanLost
                };
                arr_insight_month.push(result);
                arr_date.push(recordset.recordset[j]['BROADCAST_D']);
                arr_delivercount.push(delivercount);
                arr_interest.push(interest);
                arr_uuclick.push(uuclick);
                arr_fanlost.push(fanLost);
            }
            await sql.close();
            var json = [{
                "status": true,
                "message": "success",
                "dateAll": arr_date,
                "delivercountAll": arr_delivercount,
                "interestAll": arr_interest,
                "uuclickAll": arr_uuclick,
                "fanlostAll": arr_fanlost,
                "result": arr_insight_month
            }];
            res.json(json);
        }else{
            await sql.close();
            await utils.showResult(res,false,"No report");
        }
    }catch(err){
        console.log("error: ",err)
        await utils.showResult(res,false,"Something went wrong.");
    }
}


async function updateBatchReport(id) {
    try{
       
            var pool = await sql.connect(config);
            var recordset = await pool.request()
                .input('messageId', sql.Int, id)
                .input('date', sql.DateTimeOffset, new Date())
                .query('UPDATE message SET UPDATE_BATCH_DATE = @date WHERE ID = @messageId');
            //await sql.close();
            if(recordset.rowsAffected > 0){
                //await utils.showResult(res,true,"update success");
                return true;
            }else{
                //await utils.showResult(res,false,"update unsuccess");
                return false;
            }
        
    }catch(err){
        //await utils.showResult(res,false,"Something went wrong.");
        console.log('error ', err);
        //return false;
    }
}



async function getInsight() {
    ///****** Script for SelectTopNRows command from SSMS  ******/
/*SELECT ID, REQUEST_ID, TITLE, PILLAR_ID AS PILLAR, PRODUCT_ID AS PRODUCT, TAG, BROADCAST_TIME, IMAGE_MESSAGE, JSON
FROM [shell_bcdb].[dbo].[message] WHERE status = 'success' AND UPDATE_BATCH_DATE IS NULL; */

    try {
        var pool = await sql.connect(config);
        var recordset = await pool.request()
                .query(`SELECT TOP 15000 ID, REQUEST_ID, TITLE, PILLAR_ID AS PILLAR, PRODUCT_ID AS PRODUCT, TAG, BROADCAST_TIME, IMAGE_MESSAGE, JSON FROM [shell_bcdb].[dbo].[message] WHERE status = 'success' AND UPDATE_BATCH_DATE IS NULL`);        
        if(recordset.recordset.length > 0){
            await sql.close();
            return recordset.recordset;
            
        } else {
            await sql.close();
            //await utils.showResult(res,false,"No report");
            return false;
        }

    }catch(err){
        console.log("getdata error: ",err)
        //await utils.showResult(res,false,"Something went wrong.");
    }

}


async function saveInsight(data) {
    var pool = await sql.connect(config);
    var recordset = await pool.request()
        .input('title', sql.NVarChar, data.TITLE)
        .input('messageid', sql.VarChar, data.MESSAGE_ID)
        .input('requestid', sql.VarChar, data.REQUEST_ID)
        .input('image_message', sql.NVarChar, data.IMAGE_MESSAGE)
        .input('pillar_id', sql.NVarChar, data.PILLAR)
        .input('product_id', sql.NVarChar, data.PRODUCT)
        .input('tag', sql.NVarChar, data.TAG)
        .input('broadcast_time', sql.DateTimeOffset, new Date(data.BROADCAST_TIME))
        .input('json', sql.NText, data.JSON_DATA)
        .query('INSERT INTO message_report (MESSAGE_ID, REQUEST_ID, TITLE, PILLAR, PRODUCT, TAG, BROADCAST_TIME, IMAGE_MESSAGE, JSON) VALUES (@messageId, @requestId, @title, @pillar_id, @product_id, @tag, @broadcast_time, @image_message, @json)');
        //await sql.close();
    if(recordset.rowsAffected > 0){
        return true;

    } else {
        return false;

    }

}



module.exports = {
    getBroadcastReport,
    getYearReport,
    getMonthReport,
    getBoardcastLinkReport,
    updateBatchReport,
    getInsight,
    saveInsight
}


