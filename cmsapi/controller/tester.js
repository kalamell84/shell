const sql = require('mssql');
const config = require('../config').dbconfg;
const utils = require('../utility/utility');
const request = require('request');
const line = require('@line/bot-sdk');

async function genTrackCode(res){
    var track_code = await randomTrackCode(32,'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
    var result = [{
        "trackCode": track_code
    }]
    await utils.showResult(res,true,"generate success",result);
}

async function randomTrackCode(length, chars) {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}

async function createTester(data,res){
    try{
        var hasTrackCode = await checkExistingTrackCode(data);
        if(!hasTrackCode){
            var pool = await sql.connect(config);
            var recordset = await pool.request()
                .input('title', sql.VarChar, data.title)
                .input('trackCode', sql.VarChar, data.trackCode)
                .query('INSERT INTO tester_user (TITLE,TRACK_CODE) VALUES (@title,@trackCode)');
            if(recordset.rowsAffected > 0){
                var result = [{
                    "title": data.title,
                    "trackCode": data.trackCode
                }]
                await utils.showResult(res,true,"insert success",result);
            }else{
                await utils.showResult(res,false,"insert unsuccess");
            }
        }else{
            await utils.showResult(res,false,"This track code has duplicated, please generate new track code.");
        }
    }catch(err){
        console.log(err);
        await utils.showResult(res,false,"Something went wrong.");
    }
}

async function checkExistingTrackCode(data){
    try{
        var pool = await sql.connect(config);
        var recordset = await pool.request()
            .input('trackCode', sql.VarChar, data.trackCode)
            .query('SELECT * FROM tester_user WHERE TRACK_CODE = @trackCode');
        if(recordset.recordset.length > 0){
            return true;
        }else{
            return false;
        }
    }catch(err){
        console.log("err",err);
        return false;
    }
}

async function checkExistingLineUserId(data){
    try{
        var pool = await sql.connect(config);
        var recordset = await pool.request()
            .input('lineUserId', sql.VarChar, data.lineUserId)
            .query('SELECT * FROM tester_user WHERE LINE_USER_ID = @lineUserId AND IS_ACTIVATE = 1');
        if(recordset.recordset.length > 0){
            return true;
        }else{
            return false;
        }
    }catch(err){
        console.log("err",err);
        return false;
    }
}

async function checkExistingTester(data){
    try{
        var pool = await sql.connect(config);
        var recordset = await pool.request()
            .input('testerId', sql.Int, data.testerId)
            .query('SELECT * FROM tester_user WHERE ID = @testerId');
        if(recordset.recordset.length > 0){
            return true;
        }else{
            return false;
        }
    }catch(err){
        console.log("err",err);
        return false;
    }
}


async function updateLineUserId(data,res){
    try{
        var hasTrackCode = await checkExistingTrackCode(data);
        if(hasTrackCode){
            var hasLineUserId = await checkExistingLineUserId(data);
            if(!hasLineUserId){
                var pool = await sql.connect(config);
                var recordset = await pool.request()
                    .input('trackCode', sql.VarChar, data.trackCode)
                    .input('lineUserId', sql.VarChar, data.lineUserId)
                    .query('UPDATE tester_user SET LINE_USER_ID = @lineUserId, IS_ACTIVATE = 1 WHERE TRACK_CODE = @trackCode AND IS_ACTIVATE = 0');
                if(recordset.rowsAffected > 0){
                    var result = [{
                        "lineUserId": data.lineUserId,
                        "trackCode": data.trackCode
                    }]
                    await utils.showResult(res,true,"ALREADY ACTIVATED",result);
                }else{
                    await utils.showResult(res,false,"This track code has already been activated.");
                }
            }else{
                await utils.showResult(res,false,"This user has already been activated.");
            }
        }else{
            await utils.showResult(res,false,"This track code is not existing.");
        }
    }catch(err){
        console.log(err);
        await utils.showResult(res,false,"Something went wrong.");
    }
}

async function getAllTesters(res){
    try{
        var pool = await sql.connect(config);
        var recordset = await pool.request()
            .query('SELECT * FROM tester_user ORDER BY ID DESC');
        if(recordset.recordset.length > 0){
            await sql.close();
            await utils.showResult(res,true,"success",recordset.recordset);
        }else{
            await sql.close();
            await utils.showResult(res,false,"no test user");
        }
    }catch(err){
        console.log(err);
        await utils.showResult(res,false,"Something went wrong.");
    }
}

async function deleteTester(data,res){
    try{
        var has_tester = await checkExistingTester(data);
        if(has_tester){
            var pool = await sql.connect(config);
            var recordset = await pool.request()
                .input('testerId', sql.Int, data.testerId)
                .query('DELETE FROM tester_user WHERE ID = @testerId');
            if(recordset.rowsAffected > 0){
                await utils.showResult(res,true,"delete success");
            }else{
                await utils.showResult(res,false,"delete unsuccess");
            }
        }else{
            await utils.showResult(res,false,"This tester is not existing.");
        }
    }catch(err){
        await utils.showResult(res,false,"Something went wrong.");
    }
}


async function updateTester(data,res){
    try{
        var has_tester = await checkExistingTester(data);
        if(has_tester){
            var pool = await sql.connect(config);
            var recordset = await pool.request()
                .input('testerId', sql.Int, data.testerId)
                .input('title', sql.NVarChar, data.title)
                .query('UPDATE tester_user SET TITLE = @title WHERE ID = @testerId AND IS_ACTIVATE = 1');
            if(recordset.rowsAffected > 0){
                var result = [{
                    "testerId": data.testerId,
                    "title": data.title
                }]
                await utils.showResult(res,true,"update success",result);
            }else{
                await utils.showResult(res,false,"update unsuccess");
            }
        }else{
            await utils.showResult(res,false,"This tester is not existing.");
        }
    }catch(err){
        await utils.showResult(res,false,"Something went wrong.");
    }
}

async function getTester(data,res){
    try{
        var pool = await sql.connect(config);
        var recordset = await pool.request()
                .input('testerId', sql.Int, data.testerId)
                .query('SELECT * FROM tester_user WHERE ID = @testerId');
        if(recordset.recordset.length > 0){
            await utils.showResult(res,true,"success",recordset.recordset);
        }else{
            await utils.showResult(res,false,"This tester is not found. or not active.");
        }
    }catch(err){
        console.log(err);
        await utils.showResult(res,false,"Something went wrong.");
    }
}

module.exports = {
    genTrackCode,
    createTester,
    updateLineUserId,
    getAllTesters,
    deleteTester,
    updateTester,
    getTester
}
