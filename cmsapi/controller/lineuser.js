const sql = require('mssql');
const crypto = require('crypto-js');
const config = require('../config').dbconfg;
const secret = require('../config').secret;
const utils = require('../utility/utility');



async function getUserById(line_user_id, res){
    try{
        var pool = await sql.connect(config);
        var recordset = await pool.request()
            .input('line_user_id', sql.NVarChar, line_user_id)
            .query('SELECT * FROM line_user_info WHERE LINE_USER_ID = @line_user_id');
        if(recordset.recordset.length > 0){
            await utils.showResult(res,true,"success",recordset.recordset);
        }else{
            await utils.showResult(res,false,"This user_id is not found.",recordset.recordset);
        }
    }catch(err){
        await utils.showResult(res,false,"Something went wrong.");
    }
}

module.exports = {
    getUserById
}