const sql = require('mssql');
const config = require('../config').dbconfg;
const dbconfig = require('../config').dbconfig;
const utils = require('../utility/utility');
const fs = require('fs');
const rimraf = require("rimraf");

const mariadb = require('mariadb');
const pooldb = mariadb.createPool(dbconfig);

async function uploadImgIntoDB(req,res){
    try{
        var has_image = await checkExistingImage(req);
        if(!has_image){
            var data = req.body;
            var file = req.file;
            var pool = await sql.connect(config);
            var recordset = await pool.request()
                .input('imageName', sql.NVarChar, file.filename)
                .input('imageFolder', sql.Int, data.imageFolderId)
                .input('imagePath', sql.NVarChar, (file != null ? file.path.replace("public\\","") : ""))
                .input('imageTag', sql.NVarChar, data.imageTag)
                .query('INSERT INTO cms_image (IMAGE_NAME,IMAGE_FOLDER,IMAGE_PATH,IMAGE_TAG) VALUES (@imageName,@imageFolder,@imagePath,@imageTag)');
            if(recordset.rowsAffected > 0){
                var result = [{
                    "imageName": file.filename,
                    "imageFolder": data.imageFolder,
                    "imagePath": file.path.replace("public\\",""),
                    "imageTag": data.imageTag
                }]
                await sql.close();
                await utils.showResult(res,true,"upload success",result);
            }else{
                await utils.showResult(res,false,"upload unsuccess");
            }
        }else{
            await sql.close();
            await utils.showResult(res,false,"This image is duplicate.");
        }
    }catch(err){
        console.log("uploadImgIntoDB",err);
        await utils.showResult(res,false,"Something went wrong.",err);
    }
}

async function checkExistingImage(req){
    try{
        var data = req.body;
        var file = req.file;
        var imageName = data.imageName+"."+data.imageExtension;
        var pool = await sql.connect(config);
        var recordset = await pool.request()
            .input('imageName', sql.NVarChar, imageName)
            .input('imageFolder', sql.Int, data.imageFolderId)
            .query('SELECT * FROM cms_image WHERE IMAGE_NAME = @imageName AND IMAGE_FOLDER = @imageFolder');
        if(recordset.recordset.length > 0){
            await sql.close();
            return true;
        }else{
            await sql.close();
            return false;
        }
    }catch(err){
        console.log("checkExistingImage",err);
        return false;
    }
}

async function createFolder(data,res){
    try{
        var has_folder = await checkExistingFolderName(data);
        if(!has_folder){
            var pool = await sql.connect(config);
            var recordset = await pool.request()
                .input('imageFolder', sql.NVarChar, data.imageFolder)
                .query('INSERT INTO cms_image_folder (FOLDER_NAME) VALUES (@imageFolder)');
            await sql.close();
            if(recordset.rowsAffected > 0){
                var result = [{
                    "imageFolder": data.imageFolder
                }]
                await utils.showResult(res,true,"create success",result);
            }else{
                await utils.showResult(res,false,"create unsuccess");
            }
        }else{
            await utils.showResult(res,false,"This folder is existing.");
        }
    }catch(err){
        console.log("createFolder",err);
        await utils.showResult(res,false,"Something went wrong.",err);
    }
}

async function checkExistingFolderName(data){
    try{
        var pool = await sql.connect(config);
        var recordset = await pool.request()
            .input('imageFolder', sql.NVarChar, data.imageFolder)
            .query('SELECT * FROM cms_image_folder WHERE FOLDER_NAME = @imageFolder');
        await sql.close();
        if(recordset.recordset.length > 0){
            return true;
        }else{
            return false;
        }
    }catch(err){
        console.log("checkExistingFolderName",err);
        return false;
    }
}

async function deleteFolder(data,res){
    try{
        var has_item = await checkExistingItemFolder(data);
        if(!has_item){
            var folderName = await getFolderName(data); 
            if(folderName != null){
                rimraf('uploads\\'+folderName,  function () { console.log("delete folder done"); });
            }
            var pool = await sql.connect(config);
            var recordset = await pool.request()
                .input('imageFolderId', sql.Int, data.imageFolderId)
                .query('DELETE cms_image_folder WHERE ID = @imageFolderId');
            await sql.close();
            if(recordset.rowsAffected > 0){
                await utils.showResult(res,true,"delete success");
            }else{
                await utils.showResult(res,false,"delete unsuccess");
            }
        }else{
            await utils.showResult(res,false,"There are items in this folder, please delete all items before.");
        }
    }catch(err){
        console.log("deleteFolder",err);
        await utils.showResult(res,false,"Something went wrong.",err);
    }
}

async function checkExistingItemFolder(data){
    try{
        var pool = await sql.connect(config);
        var recordset = await pool.request()
            .input('imageFolderId', sql.Int, data.imageFolderId)
            .query('SELECT * FROM cms_image WHERE IMAGE_FOLDER = @imageFolderId');
        await sql.close();
        if(recordset.recordset.length > 0){
            return true;
        }else{
            return false;
        }
    }catch(err){
        console.log("checkExistingItemFolder",err);
        return false;
    }
}

async function updateFolder(data,res){
    try{
        var has_folder = await checkExistingFolder(data);
        if(has_folder){
            var pool = await sql.connect(config);
            var recordset = await pool.request()
                .input('imageFolderId', sql.Int, data.imageFolderId)
                .input('imageFolder', sql.NVarChar, data.imageFolder)
                .query('UPDATE cms_image_folder SET FOLDER_NAME = @imageFolder WHERE ID = @imageFolderId');
            await sql.close();
            if(recordset.rowsAffected > 0){
                await utils.showResult(res,true,"update success");
            }else{
                await utils.showResult(res,false,"update unsuccess");
            }
        }else{
            await utils.showResult(res,false,"This folder is not existing.");
        }
    }catch(err){
        console.log("updateFolder",err);
        await utils.showResult(res,false,"Something went wrong.",err);
    }
}

async function checkExistingFolder(data){
    try{
        var pool = await sql.connect(config);
        var recordset = await pool.request()
            .input('imageFolderId', sql.Int, data.imageFolderId)
            .query('SELECT * FROM cms_image_folder WHERE ID = @imageFolderId');
        await sql.close();
        if(recordset.recordset.length > 0){
            return true;
        }else{
            return false;
        }
    }catch(err){
        console.log("checkExistingFolder",err);
        return false;
    }
}

async function getImageFolders(res){
    try{
        var pool = await sql.connect(config);
        var recordset = await pool.request()
            .query('SELECT (CASE WHEN cif.ID IS null THEN 0 ELSE cif.ID END) AS IMAGE_FOLDER_ID,(CASE WHEN cif.FOLDER_NAME IS null THEN \'Default\' ELSE cif.FOLDER_NAME END) AS IMAGE_FOLDER,COUNT(ci.IMAGE_FOLDER) AS NUM_OF_FOLDER FROM cms_image_folder cif FULL OUTER JOIN cms_image ci ON cif.ID = ci.IMAGE_FOLDER GROUP BY ci.IMAGE_FOLDER,cif.ID,cif.FOLDER_NAME ORDER BY IMAGE_FOLDER_ID');
        await sql.close();
        if(recordset.rowsAffected > 0){
            var checkDefaultItem = await recordset.recordset[0].IMAGE_FOLDER_ID;
            if(checkDefaultItem == 0){
                await utils.showResult(res,true,"success",recordset.recordset);
            }else{
                var result = {
                    IMAGE_FOLDER_ID: 0,
                    IMAGE_FOLDER: "Default",
                    NUM_OF_FOLDER: 0
                };
                await recordset.recordset.unshift(result);
                await utils.showResult(res,true,"success",recordset.recordset);
            }
        }else{
            var result = [{
                "IMAGE_FOLDER_ID": 0,
                "IMAGE_FOLDER": "Default",
                "NUM_OF_FOLDER": 0
            }]
            await utils.showResult(res,true,"success",result);
        }
    }catch(err){
        console.log("getImageFolders",err);
        await utils.showResult(res,false,"Something went wrong.",err);
    }
}

async function getImageList(data,res){
    try{
        var pool = await sql.connect(config);
        var recordset = await pool.request()
            .input('imageFolderId', sql.Int, data.imageFolderId)
            .query('SELECT ID AS IMAGE_ID, IMAGE_NAME, IMAGE_PATH, IMAGE_TAG FROM cms_image WHERE IMAGE_FOLDER = @imageFolderId ORDER BY CREATE_DATE DESC');
        await sql.close();
        if(recordset.rowsAffected > 0){
            await utils.showResult(res,true,"success",recordset.recordset);
        }else{
            await utils.showResult(res,false,"no image");
        }
    }catch(err){
        console.log("getImageList",err);
        await utils.showResult(res,false,"Something went wrong.",err);
    }
}

async function deleteImage(data,res){
    try{
        var has_item = await checkExistingImageId(data);
        if(has_item){
            
            var filePath = await getFilePath(data); 
           
            if(filePath != null){
                fs.unlinkSync("public\\"+filePath);
            }
            var pool = await sql.connect(config);
            var recordset = await pool.request()
                .input('imageId', sql.Int, data.imageId)
                .query('DELETE cms_image WHERE ID = @imageId');
            await sql.close();
            if(recordset.rowsAffected > 0){
                await utils.showResult(res,true,"delete success");
            }else{
                await utils.showResult(res,false,"delete unsuccess");
            }
        }else{
            await utils.showResult(res,false,"no image ID");
        }
    }catch(err){
        console.log("deleteImage",err);
        await utils.showResult(res,false,"Something went wrong.",err);
    }
}

async function checkExistingImageId(data){
    try{
        var pool = await sql.connect(config);
        var recordset = await pool.request()
            .input('imageId', sql.Int, data.imageId)
            .query('SELECT * FROM cms_image WHERE ID = @imageId');
        await sql.close();
        if(recordset.recordset.length > 0){
            return true;
        }else{
            return false;
        }
    }catch(err){
        console.log("checkExistingImageId",err);
        return false;
    }
}

async function getFilePath(data){
    try{
        var pool = await sql.connect(config);
        var recordset = await pool.request()
            .input('imageId', sql.Int, data.imageId)
            .query('SELECT IMAGE_PATH FROM cms_image WHERE ID = @imageId');
        await sql.close();
        if(recordset.recordset.length > 0){
            return recordset.recordset[0]["IMAGE_PATH"];
        }else{
            return null;
        }
    }catch(err){
        console.log("getFilePath",err);
        return null;
    }
}

async function getFolderName(data){
    try{
        var pool = await sql.connect(config);
        var recordset = await pool.request()
            .input('imageFolderId', sql.Int, data.imageFolderId)
            .query('SELECT FOLDER_NAME FROM cms_image_folder WHERE ID = @imageFolderId');
        await sql.close();
        if(recordset.recordset.length > 0){
            return recordset.recordset[0]["FOLDER_NAME"];
        }else{
            return null;
        }
    }catch(err){
        console.log("getFolderName",err);
        return null;
    }
}

module.exports = {
    uploadImgIntoDB,
    createFolder,
    deleteFolder,
    updateFolder,
    getImageFolders,
    getImageList,
    deleteImage
}
