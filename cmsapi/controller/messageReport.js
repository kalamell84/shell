const sql = require('mssql');
const config = require('../config').dbconfg;
const chanelAccessToken = require('../config').channelAccessToken;
const utils = require('../utility/utility');
const Excel = require('exceljs');
const rp = require('request-promise');

async function exportMessageReport(data,res){
    try{
        var pool = await sql.connect(config);
        var arr_rows = [];
        var recordset = await pool.request()
            .input('month', sql.Int, data.month)
            .query('SELECT m.ID AS MESSAGE_ID, m.TITLE, p.PILLAR ,spl.LINE_USER_ID, spl.LINK, m.IMAGE_MESSAGE, m.BROADCAST_TIME ,spl.CREATE_DATE FROM shell_personalize_link spl LEFT JOIN message m ON m.ID = spl.MESSAGE_ID INNER JOIN pillar p ON m.PILLAR_ID = p.ID WHERE spl.MESSAGE_ID != -1 AND MONTH(spl.CREATE_DATE) = @month');        
                
        if(recordset.recordset.length > 0){
            arr_rows = [];
            var workbook = new Excel.Workbook();
            var worksheet = workbook.addWorksheet("LINE Report");
            worksheet.columns = [
                {header: 'MESSAGE ID', key: 'messageId', width: 10},
                {header: 'TITLE', key: 'title', width: 32}, 
                {header: 'PILLAR', key: 'pillar', width: 15,},
                {header: 'LINE_USER_ID', key: 'lineUserId', width: 25,},
                {header: 'USER NAME', key: 'username', width: 25,},
                {header: 'LINK', key: 'link', width: 32,},
                {header: 'IMAGE MESSAGE', key: 'image', width: 32,},
                {header: 'BROADCAST TIME', key: 'broadcastTime', width: 20,},
                {header: 'CREATE DATE', key: 'createdate', width: 20,}
            ];
            let promises = [];
            for(var i = 0; i < recordset.recordset.length; i++){
                var messageId = recordset.recordset[i]['MESSAGE_ID'];
                var title = recordset.recordset[i]['TITLE'];
                var pillar = recordset.recordset[i]['PILLAR'];
                var lineuid = recordset.recordset[i]['LINE_USER_ID'];
                var link = recordset.recordset[i]['LINK'];
                var imageMessage = recordset.recordset[i]['IMAGE_MESSAGE'];
                var broadcastTime = recordset.recordset[i]['BROADCAST_TIME'];
                var createdate = recordset.recordset[i]['CREATE_DATE'];
                promises.push(await messageReportAPIcall(res,lineuid,worksheet,messageId,title,pillar,link,imageMessage,broadcastTime,createdate));
            }
            Promise.all(promises).then(() => {
                // all done here
                sql.close();
                var fileName = 'linebc_direct_message_report.xlsx';
                res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                res.setHeader("Content-Disposition", "attachment; filename=" + fileName);
                workbook.xlsx.write(res).then(function(){
                    res.end();
                });
            }).catch(err => {
                res.header('Content-Type', 'text/html').send("<html><h1 style=\"text-align: center; position: relative; top: 50%; transform:translateY(-50%);\">No Report</h1></html>");
            });
        }else{
            await sql.close();
            res.header('Content-Type', 'text/html').send("<html><h1 style=\"text-align: center; position: relative; top: 50%; transform:translateY(-50%);\">No Report</h1></html>");
        }
    }catch(err){
        console.log("exportMessageReport error: ",err)
        res.header('Content-Type', 'text/html').send("<html><h1 style=\"text-align: center; position: relative; top: 50%; transform:translateY(-50%);\">Something went wrong.</h1></html>");
    }
}

async function messageReportAPIcall(res,lineuid,worksheet,messageId,title,pillar,link,imageMessage,broadcastTime,createdate){
    var options = {
        'method': 'GET',
        'url': `https://api.line.me/v2/bot/profile/${lineuid}`,
        'headers': {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${chanelAccessToken}`
        },
        'json': true
    };
    return rp(options)
        .then(function (body) {
            var line_name = body.displayName;
            var picture_img = body.pictureUrl;
            worksheet.addRow({messageId: messageId, title: title, pillar: pillar, lineUserId: lineuid, username: line_name, link: link, imageMessage: imageMessage, broadcastTime: new Date(broadcastTime), createdate: new Date(createdate)});
        })
        .catch(function (err) {
            console.log("lineapi",err);
        });
}

async function exportRichmenuReport(data,res){
    try{
        var workbook = new Excel.Workbook();
        var worksheet = workbook.addWorksheet("Richmenu");
        var worksheet2 = workbook.addWorksheet("Imagemap Menu");
        worksheet.columns = [
            {header: 'RESPONSE KEYWORD', key: 'keyword', width: 25},
            {header: 'CLICKS', key: 'click', width: 15}
        ];
        worksheet2.columns = [
            {header: 'LINK', key: 'link', width: 120},
            {header: 'SUBJECT', key: 'subject', width: 40},
            {header: 'CLICKS', key: 'click', width: 10}
        ];
        var pool = await sql.connect(config);
        var arr_rows = [];
        var sql_str = 'SELECT RESPONSE_KEYWORD, COUNT(*) AS CLICKS FROM shell_personalize_response WHERE (RESPONSE_KEYWORD = \'โปรแรงประจำเดือน\' OR RESPONSE_KEYWORD = \'อิ่มอร่อยกับเชลล์ชวนชิม\' OR RESPONSE_KEYWORD = \'ค้นหาปั๊มเชลล์\' OR RESPONSE_KEYWORD = \'เกี่ยวกับเชลล์ชวนชิม\')'+(data.month > 0 ? ' AND MONTH(CREATE_DATE) = @month': '')+(data.year != "" ? ' AND YEAR(CREATE_DATE) = @year':'')+' GROUP BY RESPONSE_KEYWORD ORDER BY CLICKS DESC'
        var recordset = await pool.request()
            .input('month', sql.Int, data.month)
            .input('year', sql.Int, data.year)
            .query(sql_str);
                   
        if(recordset.recordset.length > 0){
            arr_rows = [];
            for(var i = 0; i < recordset.recordset.length; i++){
                var keyword = recordset.recordset[i]['RESPONSE_KEYWORD'];
                var click = recordset.recordset[i]['CLICKS'];
                worksheet.addRow({keyword: keyword, click: click});
            }
            await sql.close();
            var pool2 = await sql.connect(config);
            var sql_str2 = 'SELECT LINK,(CASE WHEN CHARINDEX(\'utm=\', LINK) > 0 THEN (CASE WHEN REPLACE(SUBSTRING(LINK,CHARINDEX(\'utm=\', LINK),LEN(LINK)),\'utm=\',\'\') = \'oilprice\' THEN \'อัพเดทราคาน้ำมัน\' WHEN REPLACE(SUBSTRING(LINK,CHARINDEX(\'utm=\', LINK),LEN(LINK)),\'utm=\',\'\') = \'promo\' THEN \'โปรแรงประจำเดือน\' WHEN REPLACE(SUBSTRING(LINK,CHARINDEX(\'utm=\', LINK),LEN(LINK)),\'utm=\',\'\') = \'stationsearch\' THEN \'ค้นหาปั๊มเชลล์\' WHEN REPLACE(SUBSTRING(LINK,CHARINDEX(\'utm=\', LINK),LEN(LINK)),\'utm=\',\'\') = \'sss\' THEN \'อิ่มอร่อยกับเชลล์ชวนชิม\' WHEN REPLACE(SUBSTRING(LINK,CHARINDEX(\'utm=\', LINK),LEN(LINK)),\'utm=\',\'\') = \'oilpackages\' THEN \'แพ็กเกจออนไลน์เปลี่ยนถ่ายน้ำมันเครื่อง\' WHEN REPLACE(SUBSTRING(LINK,CHARINDEX(\'utm=\', LINK),LEN(LINK)),\'utm=\',\'\') = \'clubsmart\' THEN \'ลงทะเบียนออนไลน์ Shell ClubSmart\' ELSE REPLACE(SUBSTRING(LINK,CHARINDEX(\'utm=\', LINK),LEN(LINK)),\'utm=\',\'\') END) ELSE \'\' END) AS SUBJECT, COUNT(*) AS CLICKS FROM shell_personalize_link WHERE MESSAGE_ID = -1 AND LINK != \'https%3A%2F%2Fwww.google.com%2Fmaps%2Fd%2Fviewer%3Fmid%3D1KafHXaOU0BcBMMRWWtrhrtBYbCE%26shorturl%3D1\' AND LINK NOT LIKE \'https://www.google.com/test%\' AND LINK NOT LIKE \'%/test\''+(data.month > 0 ? ' AND MONTH(CREATE_DATE) = @month': '')+(data.year != "" ? ' AND YEAR(CREATE_DATE) = @year':'')+' GROUP BY LINK ORDER BY CLICKS DESC';
            var recordset2 = await pool2.request()
            .input('month', sql.Int, data.month)
            .input('year', sql.Int, data.year)
            .query(sql_str2)
            if(recordset2.recordset.length > 0){
                for(var i = 0; i < recordset2.recordset.length; i++){
                    var link = recordset2.recordset[i]['LINK'];
                    var subject = recordset2.recordset[i]['SUBJECT'];
                    var click = recordset2.recordset[i]['CLICKS'];
                    worksheet2.addRow({link: link, subject: subject, click: click});
                }
            }
            await sql.close();
            /* style sheet */
            worksheet.eachRow(function (row, rowNumber) {
                row.eachCell(function (cell, colNumber) {
                    row.getCell(colNumber).border = {
                        top: { style: "thin" },
                        left: { style: "thin" },
                        bottom: { style: "thin" },
                        right: { style: "thin" },
                    };
                    row.getCell(1).alignment = {
                        vertical: "top",
                        horizontal: "left"
                    }
                    row.getCell(2).alignment = {
                        vertical: "top",
                        horizontal: "right"
                    }
                    if (rowNumber == 1) {
                        cell.fill = {
                            type: "pattern",
                            pattern: "solid",
                            fgColor: { argb: "FFC000" },
                            bgColor: { argb: "FFC000" }
                        };
                        cell.font = {
                            bold: true
                        }
                    }
                });
            });

            worksheet2.eachRow(function (row, rowNumber) {
                row.eachCell(function (cell, colNumber) {
                    row.getCell(colNumber).border = {
                        top: { style: "thin" },
                        left: { style: "thin" },
                        bottom: { style: "thin" },
                        right: { style: "thin" },
                    };
                    if(rowNumber >= 2){
                        row.getCell(1).alignment = {
                            vertical: "top",
                            horizontal: "left"
                        }
                    }
                    row.getCell(2).alignment = {
                        vertical: "top",
                        horizontal: "left"
                    }
                    row.getCell(3).alignment = {
                        vertical: "top",
                        horizontal: "left"
                    }
                    if (rowNumber == 1) {
                        row.getCell(1).alignment = {
                            vertical: "top",
                            horizontal: "center"
                        }
                        cell.fill = {
                            type: "pattern",
                            pattern: "solid",
                            fgColor: { argb: "FFC000" },
                            bgColor: { argb: "FFC000" }
                        };
                        cell.font = {
                            bold: true
                        }
                    }
                });
            });
            var month = ["All","Jan","Feb","Mar","Apr","May","June","July","Aug","Sep","Oct","Nov","Dec"];
            var fileName = 'richmenu_tracking_'+month[data.month]+(data.year != ""? "_"+data.year : "")+'.xlsx';
            res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            res.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            workbook.xlsx.write(res).then(function(){
                res.end();
            });
        }else{
            res.header('Content-Type', 'text/html').send("<html><h1 style=\"text-align: center; position: relative; top: 50%; transform:translateY(-50%);\">No Report</h1></html>");
        }
    }catch(err){
        console.log("exportMessageReport error: ",err);
        res.header('Content-Type', 'text/html').send("<html><h1 style=\"text-align: center; position: relative; top: 50%; transform:translateY(-50%);\">Something went wrong.</h1></html>");
    }
}

module.exports = {
    exportMessageReport,
    exportRichmenuReport
}


