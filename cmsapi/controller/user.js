const sql = require('mssql');
const crypto = require('crypto-js');
const config = require('../config').dbconfg;
const dbconfig = require('../config').dbconfig;
const secret = require('../config').secret;
const utils = require('../utility/utility');

const mariadb = require('mariadb');
const pooldb = mariadb.createPool(dbconfig);

const pooldata = require('../db');

async function getUser(data,res){
    let conn;
    try{

        
        conn = await pooldb.getConnection();
        let sql = 'SELECT au.USER_ID, au.USERNAME, au.PASSWORD, au.EMAIL, au.ROLE_ID, ar.ROLE as ROLE_NAME, au.CREATE_DATE, au.UPDATE_DATE, au.IS_ACTIVE FROM admin_user au INNER JOIN admin_role ar ON au.ROLE_ID = ar.ID WHERE au.EMAIL LIKE %?% AND au.IS_ACTIVE = 1';
        let recordset = await conn.query(sql, [data.email]);
        if (recordset.length > 0) {
            await utils.showResult(res,true,"success", recordset);
        } else {
            await utils.showResult(res,false,"This email is not found.",recordset);
        }
        /*
        var pool = await sql.connect(config);
        var recordset = await pool.request()
            .input('email', sql.VarChar, '%'+data.email+'%')
            .query('SELECT au.USER_ID, au.USERNAME, au.PASSWORD, au.EMAIL, au.ROLE_ID, ar.ROLE as ROLE_NAME, au.CREATE_DATE, au.UPDATE_DATE, au.IS_ACTIVE FROM admin_user au INNER JOIN admin_role ar ON au.ROLE_ID = ar.ID WHERE au.EMAIL LIKE @email AND au.IS_ACTIVE = 1');
        if(recordset.recordset.length > 0){
            await utils.showResult(res,true,"success",recordset.recordset);
        }else{
            await utils.showResult(res,false,"This email is not found.",recordset.recordset);
        }*/

    }catch(err){
        await utils.showResult(res,false,"Something went wrong.");
    }
}

async function getUsers(data, res) {
    /*
    try{
        var pool = await sql.connect(config);
        var recordset = await pool.request()
            .query('SELECT  au.USER_ID, au.USERNAME, au.PASSWORD, au.EMAIL, au.ROLE_ID, ar.ROLE as ROLE_NAME, au.CREATE_DATE, au.UPDATE_DATE, au.IS_ACTIVE FROM admin_user au INNER JOIN admin_role ar ON au.ROLE_ID = ar.ID WHERE au.IS_ACTIVE = 1');
        if(recordset.recordset.length > 0){
            await utils.showResult(res,true,"success",recordset.recordset);
        }else{
            await utils.showResult(res,false,"This user_id is not found.",recordset.recordset);
        }
    }catch(err){
        await utils.showResult(res,false,"Something went wrong.");
    }
    */

    let conn;
    try{
        let sql = 'SELECT au.USER_ID, au.USERNAME, au.PASSWORD, au.EMAIL, au.ROLE_ID, ar.ROLE as ROLE_NAME, au.CREATE_DATE, au.UPDATE_DATE, au.IS_ACTIVE FROM admin_user au INNER JOIN admin_role ar ON au.ROLE_ID = ar.ID WHERE au.IS_ACTIVE = 1';

        const rows = await pooldata.query(sql);
        if (rows.length > 0) {
            await utils.showResult(res,true,"success", rows);
        } else {
            await utils.showResult(res,false,"data empty");
        }
       
        /*
        var pool = await sql.connect(config);
        var recordset = await pool.request()
            .input('email', sql.VarChar, '%'+data.email+'%')
            .query('SELECT au.USER_ID, au.USERNAME, au.PASSWORD, au.EMAIL, au.ROLE_ID, ar.ROLE as ROLE_NAME, au.CREATE_DATE, au.UPDATE_DATE, au.IS_ACTIVE FROM admin_user au INNER JOIN admin_role ar ON au.ROLE_ID = ar.ID WHERE au.EMAIL LIKE @email AND au.IS_ACTIVE = 1');
        if(recordset.recordset.length > 0){
            await utils.showResult(res,true,"success",recordset.recordset);
        }else{
            await utils.showResult(res,false,"This email is not found.",recordset.recordset);
        }*/

    }catch(err){
        await utils.showResult(res,false,"Something went wrong." + err);
    }
}

async function getUserById(data,res){
    let conn;
    try{
        conn = await pooldb.getConnection();
        let sql = 'SELECT * FROM admin_user WHERE USER_ID LIKE ? AND IS_ACTIVE = 1';
        let recordset = await conn.query(sql, [data.userId]);
        console.log(recordset.length);
        if (recordset.length > 0) {
            await utils.showResult(res,true,"success", recordset);
        } else {
            await utils.showResult(res,false,"data empty");
        }

    }catch(err){
        await utils.showResult(res,false,"Something went wrong.");
    }
}

async function searchUser(data,res){
    if(data.email != ""  && data.email != null){
        await getUser(data,res);
    }else{
        await getUsers(data,res);
    }
}

async function getUserLogin(data, res) {
    var email = data.email;
    var password = data.password;
    var query = 'SELECT USER_ID, USERNAME, EMAIL, PASSWORD, ROLE_ID FROM ADMIN_USER WHERE EMAIL = \'' + email + '\' AND IS_ACTIVE = 1'; 
    await utils.executeQueryLoginNew(res, query, password);
}

async function checkExistingAdminUser(data){
    try{
        var pool = await sql.connect(config);
        var recordset = await pool.request()
            .input('email', sql.VarChar, data.email)
            .query('SELECT * FROM admin_user WHERE EMAIL = @email AND IS_ACTIVE = 1');
        if(recordset.recordset.length > 0){
            return true;
        }else{
            return false;
        }
    }catch(err){
        console.log("err",err);
        return false;
    }
}

async function createUser(data,res){
    try{
        var has_user = await checkExistingAdminUser(data);
        if(!has_user){
            var password = await crypto.AES.encrypt(data.password, secret).toString();
            var pool = await sql.connect(config);
            var recordset = await pool.request()
                .input('username', sql.VarChar, data.username)
                .input('email', sql.VarChar, data.email)
                .input('password', sql.VarChar, password)
                .input('roleId', sql.VarChar, data.roleId)
                .query('INSERT INTO admin_user (USERNAME,PASSWORD,EMAIL,ROLE_ID) VALUES (@username,@password,@email,@roleId)');
            if(recordset.rowsAffected > 0){
                var result = [{
                    "username": data.username,
                    "email": data.email,
                    "password": password,
                    "roleId": data.roleId
                }]
                await utils.showResult(res,true,"insert success",result);
            }else{
                await utils.showResult(res,false,"insert unsuccess");
            }
        }else{
            await utils.showResult(res,false,"This user is existing.");
        }
    }catch(err){
        console.log(err);
        await utils.showResult(res,false,"Something went wrong.");
    }
}

async function updateUser(data,res){
    try{
        var has_user = await checkExistingAdminUser(data);
        if(has_user){
            var password = await crypto.AES.encrypt(data.password, secret).toString();
            var pool = await sql.connect(config);
            var recordset = await pool.request()
                .input('username', sql.VarChar, data.username)
                .input('email', sql.VarChar, data.email)
                .input('password', sql.VarChar, password)
                .input('roleId', sql.VarChar, data.roleId)
                .query('UPDATE admin_user SET USERNAME = @username, PASSWORD = @password, ROLE_ID = @roleId, UPDATE_DATE = GETDATE() WHERE EMAIL = @email');
            if(recordset.rowsAffected > 0){
                var result = [{
                    "username": data.username,
                    "email": data.email,
                    "password": password,
                    "roleId": data.roleId
                }]
                await utils.showResult(res,true,"update success",result);
            }else{
                await utils.showResult(res,false,"update unsuccess");
            }
        }else{
            await utils.showResult(res,false,"This user is not existing.");
        }
    }catch(err){
        await utils.showResult(res,false,"Something went wrong.");
    }
}

async function deleteUser(data,res){
    try{
        var has_user = await checkExistingAdminUser(data);
        if(has_user){
            var pool = await sql.connect(config);
            var recordset = await pool.request()
                .input('email', sql.VarChar, data.email)
                .query('UPDATE admin_user SET IS_ACTIVE = 0 WHERE EMAIL = @email');
            if(recordset.rowsAffected > 0){
                await utils.showResult(res,true,"delete success");
            }else{
                await utils.showResult(res,false,"delete unsuccess");
            }
        }else{
            await utils.showResult(res,false,"This user is not existing.");
        }
    }catch(err){
        await utils.showResult(res,false,"Something went wrong.");
    }
}

async function getRole(data,res){
    try{
        var pool = await sql.connect(config);
        var recordset = await pool.request()
                .input('roleId', sql.Int, data.roleId)
                .query('SELECT * FROM admin_role WHERE ID = @roleId AND IS_ACTIVE = 1');
        if(recordset.recordset.length > 0){
            await utils.showResult(res,true,"success",recordset.recordset);
        }else{
            await utils.showResult(res,false,"This role is not found. or not active.");
        }
    }catch(err){
        await utils.showResult(res,false,"Something went wrong.");
    }
}

async function getAllRoles(res){
    try{
        var pool = await sql.connect(config);
        var recordset = await pool.request()
            .query('SELECT * FROM admin_role WHERE IS_ACTIVE = 1');
        if(recordset.recordset.length > 0){
            await utils.showResult(res,true,"success",recordset.recordset);
        }else{
            await utils.showResult(res,false,"no role");
        }
    }catch(err){
        await utils.showResult(res,false,"Something went wrong.");
    }
}

function createUser2(data, res) {
    var username = data.username;
    var email = data.email;
    var password = crypto.AES.encrypt(data.password, secret).toString();
    var roleId = data.roleId;

    var insertQuery = 'INSERT INTO admin_user (USERNAME,PASSWORD,EMAIL,ROLE_ID) '
                    + ' VALUES (\'' + username + '\', \'' + password + '\', \'' + email + '\', \'' + roleId + '\');'; 
    var selectQuery = 'SELECT * FROM admin_user WHERE EMAIL = \'' + email + '\''; 

    utils.executeInsertQuery(res, insertQuery, selectQuery);
}

module.exports = {
    getUsers,
    getUser,
    getUserLogin,
    searchUser,
    createUser,
    updateUser,
    deleteUser,
    getRole,
    getUserById,
    getAllRoles
}