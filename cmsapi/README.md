# API Document

## User Management

### Login

Method: POST

URL: http://localhost:3033/api/userlogin

JSON
```
{
	"email":"test@test.com",
	"password": "test123"
}
```

### Retrieve User By Id
Method: POST

URL: http://localhost:3033/api/getUserById

JSON
```
{
	"userId": 1
}
```

### Search

Method: POST

URL: http://localhost:3033/api/searchUser

JSON
```
{
	"email": "test@test.com"
}
```

### Create User

Method: POST

URL: http://localhost:3033/api/createUser

JSON
```
{
	"email":"mail@mail.com",
	"username" : "Test Test",
	"password": "test123",
	"roleId": 2
}
```

### Update User
Method: POST

URL: http://localhost:3033/api/updateUser

JSON
```
{
	"email" : "test@mail.com",
	"username" : "Test Test",
	"password" : "test123",
	"roleId" : 2
}
```

### Delete User
Method: POST

URL: http://localhost:3033/api/deleteUser

JSON
```
{
	"email":"mail@mail.com"
}
```

### Retrieve Role By Role Id
Method: POST

URL: http://localhost:3033/api/getRole

JSON
```
{
	"roleId": 2
}
```

### Retrieve All Roles
Method: GET

URL: http://localhost:3033/api/getAllRoles




## Message Management

### Create Message
Method: POST
 
URL: http://localhost:3033/api/createMessage

JSON
```
var json1 = JSON.stringify({
	"type": "image",
	"originalContentUrl": "https://cdn.shopify.com/s/files/1/0231/6137/2752/files/bf-group_1600x.jpg",
	"previewImageUrl": "https://cdn.shopify.com/s/files/1/0231/6137/2752/files/bf-group_1600x.jpg",
	"animated": false
});

var json2 = JSON.stringify({
	"type": "text",
	"text": "Hello"
});
{
	"title": "Test Broadcast",
	"imageMessage": "https://cdn.shopify.com/s/files/1/0231/6137/2752/files/bt21-group_1600x.jpg",
	"pillarId": 1,
	"segmentId": 0,
	"broadcastTime": 2020-03-13 10:43:00,
	"broadcastType": "schedule",
	"message": [{
		"messageType": "image",
		"json": json1
	},{
		"messageType": "text",
		"json": json2 
	}]
}	
```

OR

```
{
	"title": "Test Broadcast",
	"imageMessage": "https://cdn.shopify.com/s/files/1/0231/6137/2752/files/bt21-group_1600x.jpg",
	"pillarId": 2,
	"segmentId": 0,
	"broadcastTime": "2020-03-13 10:43:00",
	"broadcastType": "schedule",
	"message": [{
		"messageType": "image",
		"json": "{\"type\": \"image\",\"originalContentUrl\": \"https://cdn.shopify.com/s/files/1/0231/6137/2752/files/bf-group_1600x.jpg\",\"previewImageUrl\": \"https://cdn.shopify.com/s/files/1/0231/6137/2752/files/bf-group_1600x.jpg\",\"animated\": false}"
	},{
		"messageType": "text",
		"json": "{\"type\": \"text\",\"text\": \"Hello\"}"
	}]
}	
```

Note: broadcastType field can be schedule or send now
Note2: json field in message must be string.

### Retrieve All Segments
Method: GET

URL: http://localhost:3033/api/getAllSegments


### Retrieve Segment By Segment Id
Method: POST

URL: http://localhost:3033/api/getSegment

JSON
```
{
	"segmentId": 2
}
```

### Retrieve All Pillars
Method: GET

URL: http://localhost:3033/api/getAllPillars


### Retrieve Pillar By Pillar Id
Method: POST

URL: http://localhost:3033/api/getPillar

JSON
```
{
	"pillarId": 2
}
```

### Retrieve All Messages
Method: GET

URL: http://localhost:3033/api/getAllMessages


### Retrieve Message By Message Id
Method: POST

URL: http://localhost:3033/api/getMessage

JSON
```
{
	"messageId": 2
}
```

### Delete Message
Method: POST

URL: http://localhost:3033/api/deleteMessage

JSON
```
{
	"messageId": 2
}
```

### Edit Message
Method: POST

URL: http://localhost:3033/api/editMessage

Note: The json format like createMessage ,but add messageId

JSON
```
var json1 = JSON.stringify({
	"type": "image",
	"originalContentUrl": "https://cdn.shopify.com/s/files/1/0231/6137/2752/files/bf-group_1600x.jpg",
	"previewImageUrl": "https://cdn.shopify.com/s/files/1/0231/6137/2752/files/bf-group_1600x.jpg",
	"animated": false
});

var json2 = JSON.stringify({
	"type": "text",
	"text": "Hello"
});

{
	"messageId": 1
	"title": "Test Broadcast",
	"imageMessage": "https://cdn.shopify.com/s/files/1/0231/6137/2752/files/bt21-group_1600x.jpg",
	"pillarId": 1,
	"segmentId": 0,
	"broadcastTime": 2020-03-13 10:43:00,
	"broadcastType": "schedule",
	"message": [{
		"messageType": "image",
		"json": json1
	},{
		"messageType": "text",
		"json": json2 
	}]
}	
```

### Test Message
Method: POST
 
URL: http://localhost:3033/api/testMessage

JSON
```
var json1 = JSON.stringify({
	"type": "image",
	"originalContentUrl": "https://cdn.shopify.com/s/files/1/0231/6137/2752/files/bf-group_1600x.jpg",
	"previewImageUrl": "https://cdn.shopify.com/s/files/1/0231/6137/2752/files/bf-group_1600x.jpg",
	"animated": false
});

var json2 = JSON.stringify({
	"type": "text",
	"text": "Hello"
});
{
	"title": "Test Broadcast",
	"imageMessage": "https://cdn.shopify.com/s/files/1/0231/6137/2752/files/bt21-group_1600x.jpg",
	"pillarId": 1,
	"segmentId": 0,
	"broadcastTime": 2020-03-13 10:43:00,
	"broadcastType": "schedule",
	"message": [{
		"messageType": "image",
		"json": json1
	},{
		"messageType": "text",
		"json": json2 
	}]
}	
```

OR

```
{
	"title": "Test Broadcast",
	"imageMessage": "https://cdn.shopify.com/s/files/1/0231/6137/2752/files/bt21-group_1600x.jpg",
	"pillarId": 2,
	"segmentId": 0,
	"broadcastTime": "2020-03-13 10:43:00",
	"broadcastType": "schedule",
	"message": [{
		"messageType": "image",
		"json": "{\"type\": \"image\",\"originalContentUrl\": \"https://cdn.shopify.com/s/files/1/0231/6137/2752/files/bf-group_1600x.jpg\",\"previewImageUrl\": \"https://cdn.shopify.com/s/files/1/0231/6137/2752/files/bf-group_1600x.jpg\",\"animated\": false}"
	},{
		"messageType": "text",
		"json": "{\"type\": \"text\",\"text\": \"Hello\"}"
	}]
}	
```

Note: use params like create message api


## Tester Management

### Generate Track Code
Method: GET

URL: http://localhost:3033/api/genTrackCode


### Create Tester
Method: POST

URL: http://localhost:3033/api/createTester

JSON
```
{
	"title": "testuser testest",
	"trackCode": "puyAWsVj1HPZ3wwyz6jroojSg0glEYPU"
}
```

### Send Tester Activation

window.location = "https://liff.line.me/1653904204-Dzqan726/tester/activation?activation_code=puyAWsVj1HPZ3wwyz6jroojSg0glEYPz";


### Retrieve Tester
Method: GET

URL: http://localhost:3033/api/getAllTesters


### Delete Tester
Method: POST

URL: http://localhost:3033/api/deleteTester

JSON
```
{
	"testerId": 2
}
```

### Update Tester
Method: POST

URL: http://localhost:3033/api/updateTester

JSON
```
{
	"testerId": 4,
	"title": "test2 test"
}
```

### Retrieve Tester By Tester Id
Method: POST

URL: http://localhost:3033/api/getTester

JSON
```
{
	"testerId": 6
}
```

## Report Dashboard

### Retrieve Broadcast Report Dashboard
Method: POST

URL: http://localhost:3033/api/getBroadcastReport

JSON
```
{
	"dateStart": "2020-03-13",
	"dateEnd": "2020-03-14"
}
```

### Retrieve Report Dashboard Yearly
Method: POST

URL: http://localhost:3033/api/getYearReport

JSON
```
{
	"year": 2020
}
```

### Retrieve Report Dashboard Monthly
Method: POST

URL: http://localhost:3033/api/getMonthReport

JSON
```
{
	"year": 2020,
	"month": 3
}
```

## Image Management

### Upload Image
Method: POST

URL: http://localhost:3033/api/uploadImage

JSON
```
{
	"imageFolderId": 1,
	"imageFolder": "test1",
	"imageName": "test-01",
	"imageExtension": "jpg",  (keep in hidden field)
	"imageTag": "test",
	"imageFile": file (file type)
}
```

Note: *if image is not in folder, imageFolderId is 0 and imageFolder is empty.*
Note: *must order params like above.*
Note: *only lower case.*

### Create Folder
Method: POST

URL: http://localhost:3033/api/createFolder

JSON
```
{
	"imageFolder": "test"
}
```

### Update Folder
Method: POST

URL: http://localhost:3033/api/updateFolder

JSON
```
{
	"imageFolder": "test"
}
```

### Delete Folder
Method: POST

URL: http://localhost:3033/api/deleteFolder

JSON
```
{
	"imageFolderId": 1
}
```

### Update Folder
Method: POST

URL: http://localhost:3033/api/updateFolder

JSON
```
{
	"imageFolderId": 1,
	"imageFolder": "test11"
}
```

### Retrieve Image Folders
Method: GET

URL: http://localhost:3033/api/getImageFolders


### Retrieve Image List
Method: POST

URL: http://localhost:3033/api/getImageList

JSON
```
{
	"imageFolderId": 0
}
```

### Delete Image
Method: POST

URL: http://localhost:3033/api/deleteImage

JSON
```
{
	"imageId": 1
}
```