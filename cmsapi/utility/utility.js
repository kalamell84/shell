const sql = require('mssql');
const crypto = require('crypto-js');
const config = require('../config').dbconfg;
const dbconfig = require('../config').dbconfig;
const secret = require('../config').secret;

const mariadb = require('mariadb');
const pooldb = mariadb.createPool(dbconfig);

async function showResult (res,status,message,result){
    
    if(result != null){
        var json = [{
            "status": status,
            "message": message,
            "result": result
        }];
        res.json(json);
    }else{
        var json = [{
            "status": status,
            "message": message
        }];
        res.json(json);
    }
}

async function executeQueryLogin(res, query, password) {    
    sql.connect(config, function (err) {
        if (err) {
            console.log("Error while connecting database :- " + err);
            res.send({
                "status": false,
                "message": err
            });
            // res.send(err);
        } else {
            // create Request object
            var request = new sql.Request();
            // query to the database
            request.query(query, function (err, result) {
                if (err) {
                    console.log("Error while querying database :- " + err);
                    res.send({
                        "status": false,
                        "message": err
                    });
                    // res.send(err);
                } else {
                    if (result.recordset.length > 0) {  
                        for (var i = 0; i < result.recordset.length; i++) {
                            //encrypt password
                            // const enc = crypto.AES.encrypt(password, secret).toString();
                            // console.log(enc);

                            // res.send({
                            //     "status": true,
                            //     "message": enc
                            // });
                            var dec  = crypto.AES.decrypt(result.recordset[i].PASSWORD, secret);
                            // console.log("true: "+dec.toString(crypto.enc.Utf8));
                            // console.log("true: "+dec);
                            if (dec.toString(crypto.enc.Utf8) == password) {
                                res.send({
                                    "status": true,
                                    "message": "success",
                                    "result": result.recordset
                                });             
                            } else {
                                res.send({
                                    "status": false,
                                    "message": "Check password"
                                });
                            }
                        }       
                    } else {
                        res.send({
                            "status": false,
                            "message": "No data"
                        });
                    }
                }
            });
        }
    });
}


async function executeQueryLoginNew(res, query, password) {    
    let conn;
    try {
        conn = await pooldb.getConnection();
        let recordset = await conn.query(query);
        if (recordset.length == 0) {
            res.send({
                "status": false,
                "message": err
            });
        } else { 
            let mypassword = recordset.password;
            var dec  = crypto.AES.decrypt(mypassword, secret);

            if (dec.toString(crypto.enc.Utf8) == password) {
                res.send({
                    "status": true,
                    "message": "success",
                    "result": recordset
                });             
            } else {
                res.send({
                    "status": false,
                    "message": "Check password"
                });
            }

        }

    } catch (err) {
        res.send({
            "status": false,
            "message": "No data"
        });

    }

    
}


function executeQuery(res, query) {
    sql.connect(config, function (err) {
        if (err) {
            console.log("Error while connecting database :- " + err);
            res.send({
                "status": false,
                "message": err
            });
            // res.send(err);
        } else {
            // create Request object
            var request = new sql.Request();
            // query to the database
            request.query(query, function (err, result) {
                if (err) {
                    console.log("Error while querying database :- " + err);
                    res.send({
                        "status": false,
                        "message": err
                    });
                    // res.send(err);
                } else {
                    if (result.recordset) {
                        const jsonData = [];

                        jsonData.push({
                            "status": true,
                            "data": result.recordset
                        });

                        // for (const [key, value] of Object.entries(result.recordset[0])) {                            
                        //     console.log(key, value);
                        // }

                        res.send(jsonData);
                    } else {
                        res.send({
                            "status": false,
                            "message": "No data"
                        });
                    }
                }
            });
        }
    });
}

async function executeInsertQuery(res, insertQuery, selectQuery) {
    try {
        let pool = await sql.connect(config)

        let result1 = await pool.request()
            .query(insertQuery)
            
        console.dir(result1)
    
        // Stored procedure
        
        let result2 = await pool.request()
            .query(selectQuery)
        
        console.dir(result2)

        if (result2.recordset) {
            const jsonData = [];

            jsonData.push({
                "status": true,
                "message": "insert success",
                "result": result2.recordset
            });

            // for (const [key, value] of Object.entries(result.recordset[0])) {                            
            //     console.log(key, value);
            // }

            res.send(jsonData);
        } else {
            res.send({
                "status": false,
                "message": "No user"
            });
        }
    } catch (err) {
        // ... error checks
        console.log("Error while querying database :- " + err);
        res.send({
            "status": false,
            "message": err
        });
    }
}

module.exports = {
    showResult,
    executeQueryLogin,
    executeQueryLoginNew,
    executeQuery,
    executeInsertQuery
}