const express = require('express')
const bodyParser = require('body-parser');
const user = require('./controller/user');
const lineusesr = require('./controller/lineuser');
const message = require('./controller/message');
const tester = require('./controller/tester'); 
const report = require('./controller/report');
const ctreport = require('./controller/report');
const upload = require('./controller/upload');
const messageReport = require('./controller/messageReport');
const path = require('path');
const fs = require('fs');
const multer = require('multer');
const cors = require('cors');
const { default: axios } = require('axios');

const app = express();
app.use(cors());
// Body Parser Middleware
app.use(bodyParser.json());

var dir = path.join(__dirname, 'public');
app.use(express.static(dir));

//CORS Middleware
app.use(function (req, res, next) {
    //Enabling CORS 
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, contentType,Content-Type, Accept, Authorization");
    next();
});



app.use(bodyParser.urlencoded({
    extended: true
}));


//Setting up server
var server = app.listen(process.env.PORT || 3001, function () {
    var port = server.address().port;
    console.log("App now running on port", port);
});

app.get('/', function (req, res) {
    res.send('HELLO WORLD');
})

////////////////////////////// USER MANAGEMENT //////////////////////////////
app.get('/api/users', function(req, res) {
    user.getUsers(req.body, res);
})
app.post("/api/userlogin", function (req, res) {
    user.getUserLogin(req.body, res);
});

app.post("/api/users", function (req, res) {
    user.getUsers(req.body, res);
});

app.post("/api/user", function (req, res) {
    user.getUser(req.body, res);
});

app.post("/api/getUserById", function (req, res) {
    user.getUserById(req.body, res);
});

app.post("/api/searchUser", function (req, res) {
    user.searchUser(req.body, res);
});

app.post("/api/createUser", function (req, res) {
    user.createUser(req.body, res);
});

app.post("/api/updateUser", function (req, res) {
    user.updateUser(req.body, res);
});

app.post("/api/deleteUser", function (req, res) {
    user.deleteUser(req.body, res);
});

app.post("/api/getRole", function (req, res) {
    user.getRole(req.body, res);
});

app.get("/api/getAllRoles", function (req, res) {
    user.getAllRoles(res);
});

////////////////////////////// MESSAGE MANAGEMENT //////////////////////////////

app.post("/api/createMessage", function (req, res) {
    message.createMessage(req.body,res);
});

app.get("/api/getAllSegments", function (req, res) {
    message.getAllSegments(res);
});

app.post("/api/getSegment", function (req, res) {
    message.getSegment(req.body,res);
});

app.get("/api/getAllPillars", function (req, res) {
    message.getAllPillars(res);
});

app.get("/api/getAllPillars2", function (req, res) {
    message.getAllPillars2(res);
});


app.get("/api/getAllCampaigns", function (req, res) {
    message.getAllCampaigns(res);
});

app.post("/api/getPillar", function (req, res) {
    message.getPillar(req.body,res);
});

app.get("/api/getAllMessages", function (req, res) {
    message.getAllMessages(res);
});

app.post("/api/getMessage", function (req, res) {
    message.getMessage(req.body,res);
});

app.post("/api/deleteMessage", function (req, res) {
    message.deleteMessage(req.body, res);
});

app.post("/api/editMessage", function (req, res) {
    message.editMessage(req.body, res);
});

app.post("/api/testMessage", function (req, res) {
    message.testMessage(req.body,res);
});

app.post("/api/getProduct", function (req, res) {
    message.getProduct(req.body,res);
});

app.get("/api/getAllProducts", function (req, res) {
    message.getAllProducts(res);
});

////////////////////////////// TESTER MANAGEMENT //////////////////////////////

app.get("/api/genTrackCode", function (req, res) {
    tester.genTrackCode(res);
});

app.post("/api/createTester", function (req, res) {
    tester.createTester(req.body,res);
});

app.post("/api/updateLineUserId", function (req, res) {
    tester.updateLineUserId(req.body,res);
});

app.get("/tester/activation", function (req, res) {
    res.sendFile(path.join(__dirname + '/view/activation.html'));
});

app.get("/trck", function (req, res) {
    res.sendFile(path.join(__dirname + '/view/trck.html'));
});

app.get("/api/getAllTesters", function (req, res) {
    tester.getAllTesters(res);
});

app.post("/api/deleteTester", function (req, res) {
    tester.deleteTester(req.body, res);
});

app.post("/api/updateTester", function (req, res) {
    tester.updateTester(req.body, res);
});

app.post("/api/getTester", function (req, res) {
    tester.getTester(req.body,res);
});

////////////////////////////// REPORT DASHBOARD //////////////////////////////

app.post("/api/getBroadcastReport", function (req, res) {
    report.getBroadcastReport(req.body,res);
});

app.post("/api/getYearReport", function (req, res) {
    report.getYearReport(req.body,res);
});

app.post("/api/getMonthReport", function (req, res) {
    report.getMonthReport(req.body,res);
});

app.post("/api/getBoardcastLinkReport", function (req, res) {
    report.getBoardcastLinkReport(req.body,res);
});

////////////////////////////// HTML MANAGEMENT //////////////////////////////

app.get("/upload", function (req, res) {
    res.sendFile(path.join(__dirname + '/view/testupload.html'));
});

app.get("/generateLink", function (req, res) {
    res.sendFile(path.join(__dirname + '/view/linkgeneration.html'));
});

app.get("/messageReport", function (req, res) {
    res.sendFile(path.join(__dirname + '/view/message_report.html'));
});

////////////////////////////// MESSAGE REPORT //////////////////////////////
app.get("/api/exportMessageReport", function (req, res) {
    messageReport.exportMessageReport(req.query,res);
});

app.get("/api/exportRichmenuReport", function (req, res) {
    messageReport.exportRichmenuReport(req.query,res);
});

////////////////////////////// TRACK LINK //////////////////////////////
app.post("/api/insertPersonalizeLink", function (req, res) {
    message.insertPersonalizeLink(req.body,res);
});

const storage = multer.diskStorage({
    destination:  function (req, file, cb) {
        var foldername = req.body.imageFolder;
        var dir = "./public/uploads/"+(foldername != null ? foldername : "");
        fs.exists(dir, exist => {
            if (!exist) {
                return fs.mkdir(dir, error => cb(error, dir))
            }
            return cb(null, dir)
        })
    },
    filename: function (req, file, cb) {
        var imagename = req.body.imageName;
        var datetimestamp = Date.now();
        cb(null, (imagename != null ? imagename : file.originalname.split('.')[0]) + '.' + file.originalname.split('.')[file.originalname.split('.').length -1])
    }
});
const uploadFilter = function(req, file, cb) {
    var ext = path.extname(file.originalname);
        if(ext.toLowerCase() !== '.png' && ext.toLowerCase() !== '.jpg' && ext.toLowerCase() !== '.gif' && ext.toLowerCase() !== '.jpeg') {
            return cb(new Error('Only images are allowed'));
        }

        var imagePath = "./public/uploads/"+req.body.imageFolder+"/"+req.body.imageName + '.' + req.body.imageExtension;
        fs.exists(imagePath, exist => {
            if(exist) {
                cb(null, false)
            }else{
                cb(null, true)
            }
        })

       
};

const uploadImg = multer({ 
    storage: storage,
    fileFilter: uploadFilter
}).single('imageFile');

app.post("/api/uploadImage", function (req, res) {
    uploadImg(req, res, function (err) {
        if (err) {
            return  res.send({
                "status": false,
                "message": "Only images are allowed"
            });
        }else{
            upload.uploadImgIntoDB(req,res)
        }
    });
});

app.post("/api/uploadImagetest", function (req, res) {
    uploadImg(req, res, function (err) {
        if (err) {
            return  res.send({
                "status": false,
                "message": "Only images are allowed"
            });
        }else{
            return  res.send({
                "status": true,
            });
        }
    });
});

app.post("/api/createFolder", function (req, res) {
    upload.createFolder(req.body,res);
});

app.post("/api/deleteFolder", function (req, res) {
    upload.deleteFolder(req.body,res);
});

app.post("/api/updateFolder", function (req, res) {
    upload.updateFolder(req.body,res);
});

app.get("/api/getImageFolders", function (req, res) {
    upload.getImageFolders(res);
});

app.get("/api/p2x", function (req, res) {
    upload.getImageFolders(res);
});

app.post("/api/getImageList", function (req, res) {
    upload.getImageList(req.body,res);
});

app.post("/api/deleteImage", function (req, res) {
    upload.deleteImage(req.body,res);
});


/*
app.get('/api/getpersonallink', async function (req, res) {

    const data = await message.getAllPersonalink(res, 1450268);
    res.json(data);
});


app.get('/api/getpersonalkeywords', function (req, res) {

    message.getAllPersonaKeywords(res);

});

app.get('/api/x11', function(req, res) {
    res.send('xxxxxxxxx');
})

*/

/* report */


app.get('/api/getpersonallink', async function (req, res) {

    //SELECT  [ID] as id,[LINE_USER_ID] as line_user_id,[MESSAGE_ID] as message_id,[LINK] as link,[CREATE_DATE] as create_date, current_timestamp as time
    try {
        const d = await message.getAllPersonalink(res, 1569325, '2021-11-01 00:00:00');
        //console.log('count ; ', d.length);
        if (d.length == 0) {
            res.json('no data .....');
            return;
        }

        d.forEach(async function(value, inx) {
            //console.log(value.id, ' ');
            let res = await updateTD('personal_link_json', value);
            if (res == 200) {
                console.log('send data ', value.id);
                message.updateSendpush(value.id, '2021-11-01 00:00:00');
            }
        });

        console.log('send data complete');
        
        res.send('personal link ok');
    } catch(err) {
        res.send(err);
    }
});


app.get('/api/getpersonalkeywords', async function (req, res) {

    try {
        const d = await message.getAllPersonaKeywordsv2(res, 1347497, '2021-11-01 00:00:00');
        res.send(d);
        //console.log('count ; ', d.length);
        /*
        if (d.length == 0) {
            res.json('no data .....');
            return;
        }



        d.forEach(async function(value, inx) {
            console.log(value.id, ' ');
            let res = await updateTD('personal_keyword_json', value);
            if (res == 200) {
                console.log('send data ', value.id);
                message.updateKeywordSendpush(value.id, '2021-11-01 00:00:00');
            }
        });

        console.log('send data complete');
        
        res.send('<<<<<<<< personal keywords ok');
        */
    } catch(err) {
        res.send(err);
    }

});


app.get('/api/getmessage', async function (req, res) {

    //SELECT  [ID] as id,[LINE_USER_ID] as line_user_id,[MESSAGE_ID] as message_id,[LINK] as link,[CREATE_DATE] as create_date, current_timestamp as time
    try {
        const d = await message.getPushMessage(res, 283, '2021-11-01 00:00:00');
        //console.log('count ; ', d.length);
        if (d.length == 0) {
            res.json('no data .....');
            return;
        }

        d.forEach(async function(value, inx) {
            //console.log(value.id, ' ');
            let res = await updateTD('message_json', value);
            if (res == 200) {
                console.log('send data ', value.id);
                
            }
        });

        console.log('send data complete');
        
        res.send('all message ok');
    } catch(err) {
        res.send(err);
    }
});

app.get('/api/getreport', async function(req, res) {
    try {
        
        const rp = await ctreport.getInsight();
        //res.json(rp);
        
        try {
            rp.forEach(async function(value, ix) {
                let rt = await getLineInsight(value.REQUEST_ID);
                if (rt) {
                    let json_data = JSON.stringify(rt);
                    let data = {
                        'TITLE': value.TITLE,
                        'MESSAGE_ID': value.ID,
                        'REQUEST_ID': value.REQUEST_ID,
                        'IMAGE_MESSAGE': value.IMAGE_MESSAGE,
                        'PILLAR': value.PILLAR,
                        'PRODUCT': value.PRODUCT,
                        'TAG': value.TAG,
                        'BROADCAST_TIME': value.BROADCAST_TIME,
                        'JSON_DATA': json_data
                    }
                    let save = await ctreport.saveInsight(data);
                    if (save) {
                        console.log('save ', value.ID);
                        ctreport.updateBatchReport(value.ID);
                        console.log('update ', value.ID);
                        console.log(json_data);
                    }
                } else {
                    console.log('update line error ', value.ID);
                }
            });

            res.send('update ok');
        } catch(e) {
            console.log('error foreach');
        }
        

        //res.send('ok...');

    } catch(err) {
        //res.send('error ', err);
        res.send(200, 'error ' +  err);
    }
    //res.send('report');
})

async function updateTD(table, data) {
    var config = {
        method: 'post',
        url: 'https://in.treasuredata.com/postback/v3/event/dtl_shell/' + table,
        headers: { 
          'Content-Type': 'application/json', 
          'X-TD-Write-Key': '9629/f02dd56fd8f4c7badb3afad0641c10228d2ee22a'
        },
        data : data
      };
      
      const res = await axios(config);

      return res.status;

}

async function getLineInsight(requestid) {
    try {
        var config = {
            method: 'get',
            url: 'https://api.line.me/v2/bot/insight/message/event?requestId=' + requestid,
            headers: { 
                'Authorization': 'Bearer og71HiVMMDpyvMISsHKP6Irrs38yEQHMrpW0gsLndD0ZTEreA1WEJdlnN1PScOKgjv11gqm+LCQciymwAm7nlMc7Em4STCb/Q8U7hVkr2ebJdeyGexNpkXWJJ/XADaAMkrO+u6evsb7n3JL4WXKGVGbZkCQsS1A9FX3FSRp/ceE='
            }
        };
    
        const res = await axios(config);
        return res.data;
        
    } catch (error) {
        console.log('errror line .... ', error);

        return false;
    }
    
    
}

app.post("/api/line_user_id", function (req, res) {
    lineusesr.getUserById(req.body.line_user_id, res);
});