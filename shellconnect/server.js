const express = require('express');
const path = require('path');
const app = express();
const bodyParser = require('body-parser');
const port = process.env.PORT || 3003;

app.use(express.static(__dirname + '/'));
//app.use(bodyParser.urlencoded({extend:true}));

app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);
app.get('/', (req, res) => {
    //res.send('hi shellconnect');
    console.log(process.env.cmsapiurl);
    res.render('home', { url: process.env.cmsapiurl});
});

app.get('/trck', (req, res) => {
    //res.sendFile(path.join(__dirname, '/trck.html'));
    res.render('trck', { url: process.env.cmsapiurl})
})

app.get('/trck/trck', (req, res) => {
    //res.sendFile(path.join(__dirname, '/trck.html'));
    res.render('trck', { url: process.env.cmsapiurl})

    
})

//Setting up server
app.listen(port, () => {
    console.log(`App now running on port ${port}`);
});
